<?php
  include_once 'core/init.php';

  $link = new Link();
  $mail = new PHPMailer();
  $table = 'users';
  $date = date('d-m-Y');
  $time = date('h:i A');
  $errors = [];

  if (isset($_POST['register'])) {
    $validate = new Validate();
    $val_validate = $validate->check($_POST, [
      'username' => [
        'required' => TRUE,
        'min' => 3,
        'unique' => 'users'
      ],
      'reg_password' => [
        'required' => TRUE,
        'min' => 3,
      ]
    ]);

    if ($val_validate->passed()) {
      $salt = Hash::salt(20);
      try {
        $link->add([
          'username' => $_POST['username'],
          'password' => Hash::make($_POST['reg_password']. $salt),
          'email' => $_POST['email'],
          'phone' => $_POST['phone'],
          'date' => $date,
          'time' => $time,
          'salt' => $salt
        ], $table);
        $user_name = $_POST['username'];
        $email = $_POST['email'];
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'lio.boxsecured.com';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'security@onedayshop.in';                 // SMTP username
        $mail->Password = '#0H%OIG#swyN';                           // SMTP password
        $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 465;                                    // TCP port to connect to


        $mail->setFrom('security@onedayshop.in', 'OneDayShop');
        // $message = "<a href='http://192.168.1.111/reset.php?email=$email&uniq=$rand'>Click Here</a>";
        include_once 'register_message.php';


        $mail->addAddress($email, 'ff');     // Add a recipient
        // $mail->addAddress('ellen@example.com');               // Name is optional
        // $mail->addReplyTo('info@kiddogardener.com', 'Information');
        // $mail->addCC('cc@example.com');
        // $mail->addBCC('bcc@example.com');

        // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
        // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
        $mail->isHTML(true);                                 // Set email format to HTML

        $mail->Subject = 'Welcome to OneDay Shop';
        $mail->Body    = $message;
        if(!$mail->send()) {
            echo 'Message could not be sent.';
            Redirect::to('account/success/yes');
            // echo 'Mailer Error: ' . $mail->ErrorInfo;
        } else { ?>
          <script type="text/javascript">
            window.location = 'account/success/yes';
          </script>
          // echo "string";
          // Redirect::to('account/success/yes');
          // $email_success = 'An Password Reset Link has been sent to your E Mail';
         <?php }

      } catch (Exception $e) {
        echo $e;
      }
    } else {
      foreach ($val_validate->errors() as $error) {
        array_push($errors, $error);
      }
    }
  }
?>

<div class="col-lg-6">
  <div class="account-heading mb-25">
    <h2>Register</h2>
  </div>
  <div class="account-form form-style p-20 mb-30 bg-fff box-shadow">
    <form action="" method="post">
      <b>Username <span>*</span></b>
      <input required type="text" name="reg_username" id="username" value="<?php echo $_POST['username']; ?>" >
      <br>
      <?php if (in_array("username is Required", $errors)): ?>
        <span class="text-warning">User Name is Required</span>
      <?php elseif (in_array("username must be minimum of 3 characters", $errors)): ?>
        <span class="text-warning">User Name Must be atlest 3 Charaters</span>
      <?php elseif (in_array("username already exists", $errors)): ?>
        <span class="text-warning">User Name already exists</span>
      <?php endif; ?>
      <b>Email address  <span>*</span></b>
      <input required type="email" name="email" value="<?php echo $_POST['email']; ?>">

      <b>Password <span>*</span></b>
      <input required type="password" name="reg_password" id="password">
      <br>
      <?php if (in_array("password is Required", $errors)): ?>
        <span class="text-warning">Password is Required</span>
      <?php elseif (in_array("password must be minimum of 3 characters", $errors)): ?>
        <span class="text-warning">Password Must be atlest 3 Charaters</span>
      <?php elseif (in_array("password already exists", $errors)): ?>
        <span class="text-warning">Password already exists</span>
      <?php endif; ?>
      <b>Phone <span>*</span></b>
      <input type="number" name="phone">

      <div class="login-button">
        <button name="register">Register</button>
      </div>
    </form>
  </div>
</div>
<html class="no-js" lang="">


<head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Account</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- Favicon -->
		<link rel="shortcut icon" type="image/x-icon" href="<?php echo $uri; ?>/img/favicon.ico">

		<!-- All css files are included here. -->
		<!-- animate css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/animate.min.css">
		<!-- Bootstrap fremwork main css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/bootstrap.min.css">
		<!-- font-awesome css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/font-awesome.min.css">
		<!-- nivo-slider css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/nivo-slider.css">
		<!-- owl carousel css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/owl.carousel.min.css">
		<!-- icofont css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/icofont.css">
		<!-- meanmenu css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/meanmenu.css">
		<!-- jquery-ui css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/jquery-ui.min.css">
		<!-- magnific-popup css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/magnific-popup.css">
		<!-- percircle css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/percircle.css">
		<!-- Theme main style -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/style.css">
		<!-- Responsive css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/responsive.css">

		<!-- Modernizr JS -->
        <script src="<?php echo $uri; ?>/js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="<?php echo $uri; ?>/http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

		<!-- header start -->

		<!-- header end -->
		<div class="main-container shop-bg">
			<div class="container">

          <?php include_once 'account'; ?>
				</div>
			</div>
		</div>
		<!-- footer-area start -->
		<?php include_once 'footer.php'; ?>
    </body>


</html>
