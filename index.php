<?php
  require_once 'core/init.php';

  // include 'classes/simpleUrl.php';

  $url = new simpleUrl('/shop');

  if (!$url->segment(2))
    $page = 'index';
  else
    $page = $url->segment(2);

  switch ($page) {
    case 'index.php':
      require_once 'home.php';
      break;
    case 'index':
      require_once 'home.php';
      break;
    case 'home':
      require_once 'home.php';
      break;
    case 'product':
      require_once 'product.php';
      break;
    case 'products':
      require_once 'category.php';
      break;
    case 'search':
      require_once 'search.php';
      break;
    case 'account':
      require_once 'account.php';
      break;
    case 'orders':
      require_once 'my_orders.php';
      break;
    case 'cart':
      require_once 'cart.php';
      break;
    case 'checkout':
      require_once 'checkout.php';
      break;
    case 'address':
      require_once 'addres.php';
      break;
    case 'profile':
      require_once '404.php';
      break;
    case 'forget':
      require_once 'forget.php';
      break;
    case 'signout':
      require_once 'signout.php';
      break;

    default:
      require_once '404.php';
      break;
  }
