<?php

    require_once 'core/init.php';

    $link = new Link();

    $table = 'cart';
    $table1 = 'sales';
      if (isset($_POST['remove'])) {
        $uniq = $_POST['uniq'];
        try {
          $link->delete_cart($table, $uniq);
          $link->delete_cart($table1, $uniq);
        } catch (Exception $e) {
          die($e);
        }

      }
?>
<!doctype html>
<html class="no-js" lang="">

<!-- Mirrored from devitems.com/html/oneclick-preview/oneclick/cart.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 17 Jan 2017 10:59:16 GMT -->
<head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Cart</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- Favicon -->
		<link rel="shortcut icon" type="image/x-icon" href="<?php echo $uri; ?>/img/favicon.ico">

		<!-- All css files are included here. -->
		<!-- animate css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/animate.min.css">
		<!-- Bootstrap fremwork main css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/bootstrap.min.css">
		<!-- font-awesome css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/font-awesome.min.css">
		<!-- nivo-slider css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/nivo-slider.css">
		<!-- owl carousel css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/owl.carousel.min.css">
		<!-- icofont css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/icofont.css">
		<!-- meanmenu css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/meanmenu.css">
		<!-- jquery-ui css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/jquery-ui.min.css">
		<!-- magnific-popup css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/magnific-popup.css">
		<!-- percircle css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/percircle.css">
		<!-- Theme main style -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/style.css">
		<!-- Responsive css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/responsive.css">

		<!-- Modernizr JS -->
        <script src="<?php echo $uri; ?>/js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
		<!-- header start -->
		<header>

      <?php include_once 'header.php'; ?>

      <?php include_once 'menu.php'; ?>

		</header>
		<!-- header end -->
		<!-- cart-area start -->
		<div class="cart-main-container shop-bg">
			<div class="cart-area">
				<div class="container">
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="woocommerce-breadcrumb mtb-15">
								<div class="menu">
									<ul>
										<li><a href="<?php echo $uri; ?>/index.php">Home</a></li>
										<li class="active"><a href="<?php echo $uri; ?>/search.php">Search</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
          <div class="row">
  					<!-- product-vew area start -->
  					<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
  						<div class="tab-area">
  							<div class="tab-menu-area bg-fff mb-30 box-shadow">
  								<div class="row">
  									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
  										<div class="shop-tab-menu">
  											<ul>
  												<li><a href="<?php echo $uri; ?>/#tab1" data-toggle="tab"><i class="fa fa-th-list"></i></a></li>
  												<li><a href="<?php echo $uri; ?>/#tab2" data-toggle="tab"><i class="fa fa-th-list"></i></a></li>
  											</ul>
  										</div>
  									</div>
                    <?php
                      $term = $_POST['search'];
                      $search_result = DB::getInstance()->query("SELECT * FROM `product` WHERE `name` LIKE CONCAT('%', '$term' ,'%') AND `status` = 'on'");
                      // var_dump($search_result);
                    ?>
  									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
  										<div class="shop-pra text-center">
  											<p>Showing <?php echo $search_result->count(); ?> results</p>
  										</div>
  									</div>
  									<!-- <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
  										<div class="woocommerce-ordering text-center">
  											<select name="orderby">
  												<option value="menu_order" selected="selected">Default sorting</option>
  												<option value="popularity">Sort by popularity</option>
  												<option value="rating">Sort by average rating</option>
  												<option value="date">Sort by newness</option>
  												<option value="price">Sort by price: low to high</option>
  												<option value="price-desc">Sort by price: high to low</option>
  											</select>
  										</div>
  									</div> -->
  								</div>
  							</div>
  							<div class="tab-content">
  								<div role="tabpanel" class="tab-pane active" id="tab1">
  									<div class="row">
                      <?php foreach ($search_result->results() as $search): ?>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
    											<div class="product-wrapper bg-fff mb-30">
    												<div class="product-img" style="margin-top: 15px;">
    													<a href="<?php echo $uri; ?>/product/<?php echo $search->name . '/' . $search->id; ?>">
                                <?php if (file_exists('resized/'.$search->image)): ?>
                                  <img src="<?php echo $uri; ?>/resized/<?php echo $search->image; ?>" alt="<?php echo $search->name; ?>" />
                                <?php else: ?>
                                  <img src="<?php echo $uri; ?>/images/<?php echo $search->image; ?>" alt="<?php echo $search->name; ?>" />
                                <?php endif; ?>
    													</a>
    												</div>
    												<div class="product-content">
    													<h3><a href="<?php echo $uri; ?>/product/<?php echo $search->name . '/' . $search->id; ?>"><?php echo $search->name; ?></a></h3>
    													<ul>
    														<li><i class="fa fa-star"></i></li>
    														<li><i class="fa fa-star"></i></li>
    														<li><i class="fa fa-star"></i></li>
    														<li><i class="fa fa-star"></i></li>
    														<li><i class="fa fa-star"></i></li>
    													</ul>
    													<span><?php echo $search->price; ?></span>
    												</div>
    											</div>
    										</div>
                      <?php endforeach; ?>















  									</div>
  								</div>
  								<div role="tabpanel" class="tab-pane fade" id="tab2">
  									<div class="row">
  										<!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  											<div class="product-wrapper bg-fff mb-30 ptb-20 box-shadow">
  												<div class="product-img shop-product-img">
  													<a href="<?php echo $uri; ?>/#">
  														<img src="<?php echo $uri; ?>/img/product/11.jpg" alt="" class="primary"/>
  														<img src="<?php echo $uri; ?>/img/product/12.jpg" alt="" class="secondary"/>
  													</a>
  												</div>
  												<div class="product-content shop-product-content">
  													<h3><a href="<?php echo $uri; ?>/#">Etiam gravida</a></h3>
  													<span>&300.00</span>
  													<ul>
  														<li><i class="fa fa-star"></i></li>
  														<li><i class="fa fa-star"></i></li>
  														<li><i class="fa fa-star"></i></li>
  														<li><i class="fa fa-star"></i></li>
  														<li><i class="fa fa-star"></i></li>
  													</ul>
  													<p>On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs</p>
  													<div class="shop-product-icon c-fff hover-bg">
  														<ul>
  															<li class="active"><a href="<?php echo $uri; ?>/#" data-toggle="tooltip" title="Add to cart">Add to cart</a></li>
  															<li><a href="<?php echo $uri; ?>/#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a></li>
  															<li><a href="<?php echo $uri; ?>/#" data-toggle="tooltip" title="Compare"><i class="fa fa-comments"></i></a></li>
  															<li><a href="<?php echo $uri; ?>/#" data-toggle="tooltip" title="Accumsan eli"><i class="fa fa-search"></i></a></li>
  														</ul>
  													</div>
  												</div>
  											</div>
  										</div> -->
  									</div>
  								</div>
  								<!--- woocommerce-pagination-area -->
  								<!-- <div class="row">
  									<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
  										<div class="woocommerce-pagination-area bg-fff box-shadow ptb-20 mb-30">
  											<div class="woocommerce-pagination text-center hover-bg">
  												<ul>
  													<li class="active"><a href="<?php echo $uri; ?>/#">1</a></li>
  													<li><a href="<?php echo $uri; ?>/#">2</a></li>
  													<li><a href="<?php echo $uri; ?>/#"><i class="fa fa-chevron-right"></i></a></li>
  												</ul>
  											</div>
  										</div>
  									</div>
  								</div> -->
  							</div>
  						</div>
  					</div>
  				</div>
				</div>
			</div>
		</div>
		<!-- footer-area start -->
		<?php include_once 'footer.php'; ?>
		<!-- footer-area end -->
    </body>

<!-- Mirrored from devitems.com/html/oneclick-preview/oneclick/cart.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 17 Jan 2017 10:59:16 GMT -->
</html>
