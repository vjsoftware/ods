<?php
include_once 'core/init.php';

$link = new Link();

$table = 'sales';
$table2 = 'cart';
$table_new = 'order_id';

$coupoun_error = [];

$order_id = 'OR-'.rand(11111, 999999999);
$date = date('d-m-Y');
$time = date('h:i A');

$user_id = $link->data()->id;
  if (isset($_POST['update_cart'])) {
    $coupoun = $_POST['coupoun'];
    $update_ids = $_POST['update_id'];
    $update_uniq = $_POST['update_uniq'];
    $update_qty = $_POST['qty'];
    $array_push = [];
    foreach ($update_uniq as $key => $value) {
      $array_push[$value] = $update_ids[$key];
    }
    // var_dump($_POST['order_id']);
    $order_id_array_filtered = array_filter($_POST['order_id']);
    $arr = $_POST['order_id'];
    $arr_unique = array_unique($arr);
    $arr_duplicates = array_diff_assoc($arr, $arr_unique);
    $final_order_id = $arr_duplicates;
    $order_id_array = $_POST['order_id'];
    $order_id_array_sin = $order_id_array[0];
    $update_uniq_exp = implode(',', $_POST['update_uniq']);
    if (isset($_POST['coupoun']) && !empty($_POST['coupoun'])) {
      $products_list_categ = [];
      $coupoun_check = DB::getInstance()->query("SELECT *  FROM `coupons` WHERE `code` = '$coupoun'");
      $coupon_category = $coupoun_check->first()->category;
      $coupon_product = $coupoun_check->first()->product;
      if ($date > strtotime($coupoun_check->first()->to)) {
        $between = 'no';
      } else {
        $between = 'yes';
      }
      $coupon_status = $coupoun_check->first()->status;
      $coupn_count = $coupoun_check->count();
      array_push($products_list_categ, $coupon_product);
      $prod_chek = DB::getInstance()->query("SELECT * FROM `product` WHERE `category` IN ($coupon_category) AND `status` = 'on'");
      foreach ($prod_chek->results() as $prods) {
        array_push($products_list_categ, $prods->id);
      }
      $update_product_ids = array_intersect($products_list_categ, $update_ids);
      foreach ($array_push as $key => $value) {
        if (!in_array($value, $update_product_ids)) {
          unset($array_push[$key]);
        }
      }
      // print_r($array_push);
      // echo $coupn_count;
      if ($coupn_count == 0 || $coupon_status == 'off' || $between == 'no') {
        array_push($coupoun_error, 'Coupon InValid');
      }
      if (empty($coupoun_error)) {
        if (empty($order_id_array_filtered)) {
          try {
            $link->add([
              'user_id' => $user_id,
              'order_id' => $order_id,
              'uniqs' => $update_uniq_exp,
              'coupoun' => $coupoun,
              'date' => $date,
              'time' => $time
            ], $table_new);
            foreach ($array_push as $ids => $key) {

              try {
                $link->update_sales([
                  'order_id' => $order_id,
                  'coupoun' => $coupoun
                ], $table, $ids);
                try {
                  $link->update_sales([
                    'order_id' => $order_id,
                    'coupoun' => $coupoun
                  ], $table2, $ids);
                  array_push($coupoun_error, 'Coupon Success');
                } catch (Exception $e) {
                  die($e);
                }
              } catch (Exception $e) {
                die($e);
              }
            }

            foreach ($update_uniq as $key => $value) {
              try {
                $link->update_sales([
                  'order_id' => $order_id,
                  'qty' => $update_qty[$key]
                ], $table, $value);
                try {
                  $link->update_sales([
                    'order_id' => $order_id,
                    'qty' => $update_qty[$key]
                  ], $table2, $value);
                } catch (Exception $e) {
                  die($e);
                }
              } catch (Exception $e) {
                die($e);
              }
            }
          } catch (Exception $e) {
            die($e);
          }
        } else {
          try {
            $link->update_order_id([
              'order_id' => $final_order_id,
              'uniqs' => $update_uniq_exp,
              'coupoun' => $coupoun,
              'date' => $date,
              'time' => $time
            ], $table_new, $final_order_id);
            foreach ($array_push as $ids => $key) {
              try {
                $link->update_sales([
                  'order_id' => $order_id,
                  'coupoun' => $coupoun
                ], $table, $ids);
                try {
                  $link->update_sales([
                    'order_id' => $order_id,
                    'coupoun' => $coupoun
                  ], $table2, $ids);
                  array_push($coupoun_error, 'Coupon Success');
                } catch (Exception $e) {
                  die($e);
                }
              } catch (Exception $e) {
                die($e);
              }
            }
            foreach ($update_uniq as $key => $value) {
              try {
                $link->update_sales([
                  'order_id' => $order_id,
                  'qty' => $update_qty[$key]
                ], $table, $value);
                try {
                  $link->update_sales([
                    'order_id' => $order_id,
                    'qty' => $update_qty[$key]
                  ], $table2, $value);
                } catch (Exception $e) {
                  die($e);
                }
              } catch (Exception $e) {
                die($e);
              }
            }
          } catch (Exception $e) {
            die($e);
          }
        }
      } else {
        # Coupon Not Valid
      }

    } else {

        foreach ($update_uniq as $key => $value) {
          try {
            $link->update_sales([
              'order_id' => $order_id,
              'qty' => $update_qty[$key]
            ], $table, $value);

            try {
              $link->update_sales([
                'order_id' => $order_id,
                'qty' => $update_qty[$key]
              ], $table2, $value);
              // echo "<pre>";
              // print_r($update_uniq);
              // echo "</pre>";
              // die(var_dump($update_uniq));
              array_push($coupoun_error, 'Updated Success');
            } catch (Exception $e) {
              die($e);
            }
          } catch (Exception $e) {
            die($e);
          }
        }
    }
  }



?>

<!doctype html>
<html class="no-js" lang="">

<!-- Mirrored from devitems.com/html/oneclick-preview/oneclick/checkout.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 17 Jan 2017 10:59:16 GMT -->
<head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Cart | OneDayShop</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- Favicon -->
		<link rel="shortcut icon" type="image/x-icon" href="<?php echo $uri; ?>/img/favicon.ico">

		<!-- All css files are included here. -->
		<!-- animate css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/animate.min.css">
		<!-- Bootstrap fremwork main css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/bootstrap.min.css">
		<!-- font-awesome css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/font-awesome.min.css">
		<!-- nivo-slider css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/nivo-slider.css">
		<!-- owl carousel css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/owl.carousel.min.css">
		<!-- icofont css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/icofont.css">
		<!-- meanmenu css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/meanmenu.css">
		<!-- jquery-ui css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/jquery-ui.min.css">
		<!-- magnific-popup css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/magnific-popup.css">
		<!-- percircle css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/percircle.css">
		<!-- Theme main style -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/style.css">
		<!-- Responsive css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/responsive.css">
        <link rel="stylesheet" href="<?php echo $uri; ?>/dist/sweetalert.css">
        <style media="screen">
        @media (min-width: 768px) {
          .col-lg-4 {
            width: 30%;
          }
         }

          /* Medium devices (desktops, 992px and up) */
          @media (min-width: 992px) {
            .col-lg-4 {
              width: 30%;
            }
           }

          /* Large devices (large desktops, 1200px and up) */
          @media (min-width: 1200px) {
            .col-lg-4 {
              width: 30%;
            }
           }
        </style>

		<!-- Modernizr JS -->
        <script src="<?php echo $uri; ?>/js/vendor/modernizr-2.8.3.min.js"></script>
        <script>
    var hash = '<?php echo $hash ?>';
    function submitPayuForm() {
      if(hash == '') {
        return;
      }
      var payuForm = document.forms.payuForm;
      payuForm.submit();
    }
  </script>
    </head>
    <body onload="submitPayuForm()">
		<!-- header start -->
		<header>

      <?php include_once 'header.php'; ?>

      <?php include_once 'menu.php'; ?>

		</header>
		<!-- header end -->
		<!-- start checkout -->
		<div class="main-container">
			<div class="container">
				<div class="row" id="breadcrum">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="menu mtb-15">
							<ul>
								<li><a href="<?php echo $uri; ?>/">Home</a></li>
								<li class="active"><a href="<?php echo $uri; ?>/cart">Cart</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="checkout-title text-center mtb-20">
              <?php if ($eror == ''): ?>
                <h1 id="title">Cart</h1>
                <?php if (in_array("Coupon InValid", $coupoun_error)): ?>
                  <h1>Coupon InValid</h1>
                <?php endif; ?>
                <?php if (in_array("Coupon Success", $coupoun_error)): ?>
                  <h1>Coupon Applied Successfully</h1>
                <?php endif; ?>
                <?php if (in_array("Updated Success", $coupoun_error)): ?>
                  <h1>Updated Successfully</h1>
                <?php endif; ?>
              <?php else: ?>
                <?php echo $eror; ?>
              <?php endif; ?>
						</div>
					</div>
				</div>
        <div class="row">
          <div class="col-lg-12">
            <div class="simple-product-tab box-shadow">

              <div class="tab-content bg-fff ">
                <div class="tab-pane active" id="cart">
                  <form class="" action="" method="post">
                  <div class="row">
                    <div class="col-lg-12">

                        <div class="checkout-area review-form form-style">
                          <ul>
                            <?php
                              $products_list = DB::getInstance()->query("SELECT * FROM `cart` WHERE `user_id` = '$user_id'");
                              $total = 0;
                              $total_main = 0;
                              $uniq = [];
                              $count = $products_list->count();
                              $i = 1;
                              $count = $products_list->count();
                              $tot_qty = '';
                              $modalid = 1;
                            ?>

                            <?php foreach ($products_list->results() as $product): ?>
                              <?php $tot_qty += $product->qty; ?>
                              <?php // $total += $product->qty * $product->price; ?>
                            <?php endforeach; ?>
                            <?php foreach ($products_list->results() as $product): ?>
                              <?php $product_details = DB::getInstance()->query("SELECT * FROM `product` WHERE `id` = '$product->product_id' AND `status` = 'on'"); ?>
                                <li id="<?php echo $modalid; ?>">
                                  <div class="review mb-20" style="">
                                    <div class="checkout-content p-15 col-lg-4">
                                      <b><?php echo substr($product_details->first()->name, 0, 15); ?> -</b> <b class="pull-right modalClick" data-toggle="modal" data-target="#myModal<?php echo $modalid; ?>" style="cursor: pointer;">x</b>

                                      <div class="checkout-img">
                                        <?php if (file_exists('thumb/'.$product_details->first()->image)): ?>
                                          <img src="<?php echo $uri; ?>/thumb/<?php echo $product_details->first()->image; ?>" alt="<?php echo $product_details->first()->name; ?>" />
                                        <?php else: ?>
                                          <img src="<?php echo $uri; ?>/images/<?php echo $product_details->first()->image; ?>" alt="<?php echo $product_details->first()->name; ?>" />
                                        <?php endif; ?>
                                      </div>
                                      <p style="margin-bottom: 0;">
                                        Price: <span class="text-warning"><?php echo $product->price; ?></span>
                                        <br>
                                        <input type="hidden" name="order_id[]" value="<?php echo $product->order_id; ?>">
                                        <input type="hidden" name="update_uniq[]" value="<?php echo $product->uniq; ?>">
                                        <input type="hidden" name="update_id[]" value="<?php echo $product_details->first()->id; ?>">
                                        Qty &nbsp;&nbsp;: <input onkeypress='validate(event)' class="col-lg-3" style="float: none;" type="text" name="qty[]" value="<?php echo $product->qty; ?>" pattern=".{1,}"   required title="1 characters minimum"/>
                                        <br>
                                        Total:
                                        <?php if ($product->coupoun): ?>
                                          <del class="text-warning"><?php echo $product->qty * $product->price; ?></del>
                                          <?php
                                            $coupoun_details = DB::getInstance()->query("SELECT * FROM `coupons` WHERE `code` = '$product->coupoun'");
                                            if ($coupoun_details->count()) {
                                              $cop_type = $coupoun_details->first()->type;
                                              $cop_value = $coupoun_details->first()->value;
                                              $tot_before = $product->qty * $product->price;
                                              if ($cop_type == 1) { ?>
                                              <span> <?php echo $tot_before - $cop_value; ?>  </span>
                                              <?php $total_main += $tot_before - $cop_value; ?>
                                              <?php } elseif ($cop_type == 2) { ?>
                                              <span><?php echo $tot_before - ($tot_before * ($cop_value / 100)); ?></span>
                                              <?php $total_main += $tot_before - ($tot_before * ($cop_value / 100)); ?>
                                              <?php }
                                            }
                                          ?>
                                        <?php else: ?>
                                          <span class="text-warning"><?php echo $product->qty * $product->price; ?></span>
                                          <?php $total_main += $product->qty * $product->price; ?>
                                        <?php endif; ?>
                                      </p>
                                    </div>
                                  </div>

                                </li>
                                <!-- Modal Start -->
                                <div class="modal fade" id="myModal<?php echo $modalid; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content" style="border-radius: 0;">
                                            <div class="modal-header">
                                                <!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button> -->
                                                <h4 class="modal-title" id="myModalLabel">Request Cencellation<b class="pull-right" data-dismiss="modal" style="cursor: pointer;">X</b></h4>
                                                </div>
                                                <?php $product_details = DB::getInstance()->query("SELECT * FROM `product` WHERE `id` = '$product->product_id' AND `status` = 'on'"); ?>
                                            <div class="modal-body">
                                                <div class="row">
                                                  <div class="col-md-3">
                                                    <img src="<?php if (file_exists('thumb/'.$product_details->first()->image)): ?>
                                                      <?php echo $uri; ?>/thumb/<?php echo $product_details->first()->image; ?>" alt="<?php echo $product_details->first()->name; ?>
                                                    <?php else: ?>
                                                      <?php echo $uri; ?>/images/<?php echo $product_details->first()->image; ?>" alt="<?php echo $product_details->first()->name; ?>
                                                    <?php endif; ?>" width="80" height="80" border="0" class="img-circle"></a>
                                                  </div>
                                                  <div class="col-md-9">
                                                    <div class="col-md-4">
                                                      <span><b>Item Name</b></span>
                                                    </div>
                                                    <div class="col-md-2">
                                                      <span><b>Qty</b></span>
                                                    </div>
                                                    <div class="col-md-3">
                                                      <span><b>Total</b></span>
                                                    </div>
                                                    <div class="col-md-12">

                                                    </div>
                                                    <div class="col-md-4">
                                                      <span><?php echo $product_details->first()->name; ?> -</span>
                                                    </div>
                                                    <div class="col-md-2">
                                                      <span><?php echo $product->qty; ?></span>
                                                    </div>
                                                    <div class="col-md-3">
                                                      <span>Rs. <?php if ($product->coupoun): ?>
                                                        <?php
                                                          $coupoun_details = DB::getInstance()->query("SELECT * FROM `coupons` WHERE `code` = '$product->coupoun'");
                                                          if ($coupoun_details->count()) {
                                                            $cop_type = $coupoun_details->first()->type;
                                                            $cop_value = $coupoun_details->first()->value;
                                                            $tot_before = $product->qty * $product->price;
                                                            if ($cop_type == 1) { ?>
                                                            <span> <?php echo $tot_before - $cop_value; ?>  </span>
                                                            <?php $total += $tot_before - $cop_value; ?>
                                                            <?php } elseif ($cop_type == 2) { ?>
                                                            <span><?php echo $tot_before - ($tot_before * ($cop_value / 100)); ?></span>
                                                            <?php $total += $tot_before - ($tot_before * ($cop_value / 100)); ?>
                                                            <?php }
                                                          }
                                                        ?>
                                                      <?php else: ?>
                                                        <span class="text-warning"><?php echo $product->qty * $product->price; ?></span>
                                                        <?php $total += $product->qty * $product->price; ?>
                                                      <?php endif; ?>
                                                    </span>
                                                    </div>
                                                  </div>
                                                <!-- <span><strong>Order ID: </strong></span>
                                                    <span class="label label-warning" style="font-family: sans-serif;">87946546</span> -->
                                                </div>
                                                <hr>
                                                <div class="row">
                                                  <div class="col-md-4">
                                                    <label for="">Reason for cancellation*</label>
                                                  </div>
                                                  <div class="col-md-8">
                                                    <select class="form-control" name="reason" id="reason<?php echo $modalid; ?>" style="border-radius: 0;">
                                                      <option value="">Select</option>
                                                      <option value="1">Order Placed by Mistake</option>
                                                      <option value="2">Expected Delivery time Is too Long</option>
                                                      <option value="3">I'm not Available at Given Delivery Time</option>
                                                      <option value="4">Price Of Product is High</option>
                                                      <option value="5">Others</option>
                                                    </select>
                                                    <div class="text-warning hidden" id="hide<?php echo $modalid; ?>">
                                                      This Field is Required
                                                    </div>
                                                  </div>
                                                  <div class="col-md-12">
                                                    <br>
                                                  </div>
                                                  <div class="col-md-4">
                                                    <label for="">Comments</label>
                                                  </div>
                                                  <div class="col-md-8">
                                                    <textarea class="form-control" name="comment" id="comment<?php echo $modalid; ?>" rows="3" style="border-radius: 0; height: 75px;"></textarea>
                                                    <input type="hidden" name="delete_id" id="delete_id<?php echo $modalid; ?>" value="<?php echo $modalid; ?>">
                                                    <input type="hidden" name="delete_order_id" id="delete_order_id<?php echo $modalid; ?>" value="<?php echo $product->uniq; ?>">
                                                  </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <div class="col-md-6">

                                                </div>
                                                <div class="col-md-2">
                                                    <button type="button" data-dismiss="modal">Close</button>
                                                </div>
                                                <div class="col-md-4">
                                                  <button class="cancel_order" type="button" onclick="delete_item(<?php echo $modalid; ?>)" value="<?php echo $modalid; ?>">Confirm Cancel</button>
                                                  <!-- <span class="cancel_order">Confirm Cancel</span> -->
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Modal End -->
                              <?php array_push($uniq, $product->uniq); ?>

                              <?php $modalid++; ?>
                            <?php endforeach; ?>
                            <div class="col-lg-12">

                            </div>
                            <li>
                              <div class="review mb-20" style="">
                                <div class="col-md-7">

                                </div>
                                <div class="checkout-content p-15 col-md-4">
                                  <b>Total Amount -</b>

                                  <p style="margin-bottom: 0;">Items : <span class="pull-right"><b id="items"><?php echo $count; ?></b></span><br> Units &nbsp;: <span class="pull-right"><b id="items_qty"><?php echo $tot_qty; ?></b></span> <br>Total &nbsp;: <span class="pull-right"><b id="total_amount"><?php echo $total_main; ?></b></span></p>
                                </div>
                              </div>
                            </li>

                          </ul>

                            <div class="col-lg-7"> </div>

                              <div class="col-lg-2">
                                <input class="form-control" type="text" name="coupoun" value="" placeholder="Coupon Code"><br>
                              </div>
                              <div class="col-lg-2">
                                <button type="submit" name="update_cart">Update Cart ...!</button>
                                <!-- <input class="btn btn-warning" type="submit" name="update_cart" value="Update Cart"> -->
                              </div>

                            <div class="col-lg-12">
                            </div>
                            <div class="col-lg-9">

                            </div>
                            <div class="col-lg-3">
                              <a class="btn btn-warning" href="<?php echo $uri; ?>/checkout">Proceed to Checkout</a>

                            </div>
                            <div class="col-lg-12">
                              <br>
                            </div>

                        </div>


                    </div>
                  </div>
                  </form>


                </div>


              </div>
            </div>
          </div>
        </div>
        <br>


        <div class="col-md-12">
          <br><br>
        </div>
			</div>
		</div>

		<!-- footer-area start -->
		<?php include_once 'footer.php'; ?>
    <script type="text/javascript">
      $('.modalClick').click(function() {
        $('#menu_sticky').removeClass('menu_sticky');
        $('#menu_sticky').removeClass('sticky');
        $('#menu_sticky').removeAttr("style");
      })
    </script>
    <script src="<?php echo $uri; ?>/dist/sweetalert.min.js"> </script>
		<!-- footer-area end -->

    <script type="text/javascript">
    // Order Cancel Start
      function delete_item(orderId) {
        // var tt = $(this).val();
        // console.log(orderId);
        var res = "#reason" + orderId;
        var reason = $(res).val();
        var comment = $('#comment'+orderId).val();
        var delete_id = $('#delete_id'+orderId).val();
        var delete_order_id = $('#delete_order_id'+orderId).val();
        var hide = '#hide'+orderId;
        if (reason != '') {
          $(hide).addClass('hidden');
          $.ajax({
                  url: "cancel_order.php",
                  type: "post",
                  dataType : 'json',
                  data: {reason: reason, comment: comment, delete_id: delete_id, delete_order_id: delete_order_id} ,
                  success: function (response) {
                     // you will get response from your php page (what you echo or print)
                    //  console.log("Success");
                    //  console.log(response.delete_id);
                     $('#'+response.delete_id).remove();
                     $('#myModal'+response.delete_id).modal('toggle');
                     swal("Order Canceled Successfully", "Continue ...!", "success");
                     var items = $('#items').text();
                     var total_amount = $('#total_amount').text();
                     var total_amount_float = parseFloat(total_amount) || 0;
                     var items_qty = $('#items_qty').text();
                     var items_qty_int = parseInt(items_qty) || 0;
                     $('#items').text(items - 1);
                     $('#items_qty').text(response.qty);
                     $('#total_amount').text(response.less);
                    //  console.log(response.less);
                  },
                  error: function(jqXHR, textStatus, errorThrown) {
                     console.log(textStatus, errorThrown);
                  }


              });
        } else {
          $(hide).removeClass('hidden');
        }


        // var test = $('.cancel_order').val();
        // console.log(reason);
      };
      // $('.cancel_order').click(function() {
      //   // alert('op');
      //   // event.preventDefault();
      //   // var reason = $('#reason').val();
      //   var reason = $( ".reason option:selected" ).val();
      //   var comment = $('.comment').val();
      //   var delete_id = $('.delete_id').val();
      //   var delete_order_id = $('.delete_order_id').val();
      //
      //   var test = $('.cancel_order').val();
      //   console.log(test);
      //
      //
      //   // $.ajax({
      //   //         url: "cancel_order.php",
      //   //         type: "post",
      //   //         dataType : 'json',
      //   //         data: {reason: reason, comment: comment, delete_id: delete_id, delete_order_id: delete_order_id} ,
      //   //         success: function (response) {
      //   //            // you will get response from your php page (what you echo or print)
      //   //            console.log(response.delete_id);
      //   //            $('#'+response.delete_id).remove();
      //   //            $('#myModal'+response.delete_id).modal('toggle');
      //   //
      //   //         },
      //   //         error: function(jqXHR, textStatus, errorThrown) {
      //   //            console.log(textStatus, errorThrown);
      //   //         }
      //   //
      //   //
      //   //     });
      // });
    // Order Cancel End
    </script>
    </body>
    <script type="text/javascript">
    $("#cart, #credit, #cod").click(function() {
      // alert('op');
      $('html,body').animate({
        scrollTop: $("#breadcrum").offset().top},
        'slow');
      });
      </script>
      <script>
        function validate(evt) {
          var theEvent = evt || window.event;
          var key = theEvent.keyCode || theEvent.which;
          key = String.fromCharCode( key );
          var regex = /[0-9]|\./;
          if( !regex.test(key) ) {
            theEvent.returnValue = false;
            if(theEvent.preventDefault) theEvent.preventDefault();
          }
        }


      </script>

<!-- Mirrored from devitems.com/html/oneclick-preview/oneclick/checkout.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 17 Jan 2017 10:59:16 GMT -->
</html>
