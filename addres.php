<?php
  include_once 'core/init.php';

  $link = new Link();

  $user_id = $link->data()->id;
  $address_list = DB::getInstance()->query("SELECT * FROM `address` WHERE `user_id` = '$user_id'");
  if ($address_list->count()) {
    redirect::to('index');
  }

  $date = date('d-m-Y');
  $time = date('h:i A');
  $table = 'address';

  $ret = $link->data()->id;
  $ret_res = DB::getInstance()->query("SELECT * FROM `users` where `id`='$ret'");
  $id = $ret_res->first()->id;


  if (isset($_POST['add'])) {
    $validate = new Validate();
    $val_validate = $validate->check($_POST, [
      'address' => [
        'required' => TRUE
      ]
    ]);

    if ($val_validate->passed()) {
      try {
        $link->add([
          'user_id' => $id,
          'address' => $_POST['address'],
          'address_street' => $_POST['address_street'],
          'town' => $_POST['town'],
          'pincode' => $_POST['pincode'],
          'date' => $date,
          'time' => $time
        ], $table);
        Redirect::to('index.php');
      } catch (Exception $e) {
        echo $e;
      }
    } else {

      }
  }
?>

<!doctype html>
<html class="no-js" lang="">

<!-- Mirrored from devitems.com/html/oneclick-preview/oneclick/checkout.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 17 Jan 2017 10:59:16 GMT -->
<head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Address</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- Favicon -->
		<link rel="shortcut icon" type="image/x-icon" href="<?php echo $uri; ?>/img/favicon.ico">

		<!-- All css files are included here. -->
		<!-- animate css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/animate.min.css">
		<!-- Bootstrap fremwork main css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/bootstrap.min.css">
		<!-- font-awesome css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/font-awesome.min.css">
		<!-- nivo-slider css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/nivo-slider.css">
		<!-- owl carousel css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/owl.carousel.min.css">
		<!-- icofont css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/icofont.css">
		<!-- meanmenu css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/meanmenu.css">
		<!-- jquery-ui css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/jquery-ui.min.css">
		<!-- magnific-popup css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/magnific-popup.css">
		<!-- percircle css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/percircle.css">
		<!-- Theme main style -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/style.css">
		<!-- Responsive css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/responsive.css">

		<!-- Modernizr JS -->
        <script src="<?php echo $uri; ?>/js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
		<!-- header start -->
		<header>

			<?php include_once 'header.php'; ?>

      <?php include_once 'menu.php'; ?>
		</header>
		<!-- header end -->
		<!-- start checkout -->
		<div class="main-container">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="menu  mtb-15">
							<ul>
								<li><a href="<?php echo $uri; ?>/index.php">Home</a></li>
								<li class="active"><a href="<?php echo $uri; ?>/addres.php">Address</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="row">


					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="checkout-title text-center mtb-20">
              <h1>Address</h1>
						</div>
					</div>
				</div>

        <div class="customer-details-area">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
							<div class="customer-details mb-50">

								<div class="customer-details-form account-form p-20 clear">
									<form action="" method="post">
										<span>
											<b>Address <span class="required">*</span></b>
											<input type="text" placeholder="Door Number Etc..." name="address"/>
										</span>
										<span>
											<input type="text" placeholder="Street Name, Etc..." name="address_street"/>
										</span>
										<span>
											<b>Town/City <span class="required">*</span></b>
											<input type="text" placeholder="City Name" name="town"/>
                    </span>
										<span>
											<b>PinCode <span class="required">*</span></b>
											<input type="text" placeholder="Pincode" name="pincode"/>
                    </span>
                    <div class="login-button">
                      <button name="add">Add</button>
                    </div>
									</form>
								</div>
							</div>
						</div>

					</div>
				</div>

			</div>
		</div>
		<!-- footer-area start -->
		<?php include_once 'footer.php'; ?>
		<!-- footer-area end -->
    </body>

<!-- Mirrored from devitems.com/html/oneclick-preview/oneclick/checkout.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 17 Jan 2017 10:59:16 GMT -->
</html>
