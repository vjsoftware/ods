<?php
  include_once 'core/init.php';
  $link = new Link();
  $mail = new PHPMailer();
  $table = 'users';
  $errors = [];

  if ($link->isloggedIn()) {
     redirect::to('index.php');
   }

  if (isset($_POST['reset'])) {

    $email = $_POST['email'];

    $email_check = DB::getInstance()->query("SELECT * FROM `users` WHERE `email` = '$email'");
    if ($email_check->count()) {
      // die('Ok');
      $update_id = $email_check->first()->id;
      $user_name = $email_check->first()->username;
      $rand = generateRandomString();

      try {
        $link->update([
          'reset' => $rand
        ], $update_id);
        //$mail->SMTPDebug = 3;                               // Enable verbose debug output

        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'lio.boxsecured.com';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'security@onedayshop.in';                 // SMTP username
        $mail->Password = '@m^{c6dweH*]';                           // SMTP password
        $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 465;                                    // TCP port to connect to

        $email = $_POST['email'];
        $mail->setFrom('security@onedayshop.in', 'One Day Shop');
        // $message = "<a href='http://192.168.1.111/reset.php?email=$email&uniq=$rand'>Click Here</a>";
        include_once 'reset_message.php';


        $mail->addAddress($email, 'ff');     // Add a recipient
        // $mail->addAddress('ellen@example.com');               // Name is optional
        // $mail->addReplyTo('info@kiddogardener.com', 'Information');
        // $mail->addCC('cc@example.com');
        // $mail->addBCC('bcc@example.com');

        // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
        // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
        $mail->isHTML(true);                                 // Set email format to HTML

        $mail->Subject = 'Change Password';
        $mail->Body    = $message;

        if(!$mail->send()) {
            // echo 'Message could not be sent.';
            // echo 'Mailer Error: ' . $mail->ErrorInfo;
        } else {
            $email_success = 'An Password Reset Link has been sent to your E Mail';
        }
      } catch (Exception $e) {

      }


    } else {
      $email_not_valid = "Email Address Not Valid";
    }
  }

  function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
?>
<!doctype html>
<html class="no-js" lang="">


<head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Account</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- Favicon -->
		<link rel="shortcut icon" type="image/x-icon" href="<?php echo $uri; ?>/img/favicon.ico">

		<!-- All css files are included here. -->
		<!-- animate css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/animate.min.css">
		<!-- Bootstrap fremwork main css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/bootstrap.min.css">
		<!-- font-awesome css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/font-awesome.min.css">
		<!-- nivo-slider css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/nivo-slider.css">
		<!-- owl carousel css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/owl.carousel.min.css">
		<!-- icofont css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/icofont.css">
		<!-- meanmenu css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/meanmenu.css">
		<!-- jquery-ui css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/jquery-ui.min.css">
		<!-- magnific-popup css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/magnific-popup.css">
		<!-- percircle css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/percircle.css">
		<!-- Theme main style -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/style.css">
		<!-- Responsive css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/responsive.css">

		<!-- Modernizr JS -->
        <script src="<?php echo $uri; ?>/js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

		<!-- header start -->
    <?php include_once 'slider.php'; ?>

		<!-- header end -->
		<div class="main-container shop-bg">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="woocommerce-breadcrumb mtb-15">
							<div class="menu">
								<ul>
									<li><a href="<?php echo $uri; ?>/index.php">Home</a></li>
									<li class="active"><a href="<?php echo $uri; ?>/account">Forget Password</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="account-title mb-20 text-center">
							<h1>Forget Password</h1>
						</div>
					</div>
          <?php if ($email_success): ?>
            <div class="account-title mb-20 text-center">
              <h1><?php echo $email_success; ?></h1>
            </div>
          <?php elseif ($email_not_valid): ?>
            <div class="account-title mb-20 text-center">
              <h1><?php echo $email_not_valid; ?></h1>
            </div>
          <?php else: ?>
            <div class="col-lg-6">
              <div class="account-heading mb-25">
                <h2>Recovery</h2>
              </div>
              <div class="account-form form-style p-20 mb-30 bg-fff box-shadow">
                <form action="" method="post">
                  <b style="font-family: sans-serif;">E@Mail <span>*</span></b>
                  <input required type="text" name="email" id="email">
                  <br>
                  <?php if (in_array("email is Required", $errors)): ?>
                    <span class="text-warning">User Name is Required</span>
                  <?php elseif (in_array("email must be minimum of 3 characters", $errors)): ?>
                    <span class="text-warning">Must be atlest 3 Charaters Limit</span>
                  <?php endif; ?>

                  <div class="text-warning">
                  <br>
                  </div>
                  <div class="login-button">
                  <button class="" type="submit" name="reset">RESET</button>
                </div>
                </form>

              </div>
            </div>
          <?php endif; ?>



				</div>
			</div>
		</div>


		<!-- footer-area start -->
		<?php include_once 'footer.php'; ?>
		<!-- footer-area end -->

		<!-- Placed js at the end of the document so the pages load faster -->
		<!-- jquery latest version -->
        <script src="<?php echo $uri; ?>/js/vendor/jquery-1.12.4.min.js"></script>
		<!-- magnific popup js -->
        <script src="<?php echo $uri; ?>/js/jquery.magnific-popup.min.js"></script>
		<!-- mixitup js -->
        <script src="<?php echo $uri; ?>/js/jquery.mixitup.min.js"></script>
		<!-- jquery-ui price-->
        <script src="<?php echo $uri; ?>/js/jquery-ui.min.js"></script>
		<!-- ScrollUp Js -->
        <script src="<?php echo $uri; ?>/js/jquery.scrollUp.min.js"></script>
		<!-- countDown Js -->
        <script src="<?php echo $uri; ?>/js/jquery.countdown.min.js"></script>
		<!-- nivo slider js -->
        <script src="<?php echo $uri; ?>/js/jquery.nivo.slider.pack.js"></script>
		<!-- mobail menu js -->
        <script src="<?php echo $uri; ?>/js/jquery.meanmenu.js"></script>
		<!-- Bootstrap framework js -->
        <script src="<?php echo $uri; ?>/js/bootstrap.min.js"></script>
		<!-- owl carousel js -->
        <script src="<?php echo $uri; ?>/js/owl.carousel.min.js"></script>
		<!-- All js plugins included in this file. -->
        <script src="<?php echo $uri; ?>/js/plugins.js"></script>
		<!-- Main js file that contents all jQuery plugins activation. -->
        <script src="<?php echo $uri; ?>/js/main.js"></script>
    </body>


</html>
