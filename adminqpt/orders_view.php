<?php
 include_once 'core/init.php';

 $link = new Link();
  if (!$link->isLoggedIn()) {
      redirect::to('signin.php');
   }
?>


<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <title>One Day Shop Dashboard</title>
  <meta name="description" content="Responsive, Bootstrap, BS4" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <!-- for ios 7 style, multi-resolution icon of 152x152 -->
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-barstyle" content="black-translucent">
  <link rel="apple-touch-icon" href="images/logo.png">
  <meta name="apple-mobile-web-app-title" content="Flatkit">
  <!-- for Chrome on Android, multi-resolution icon of 196x196 -->
  <meta name="mobile-web-app-capable" content="yes">
  <link rel="shortcut icon" sizes="196x196" href="images/logo.png">

  <!-- style -->
  <link rel="stylesheet" href="css/animate.css/animate.min.css" type="text/css" />
  <link rel="stylesheet" href="css/glyphicons/glyphicons.css" type="text/css" />
  <link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css" type="text/css" />
  <link rel="stylesheet" href="css/material-design-icons/material-design-icons.css" type="text/css" />
  <link rel="stylesheet" href="css/ionicons/css/ionicons.min.css" type="text/css" />
  <link rel="stylesheet" href="css/simple-line-icons/css/simple-line-icons.css" type="text/css" />
  <link rel="stylesheet" href="css/bootstrap/dist/css/bootstrap.min.css" type="text/css" />

  <!-- build:css css/styles/app.min.css -->
  <link rel="stylesheet" href="css/styles/app.css" type="text/css" />
  <link rel="stylesheet" href="css/styles/style.css" type="text/css" />
  <!-- endbuild -->
  <link rel="stylesheet" href="css/styles/font.css" type="text/css" />
</head>
<body>
  <div class="app" id="app">

<!-- ############ LAYOUT START-->

  <!-- aside -->
  <?php include_once 'sidebar.php'; ?>
  <!-- / -->

  <!-- content -->
  <div id="content" class="app-content box-shadow-z2 bg pjax-container" role="main">

    <div class="app-header white bg b-b">
          <div class="navbar" data-pjax>
                <a data-toggle="modal" data-target="#aside" class="navbar-item pull-left hidden-lg-up p-r m-a-0">
                  <i class="ion-navicon"></i>
                </a>
                <div class="navbar-item pull-left h5" id="pageTitle">Orders</div>
                <!-- nabar right -->
                <?php include_once 'head.php'; ?>
                <!-- / navbar right -->
          </div>
    </div>

    <?php include_once 'footer.php'; ?>
    <div class="app-body">

<!-- ############ PAGE START-->
<div class="padding">
    <!-- <i class="material-icons md-24">(.*)</i>   <em>$1</em>-->
    <div class="box nav-active-border b-info">
      <ul class="nav nav-md">
        <li class="nav-item inline">
          <a class="nav-link active" href="#" data-toggle="tab" data-target="#tab_1">
            <span class="text-md">Order Details</span>
            <small class="block text-muted hidden-sm-down">Order</small>
          </a>
        </li>
        <li class="nav-item inline">
          <a class="nav-link" href="#" data-toggle="tab" data-target="#tab_2">
            <span class="text-md">Payment <span class="hidden-sm-down">Details</span></span>
            <small class="block text-muted hidden-sm-down">Payment</small>
          </a>
        </li>
        <li class="nav-item inline">
          <a class="nav-link" href="#" data-toggle="tab" data-target="#tab_3">
            <span class="text-md">Shipping</span>
            <small class="block text-muted hidden-sm-down">Shipping</small>
          </a>
        </li>
        <li class="nav-item inline">
          <a class="nav-link" href="#" data-toggle="tab" data-target="#tab_4">
            <span class="text-md">Products</span>
            <small class="block text-muted hidden-sm-down">Products</small>
          </a>
        </li>
        <li class="nav-item inline">
          <a class="nav-link" href="#" data-toggle="tab" data-target="#tab_5">
            <span class="text-md">History</span>
            <small class="block text-muted hidden-sm-down">History</small>
          </a>
        </li>
      </ul>
      <div class="tab-content clear b-t">
        <div class="tab-pane active" id="tab_1">
            <div class="box-body table-responsive">
              <style media="screen">
              .table td, .table th {
                  padding: 0;
                  vertical-align: top;
                  border-top: 1px solid #eceeef;

                }
                tr td:first-child {
                    /* styles */
                    width: 150px;
                }
              </style>
              <?php
                $uniq = $_GET['id'];
                $details = DB::getInstance()->query("SELECT * FROM `sales` WHERE `uniq` = '$uniq'");
                $order_number = $details->first()->uniq;
                $user_id = $details->first()->user_id;
                $total = $details->first()->qty * $details->first()->price;
                $qty = $details->first()->qty;
                $price = $details->first()->price;
                $date = $details->first()->date;
                $product_id = $details->first()->product_id;
                $payment_method = $details->first()->status;
                $customer_details = DB::getInstance()->query("SELECT * FROM `users` WHERE `id` = '$user_id'");
                $customer_name = $customer_details->first()->username;
                $customer_email = $customer_details->first()->email;
                $customer_phone = $customer_details->first()->phone;
                $address_details = DB::getInstance()->query("SELECT * FROM `address` WHERE `user_id` = '$user_id'");
                $address = $address_details->first()->address;
                $town = $address_details->first()->town;
                $product_details = DB::getInstance()->query("SELECT * FROM `product` WHERE `id` = '$product_id'");
                $product_name = $product_details->first()->name;
              ?>
              <table class="table table-bordered">
                <tbody>
                  <tr>
                    <td>Order ID: </td>
                    <td><?php echo $order_number; ?></td>
                  </tr>
                  <tr>
                    <td>Store Name: </td>
                    <td>One Day Shop</td>
                  </tr>
                  <tr>
                    <td>Customer: </td>
                    <td><?php echo $customer_name; ?></td>
                  </tr>
                  <!-- <tr>
                    <td>Customer Group: </td>
                    <td>Test</td>
                  </tr> -->
                  <tr>
                    <td>Email: </td>
                    <td><?php echo $customer_email; ?></td>
                  </tr>
                  <tr>
                    <td>Telephone: </td>
                    <td><?php echo $customer_phone; ?></td>
                  </tr>
                  <tr>
                    <td>Total: </td>
                    <td><?php echo $total; ?></td>
                  </tr>
                  <tr>
                    <td>Order Status: </td>
                    <td>Pending</td>
                  </tr>
                  <!-- <tr>
                    <td>Ip Address: </td>
                    <td>192.168.1.10</td>
                  </tr> -->
                  <!-- <tr>
                    <td>User Agent: </td>
                    <td>Google Chrome</td>
                  </tr> -->
                  <!-- <tr>
                    <td>Accept Language: </td>
                    <td>Google Chrome</td>
                  </tr> -->
                  <tr>
                    <td>Date Added: </td>
                    <td><?php echo $date; ?></td>
                  </tr>
                  <tr>
                    <td>Date Modified: </td>
                    <td><?php echo $date; ?></td>
                  </tr>
                </tbody>
              </table>
            </div>

        </div>
        <div class="tab-pane" id="tab_2">

          <div class="box-body table-responsive">
            <table class="table table-bordered">
              <tbody>
                <tr>
                  <td>Name: </td>
                  <td><?php echo $customer_name; ?></td>
                </tr>
                <!-- <tr>
                  <td>Company: </td>
                  <td>vJsOFT</td>
                </tr> -->
                <tr>
                  <td>Address 1: </td>
                  <td><?php echo $address; ?></td>
                </tr>
                <tr>
                  <td>City: </td>
                  <td><?php echo $town; ?></td>
                </tr>
                <!-- <tr>
                  <td>Region / State: </td>
                  <td>Andhra Pradesh</td>
                </tr>
                <tr>
                  <td>Region / State Code: </td>
                  <td>AP</td>
                </tr> -->
                <tr>
                  <td>Country: </td>
                  <td>India</td>
                </tr>
                <tr>
                  <td>Payment Method: </td>
                  <td><?php if ($payment_method == 1) {
                    echo "Online Payment";
                  } elseif ($payment_method == 2) {
                    echo "Cash On Delivery";
                  } ?></td>
                </tr>
                <!-- <tr>
                  <td>Payment Method: </td>
                  <td>Cash On Delivery</td>
                </tr> -->
              </tbody>
            </table>
          </div>

        </div>

        <div class="tab-pane" id="tab_3">
          <div class="box-body table-responsive">
            <table class="table table-bordered">
              <tbody>
                <tr>
                  <td>Name: </td>
                  <td><?php echo $customer_name; ?></td>
                </tr>
                <!-- <tr>
                  <td>Company: </td>
                  <td>vJsOFT</td>
                </tr> -->
                <tr>
                  <td>Address 1: </td>
                  <td><?php echo $address; ?></td>
                </tr>
                <tr>
                  <td>City: </td>
                  <td><?php echo $town; ?></td>
                </tr>
                <!-- <tr>
                  <td>Region / State: </td>
                  <td>Andhra Pradesh</td>
                </tr>
                <tr>
                  <td>Region / State Code: </td>
                  <td>AP</td>
                </tr> -->
                <tr>
                  <td>Country: </td>
                  <td>India</td>
                </tr>
                <!-- <tr>
                  <td>Payment Method: </td>
                  <td>Cash On Delivery</td>
                </tr> -->
              </tbody>
            </table>
          </div>
        </div>
        <div class="tab-pane" id="tab_4">
          <div class="box-body table-responsive">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th>Product</th>
                  <!-- <th>Model</th> -->
                  <th>Quanity</th>
                  <th>Unit Price</th>
                  <th>Total</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td style="width: 380px; vertical-align: middle">
                    <?php echo $product_name; ?>
                  </td>
                  <!-- <td style="vertical-align: middle;">A1303</td> -->
                  <td style="vertical-align: middle;"><?php echo $qty; ?></td>
                  <td style="vertical-align: middle;">Rs. <?php echo $price; ?></td>
                  <td style="vertical-align: middle;">Rs. <?php echo $qty * $price; ?></td>
                </tr>
                <tr>
                    <td colspan="3" align="right">Sub-Total: </td>
                    <td><?php echo $qty * $price; ?></td>
                </tr>
                <tr>
                    <td colspan="3" align="right">Flat Shipping Rate: </td>
                    <td><?php echo $qty * $price; ?></td>
                </tr>
                <tr>
                    <td colspan="3" align="right">Total: </td>
                    <td><?php echo $qty * $price; ?></td>
                </tr>

              </tbody>
            </table>
          </div>
        </div>
        <div class="tab-pane" id="tab_5">
          <div class="box-body table-responsive">
            <?php $history_details = DB::getInstance()->query("SELECT * FROM `order_status` WHERE `uniq` = '$order_number'"); ?>
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th>Date Added</th>
                  <th>Comments</th>
                  <th>Status</th>
                  <th>Email</th>
                  <th>SMS</th>
                </tr>
              </thead>

                <tbody id="append">
                  <?php if ($history_details->count()): ?>
                    <?php foreach ($history_details->results() as $history): ?>
                      <tr>
                        <td style="width: 220px;"><?php echo $history->date; ?> / <?php echo $history->time; ?> </td>
                        <td><?php echo $history->comment; ?></td>
                        <td>Pending</td>
                        <td style="width: 150px;"><?php if ($history->notify == 1): ?>
                          Yes
                        <?php else: ?>
                          No
                        <?php endif; ?>
                      </td>
                        <td style="width: 150px;"><?php if ($history->notify_sms == 1): ?>
                          Yes
                        <?php else: ?>
                          No
                        <?php endif; ?>
                      </td>
                      </tr>
                    <?php endforeach; ?>
                  <?php endif; ?>
                </tbody>

            </table>


            <br>
            Order History <hr>
            <?php $history_details = DB::getInstance()->query("SELECT * FROM `order_status` WHERE `uniq` = '$order_number' ORDER BY `id` DESC"); ?>
            <form class="form-horizontal" id="input-form" method="post">
              <input type="hidden" name="uniq" value="<?php echo $order_number; ?>" id="uniq">
              <input type="hidden" name="user_idd" value="<?php echo $user_id; ?>" id="user_idd">
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-order-status">Order Status</label>
                  <div class="col-sm-10">
                    <select name="order_status_id" id="input-order-status" class="form-control">
                      <option value="0" <?php if ($history_details->first()->status == 0): ?>
                        selected
                      <?php endif; ?>>Pending</option>
                      <option value="1" <?php if ($history_details->first()->status == 1): ?>
                        selected
                      <?php endif; ?>>Processing</option>
                      <option value="2" <?php if ($history_details->first()->status == 2): ?>
                        selected
                      <?php endif; ?>>Shipped</option>
                      <option value="3" <?php if ($history_details->first()->status == 3): ?>
                        selected
                      <?php endif; ?>>Complete</option>
                      <option value="4" <?php if ($history_details->first()->status == 4): ?>
                        selected
                      <?php endif; ?>>Canceled</option>
                      <option value="5" <?php if ($history_details->first()->status == 5): ?>
                        selected
                      <?php endif; ?>>Refunded</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-12">
                  <br>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-notify">Notify Email</label>
                  <div class="col-sm-2">
                    <input type="checkbox" name="notify" id="input-notify">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-notify-sms">Notify SMS</label>
                  <div class="col-sm-2">
                    <input type="checkbox" name="notify_sms" id="input-notify-sms">
                  </div>
                </div>
                <div class="col-md-12">

                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-comment">Comment</label>
                  <div class="col-sm-10">
                    <textarea name="comment" rows="8" id="input-comment" class="form-control"></textarea>
                  </div>
                </div>
                <div class="col-md-12">
                  <br>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-submit"></label>
                  <div class="col-sm-10">
                    <input class="btn btn-warning pull-right" type="submit" name="submit" id="input-submit" value="Add Hsitory">
                  </div>
                </div>
              </form>
          </div>
        </div>
      </div>
    </div>
</div>



<!-- ############ PAGE END-->

    </div>
  </div>
  <!-- / -->


  <!-- ############ SWITHCHER START-->
    <?php include_once 'rightsidebar.php'; ?>
  <!-- ############ SWITHCHER END-->

<!-- ############ LAYOUT END-->
  </div>

<!-- build:js scripts/app.min.js -->
<!-- jQuery -->
  <script src="libs/jquery/dist/jquery.js"></script>
<!-- Bootstrap -->
  <script src="libs/tether/dist/js/tether.min.js"></script>
  <script src="libs/bootstrap/dist/js/bootstrap.js"></script>
<!-- core -->
  <script src="libs/jQuery-Storage-API/jquery.storageapi.min.js"></script>
  <script src="libs/PACE/pace.min.js"></script>
  <script src="libs/jquery-pjax/jquery.pjax.js"></script>
  <script src="libs/blockUI/jquery.blockUI.js"></script>
  <script src="libs/jscroll/jquery.jscroll.min.js"></script>

  <script src="scripts/config.lazyload.js"></script>
  <script src="scripts/ui-load.js"></script>
  <script src="scripts/ui-jp.js"></script>
  <script src="scripts/ui-include.js"></script>
  <script src="scripts/ui-device.js"></script>
  <script src="scripts/ui-form.js"></script>
  <script src="scripts/ui-modal.js"></script>
  <script src="scripts/ui-nav.js"></script>
  <script src="scripts/ui-list.js"></script>
  <script src="scripts/ui-screenfull.js"></script>
  <script src="scripts/ui-scroll-to.js"></script>
  <script src="scripts/ui-toggle-class.js"></script>
  <script src="scripts/ui-taburl.js"></script>
  <script src="scripts/app.js"></script>
  <script src="scripts/ajax.js"></script>
<!-- endbuild -->
<script type="text/javascript">
  $(document).ready(function(){
      $('#input-form').on('submit',function(e){
        // alert('selected');
        e.preventDefault();
          var status = $('#input-order-status').val();
          // var notify = $('#input-notify').val();
          if ($("#input-notify").is(":checked")) {
            var notify = 1;
          } else {
            var notify = 0;
          }

          if ($("#input-notify-sms").is(":checked")) {
            var notify_sms = 1;
          } else {
            var notify_sms = 0;
          }
          var comments = $('#input-comment').val();
          var uniq = $('#uniq').val();
          var user_id = $('#user_idd').val();
          // myData = {'status': status, 'notify': notify, 'notify_sms': notify_sms, 'commentr': comments, 'user_id': user_id, 'uniq': uniq}
          if(status){
            // console.log(myData);
            $.ajax({
                type:'POST',
                url:'historyAjax.php',
                dataType: "json",
                data: {status: status, notify: notify, notify_sms: notify_sms, user_id: user_id, uniq: uniq, messages: comments},
                success:function(data){
                  // console.log(data);
                  // console.log('poi');
                  if (data['success']['notify'] == 1) {
                    var notify = 'Yes';
                  } else {
                    var notify = 'No';
                  }

                  if (data['success']['notify_sms'] == 1) {
                    var notify_sms = 'Yes';
                  } else {
                    var notify_sms = 'No';
                  }
                  if (data['success']['status'] == 0) {
                    var status = 'Pending';
                  } else if (data['success']['status'] == 1) {
                    var status = 'Processing';
                  } else if (data['success']['status'] == 2) {
                    var status = 'Shipped';
                  } else if (data['success']['status'] == 3) {
                    var status = 'Complete';
                  } else if (data['success']['status'] == 4) {
                    var status = 'Canceled';
                  } else if (data['success']['status'] == 5) {
                    var status = 'Refunded';
                  }
                  $('#append').append("<tr><td>"+data['success']['date']+"</td><td>"+data['success']['comment']+"</td><td>"+status+"</td><td>"+notify+"</td><td>"+notify_sms+"</td></tr>");
                  $('#input-order-status').val('');
                  $('#input-notify').attr('checked', false);
                  $('#input-notify-sms').attr('checked', false);
                  $('#input-comment').val('');
                  var json = data;
                    $('#offence_details').val(data[0]);
                    $('#offence_original').val(data[1]);
                    $('#fine_one').val(data[2]);
                    $('#fine_two').val(data[3]);
                    // console.log(html);
                }
            });
          }
      });
  });
</script>
</body>
</html>
