<div id="aside" class="app-aside fade nav-dropdown black">
  <!-- fluid app aside -->
  <div class="navside dk" data-layout="column">
    <div class="navbar no-radius">
      <!-- brand -->
      <a href="index.php" class="navbar-brand">
        <div data-ui-include="'images/logo.svg'"></div>
        <!-- <img src="images/logo.png" alt="." class="hide"> -->
        <span class="hidden-folded inline">Day Shop</span>
      </a>
      <!-- / brand -->
    </div>
    <div data-flex class="hide-scroll">
        <nav class="scroll nav-stacked nav-stacked-rounded nav-color">

          <ul class="nav" data-ui-nav>
            <li class="nav-header hidden-folded">
              <span class="text-xs">Main</span>
            </li>
            <li>
              <a href="index.php" class="b-danger">
                <span class="nav-icon text-white no-fade">
                  <i class="ion-filing"></i>
                </span>
                <span class="nav-text">Dashboard</span>
              </a>
            </li>
            <li>
              <a href="users_list.php" class="b-default">
                <span class="nav-icon text-white no-fade">
                  <i class="ion-person"></i>
                </span>
                <span class="nav-text">Users</span>
              </a>
            </li>
            <li class="">
            <a>
              <span class="nav-caret">
                <i class="fa fa-caret-down"></i>
              </span>
              <span class="nav-icon">
                <i class="ion-android-more-vertical"></i>
              </span>
              <span class="nav-text">Categories</span>
            </a>
            <ul class="nav-sub nav-mega nav-mega-3">
              <li class="">
                <a href="addcategory.php">
                  <span class="nav-text">Add</span>
                </a>
              </li>
              <li class="">
                <a href="categorylist.php">
                  <span class="nav-text">List</span>
                </a>
              </li>

            </ul>
          </li>

          <li class="">
            <a>
              <span class="nav-caret">
                <i class="fa fa-caret-down"></i>
              </span>
              <span class="nav-icon">
                <i class="ion-cube"></i>
              </span>
              <span class="nav-text">Products</span>
            </a>
            <ul class="nav-sub nav-mega nav-mega-3">
              <li class="">
                <a href="productadd.php">
                  <span class="nav-text">Add</span>
                </a>
              </li>
              <li class="">
                <a href="productlist.php">
                  <span class="nav-text">List</span>
                </a>
              </li>

            </ul>
          </li>

          <li class="">
          <a>
            <span class="nav-caret">
              <i class="fa fa-caret-down"></i>
            </span>
            <span class="nav-icon">
              <i class="ion-social-buffer"></i>
            </span>
            <span class="nav-text">Stock</span>
          </a>
          <ul class="nav-sub nav-mega nav-mega-3">
            <li class="">
              <a href="stockadd.php">
                <span class="nav-text">Add</span>
              </a>
            </li>
            <li class="">
              <a href="stocklist.php">
                <span class="nav-text">List</span>
              </a>
            </li>

          </ul>
        </li>

        <li class="">
          <a>
            <span class="nav-caret">
              <i class="fa fa-caret-down"></i>
            </span>
            <span class="nav-icon">
              <i class="ion-social-euro"></i>
            </span>
            <span class="nav-text">Coupons</span>
          </a>
          <ul class="nav-sub nav-mega nav-mega-3">
            <li class="">
              <a href="addcoupons.php">
                <span class="nav-text">Add</span>
              </a>
            </li>
            <li class="">
              <a href="listcoupons.php">
                <span class="nav-text">List</span>
              </a>
            </li>

          </ul>
        </li>
        <li class="">
          <a>
            <span class="nav-caret">
              <i class="fa fa-caret-down"></i>
            </span>
            <span class="nav-icon">
              <i class="ion-ios-analytics"></i>
            </span>
            <span class="nav-text">Orders</span>
          </a>
          <ul class="nav-sub nav-mega nav-mega-3">
            <li class="">
              <a href="orders.php">
                <span class="nav-text">New</span>
              </a>
            </li>
            <li class="">
              <a href="orders.php">
                <span class="nav-text">Completed</span>
              </a>
            </li>
            <li class="">
              <a href="orders.php">
                <span class="nav-text">Cancelled</span>
              </a>
            </li>
            <li class="">
              <a href="orders.php">
                <span class="nav-text">Refunded</span>
              </a>
            </li>

          </ul>
        </li>
        <li class="">
          <a>
            <span class="nav-caret">
              <i class="fa fa-caret-down"></i>
            </span>
            <span class="nav-icon">
              <i class="ion-ios-analytics"></i>
            </span>
            <span class="nav-text">Reports</span>
          </a>
          <ul class="nav-sub nav-mega nav-mega-3">
            <li class="">
              <a href="stock_report.php">
                <span class="nav-text">Stock</span>
              </a>
            </li>


          </ul>
        </li>


            </ul>
        </nav>
    </div>
    <div data-flex-no-shrink>
      <div class="nav-fold dropup">
        <a data-toggle="dropdown">
            <div class="pull-left">
              <div class="inline"><span class="avatar w-40 grey">VE</span></div>
              <img src="images/a0.jpg" alt="..." class="w-40 img-circle hide">
            </div>
            <div class="clear hidden-folded p-x">
              <span class="block _500 text-muted">Venky</span>
              <div class="progress-xxs m-y-sm lt progress">
                  <div class="progress-bar info" style="width: 15%;">
                  </div>
              </div>
            </div>
        </a>
        <div class="dropdown-menu w dropdown-menu-scale ">
          <a class="dropdown-item" href="profile.html">
            <span>Profile</span>
          </a>
          <a class="dropdown-item" href="setting.html">
            <span>Settings</span>
          </a>
          <a class="dropdown-item" href="app.inbox.html">
            <span>Inbox</span>
          </a>
          <a class="dropdown-item" href="app.message.html">
            <span>Message</span>
          </a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="docs.html">
            Need help?
          </a>
          <a class="dropdown-item" href="signout.php">Sign out</a>
        </div>
      </div>
    </div>
  </div>
</div>
