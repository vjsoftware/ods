<?php

    include_once 'core/init.php';
    $link = new Link();
    $table = 'coupons';
    $errors = [];

    if (isset($_POST['enter'])) {
     $validation = new Validate();
     $valid = $validation->check($_POST, [
       'name' => [
         'required' => TRUE,
         'min' => 3
       ],
       'code' => [
         'required' => TRUE,
         'min' => 3
       ]
     ]);

     if ($valid->passed()) {
         try {
         $link-> add([
           'name' => $_POST['name'],
           'code' => $_POST['code'],
           'type' => $_POST['type'],
           'value' => $_POST['value'],
           'total_amount' => $_POST['total_amount'],
           'product' => $_POST['product'],
           'category' => $_POST['category'],
           'from' => $_POST['from'],
           'to' => $_POST['to'],
           'use_per_coupon' => $_POST['use_per_coupon'],
           'use_per_customer' => $_POST['use_per_customer'],
           'status' => $_POST['status']
         ], $table);
         Redirect::to('listcoupons.php');
       } catch (Exception $e) {
         die($e);
       }

     } else {
       foreach ($valid->errors() as $error) {
         array_push($errors, $error);
       }
     }
    }
  ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <title>aside - Bootstrap 4 web application</title>
  <meta name="description" content="Responsive, Bootstrap, BS4" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <!-- for ios 7 style, multi-resolution icon of 152x152 -->
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-barstyle" content="black-translucent">
  <link rel="apple-touch-icon" href="images/logo.png">
  <meta name="apple-mobile-web-app-title" content="Flatkit">
  <!-- for Chrome on Android, multi-resolution icon of 196x196 -->
  <meta name="mobile-web-app-capable" content="yes">
  <link rel="shortcut icon" sizes="196x196" href="images/logo.png">

  <!-- style -->
  <link rel="stylesheet" href="css/animate.css/animate.min.css" type="text/css" />
  <link rel="stylesheet" href="css/glyphicons/glyphicons.css" type="text/css" />
  <link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css" type="text/css" />
  <link rel="stylesheet" href="css/material-design-icons/material-design-icons.css" type="text/css" />
  <link rel="stylesheet" href="css/ionicons/css/ionicons.min.css" type="text/css" />
  <link rel="stylesheet" href="css/simple-line-icons/css/simple-line-icons.css" type="text/css" />
  <link rel="stylesheet" href="css/bootstrap/dist/css/bootstrap.min.css" type="text/css" />

  <!-- build:css css/styles/app.min.css -->
  <link rel="stylesheet" href="css/styles/app.css" type="text/css" />
  <link rel="stylesheet" href="css/styles/style.css" type="text/css" />
  <!-- endbuild -->
  <link rel="stylesheet" href="css/styles/font.css" type="text/css" />
</head>
<body class="  pace-done pace-done" ui-class=""><div class="pace  pace-inactive pace-inactive"><div class="pace-progress" data-progress-text="100%" data-progress="99" style="transform: translate3d(100%, 0px, 0px);">
  <div class="pace-progress-inner"></div>
</div>
<div class="pace-activity"></div></div>
  <div class="app" id="app">

<!-- ############ LAYOUT START-->

  <!-- aside -->
  <div id="aside" class="app-aside fade nav-dropdown black">
    <!-- fluid app aside -->
    <div class="navside dk" data-layout="column">
      <div class="navbar no-radius">
        <!-- brand -->
        <a href="index.html" class="navbar-brand">
        	<!-- <div data-ui-include="'images/logo.svg'"></div>
        	<img src="images/logo.png" alt="." class="hide"> -->
        	<span class="hidden-folded inline">Shop</span>
        </a>
        <!-- / brand -->
      </div>
      <?php include_once 'sidebar.php'; ?>

    </div>
  </div>
  <!-- / -->
  <div id="content" class="app-content box-shadow-z2 bg pjax-container" role="main">

    <div class="app-header white bg b-b">
          <div class="navbar" data-pjax>
                <a data-toggle="modal" data-target="#aside" class="navbar-item pull-left hidden-lg-up p-r m-a-0">
                  <i class="ion-navicon"></i>
                </a>
                <div class="navbar-item pull-left h5" id="pageTitle">Add Coupoun</div>
                <!-- nabar right -->
                <?php include_once 'head.php'; ?>
                <!-- / navbar right -->
          </div>
    </div>

    <?php include_once 'footer.php'; ?>
  <!-- content -->



    <div class="app-body">

<!-- ############ PAGE START-->
<div class="padding">
  <div class="row">




      <div class="box">
        <div class="box-header">
          <h2>Add Coupoun</h2>

        </div>
        <div class="box-divider m-a-0"></div>
        <div class="box-body p-v-md">
          <form class="form-inline" role="form" action="" method="post" enctype="multipart/form-data">

              <div class="form-group col-md-6">
                <label class="col-sm-3 form-control-label" for="exampleInputEmail2">Coupon Name</label>
                <input required type="text" class="form-control" name="name" id="name" placeholder="Coupon Name">
                <br>
                <?php if (in_array("name is Required", $errors)): ?>
                  <span class="text-warning">Coupon Name is Required</span>
                <?php elseif (in_array("name must be minimum of 3 characters", $errors)): ?>
                  <span class="text-warning">Must be atlest 3 Charaters Limit</span>
                <?php endif; ?>
                <div class="text-warning">
                                    <br>
                                  </div>
              </div>

              <div class="form-group col-md-6">
                <label class="col-sm-3 form-control-label" for="exampleInputEmail2">Coupon Code</label>
                <input required type="text" class="form-control" name="code" id="code" placeholder="Coupon Code">
                <br>
                <?php if (in_array("code is Required", $errors)): ?>
                  <span class="text-warning">Coupon Code is Required</span>
                <?php elseif (in_array("code must be minimum of 3 characters", $errors)): ?>
                  <span class="text-warning">Must be atlest 3 Charaters Limit</span>
                <?php endif; ?>
                <div class="text-warning">
                                    <br>
                                  </div>
              </div>

              <div class="form-group col-md-6">
              <label class="col-sm-3 form-control-label" for="exampleInputEmail2">Type</label>
              <select required class="form-control" name="type" id="type">
                <option value="">Select</option>
                <option value="1">Fixed Amount</option>
                <option value="2">Percentage</option>
              </select>
              <div class="text-warning">
                <br>
              </div>
            </div>

            <div class="form-group col-md-6">
              <label class="col-sm-3 form-control-label" for="exampleInputEmail2">Value</label>
              <input required type="text" class="form-control" name="value" id="value">
              <br>
              <?php if (in_array("value is Required", $errors)): ?>
                <span class="text-warning">Value is Required</span>
              <?php endif; ?>
              <div class="text-warning">
                                  <br>
                                </div>
            </div>
            <div class="col-md-12">

            </div>
            <div class="form-group col-md-6">
              <label class="col-sm-3 form-control-label" for="exampleInputEmail2">Total Amount</label>
              <input type="text" class="form-control" name="total_amount" id="total_amount">
              <br>
              <?php if (in_array("total_amount is Required", $errors)): ?>
                <span class="text-warning">Total Amount is Required</span>
              <?php endif; ?>
              <div class="text-warning">
                                  <br>
                                </div>
            </div>

            <div class="form-group col-md-12"></div>

            <div class="form-group col-md-6">
            <label class="col-sm-3 form-control-label" for="exampleInputEmail2">Product</label>
            <select class="form-control" name="product" id="product">
              <option value="0">Select</option>
              <option value="all">All Products</option>
              <?php $pro = DB::getInstance()->query("SELECT * FROM `product`"); ?>
              <?php foreach ($pro->results() as $val): ?>
                <option value="<?php echo $val->id; ?>"><?php echo $val->name; ?></option>
              <?php endforeach; ?>
            </select>
            <div class="text-warning">
              <br>
            </div>
          </div>

          <div class="form-group col-md-6">
          <label class="col-sm-3 form-control-label" for="exampleInputEmail2">Category</label>
          <select class="form-control" name="category" id="category">
            <option value="0">Select</option>
                  <?php $product_list = DB::getInstance()->query("SELECT * FROM category"); ?>
                  <?php foreach ($product_list->results() as $product): ?>
                    <option value="<?php echo $product->id; ?>"><?php echo $product->name; ?></option>
                  <?php endforeach; ?>
                </select>
          <div class="text-warning">
            <br>
          </div>
        </div>


        <div class="form-group col-md-12">
                <label class="col-sm-2 form-control-label" for="exampleInputEmail2">From</label>
                  <div class="input-group date col-sm-4" style="width: 0px;" data-ui-jp="datetimepicker" data-ui-options="{
                        format: 'DD-MM-YYYY',
                        icons: {
                          time: 'fa fa-clock-o',
                          date: 'fa fa-calendar',
                          up: 'fa fa-chevron-up',
                          down: 'fa fa-chevron-down',
                          previous: 'fa fa-chevron-left',
                          next: 'fa fa-chevron-right',
                          today: 'fa fa-screenshot',
                          clear: 'fa fa-trash',
                          close: 'fa fa-remove'
                        }
                      }">
                      <input type="text" name="from" class="form-control has-value">
                      <span class="input-group-addon">
                          <span class="fa fa-calendar"></span>
                      </span>
                  </div>
                <label class="col-sm-2 form-control-label" for="exampleInputEmail2">To</label>
                  <div class="input-group date col-sm-4" style="width: 0px;" data-ui-jp="datetimepicker" data-ui-options="{
                        format: 'DD-MM-YYYY',
                        icons: {
                          time: 'fa fa-clock-o',
                          date: 'fa fa-calendar',
                          up: 'fa fa-chevron-up',
                          down: 'fa fa-chevron-down',
                          previous: 'fa fa-chevron-left',
                          next: 'fa fa-chevron-right',
                          today: 'fa fa-screenshot',
                          clear: 'fa fa-trash',
                          close: 'fa fa-remove'
                        }
                      }">
                      <input type="text" name="to" class="form-control has-value">
                      <span class="input-group-addon">
                          <span class="fa fa-calendar"></span>
                      </span>
                  </div>
              </div>

          <div class='input-group'></div>


        <div class="form-group col-md-6">
          <label class="col-sm-3 form-control-label" for="exampleInputEmail2">Use per Coupon</label>
          <input type="text" class="form-control" name="use_per_coupon" id="use_per_coupon">
          <div class="text-warning">
                              <br>
                            </div>
        </div>

        <div class="form-group col-md-6">
          <label class="col-sm-4 form-control-label" for="exampleInputEmail2">Use per Customer</label>
          <input type="text" class="form-control" name="use_per_customer" id="use_per_customer">
          <div class="text-warning">
                              <br>
                            </div>
        </div>

        <div class="form-group col-md-6">
          <label class="col-sm-3 form-control-label" for="exampleInputEmail2">Status</label>
          <div class="radio">
          <label class="ui-check ui-check-lg">
            <input type="radio" name="status" value="on" class="has-value" required>
            <i class="dark-white"></i>
            Active
          </label>
          <label class="ui-check ui-check-lg">
            <input type="radio" name="status" value="off" class="has-value">
            <i class="dark-white"></i>
            In Active
          </label>
        </div>
          <div class="text-warning">
                              <br>
                            </div>
        </div>


            <br><br>
            <div class="form-group">
              <input class="btn btn-warning" type="submit" name="enter" value="Add Coupons">
              <div class="text-warning">
                <br>
              </div>
            </div>

          </form>
        </div>
      </div>

  </div>
</div>

</div>
<!-- ############ PAGE END-->

    </div>
  </div>
  <!-- / -->


  <!-- ############ SWITHCHER START-->
    <?php include_once 'rightsidebar.php'; ?>
  <!-- ############ SWITHCHER END-->

<!-- ############ LAYOUT END-->
  </div>

<!-- build:js scripts/app.min.js -->
<!-- jQuery -->
  <script src="libs/jquery/dist/jquery.js"></script>
<!-- Bootstrap -->
  <script src="libs/tether/dist/js/tether.min.js"></script>
  <script src="libs/bootstrap/dist/js/bootstrap.js"></script>
<!-- core -->
  <script src="libs/jQuery-Storage-API/jquery.storageapi.min.js"></script>
  <script src="libs/PACE/pace.min.js"></script>
  <script src="libs/jquery-pjax/jquery.pjax.js"></script>
  <script src="libs/blockUI/jquery.blockUI.js"></script>
  <script src="libs/jscroll/jquery.jscroll.min.js"></script>

  <script src="scripts/config.lazyload.js"></script>
  <script src="scripts/ui-load.js"></script>
  <script src="scripts/ui-jp.js"></script>
  <script src="scripts/ui-include.js"></script>
  <script src="scripts/ui-device.js"></script>
  <script src="scripts/ui-form.js"></script>
  <script src="scripts/ui-modal.js"></script>
  <script src="scripts/ui-nav.js"></script>
  <script src="scripts/ui-list.js"></script>
  <script src="scripts/ui-screenfull.js"></script><script src="libs/screenfull/dist/screenfull.min.js"></script>
  <script src="scripts/ui-scroll-to.js"></script>
  <script src="scripts/ui-toggle-class.js"></script>
  <script src="scripts/ui-taburl.js"></script>
  <script src="scripts/app.js"></script>
  <script src="scripts/ajax.js"></script>
<!-- endbuild -->


</body>
</html>
