<ul class="nav navbar-nav pull-right">
  <li class="nav-item dropdown">
    <a class="nav-link clear" data-toggle="dropdown">
      <span class="avatar w-32">
        <img src="images/a3.jpg" class="w-full rounded" alt="...">
      </span>
    </a>
    <div class="dropdown-menu w dropdown-menu-scale pull-right">
      <a class="dropdown-item" href="profile.html">
        <span>Profile</span>
      </a>
      <a class="dropdown-item" href="setting.html">
        <span>Settings</span>
      </a>
      <a class="dropdown-item" href="app.inbox.html">
        <span>Inbox</span>
      </a>
      <a class="dropdown-item" href="app.message.html">
        <span>Message</span>
      </a>
      <div class="dropdown-divider"></div>
      <a class="dropdown-item" href="docs.html">
        Need help?
      </a>
      <a class="dropdown-item" href="signout.php">Sign out</a>
    </div>
  </li>
</ul>
