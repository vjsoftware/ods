<?php
 include_once 'core/init.php';

 $link = new Link();
  if (!$link->isLoggedIn()) {
     redirect::to('signin.php');
   }
   $table = "product";
   if (isset($_GET['delete'])) {
     $delete_id = $_GET['delete'];
     $link->deletes($table, $delete_id);
     Redirect::to('productlist.php');
   }
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <title>aside - Bootstrap 4 web application</title>
  <meta name="description" content="Responsive, Bootstrap, BS4" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <!-- for ios 7 style, multi-resolution icon of 152x152 -->
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-barstyle" content="black-translucent">
  <link rel="apple-touch-icon" href="images/logo.png">
  <meta name="apple-mobile-web-app-title" content="Flatkit">
  <!-- for Chrome on Android, multi-resolution icon of 196x196 -->
  <meta name="mobile-web-app-capable" content="yes">
  <link rel="shortcut icon" sizes="196x196" href="images/logo.png">

  <!-- style -->
  <link rel="stylesheet" href="css/animate.css/animate.min.css" type="text/css" />
  <link rel="stylesheet" href="css/glyphicons/glyphicons.css" type="text/css" />
  <link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css" type="text/css" />
  <link rel="stylesheet" href="css/material-design-icons/material-design-icons.css" type="text/css" />
  <link rel="stylesheet" href="css/ionicons/css/ionicons.min.css" type="text/css" />
  <link rel="stylesheet" href="css/simple-line-icons/css/simple-line-icons.css" type="text/css" />
  <link rel="stylesheet" href="css/bootstrap/dist/css/bootstrap.min.css" type="text/css" />

  <!-- build:css css/styles/app.min.css -->
  <link rel="stylesheet" href="css/styles/app.css" type="text/css" />
  <link rel="stylesheet" href="css/styles/style.css" type="text/css" />
  <!-- endbuild -->
  <link rel="stylesheet" href="css/styles/font.css" type="text/css" />
</head>
<body>
  <div class="app" id="app">

<!-- ############ LAYOUT START-->

  <!-- aside -->
  <?php include_once 'sidebar.php'; ?>
  <!-- / -->
  <div id="content" class="app-content box-shadow-z2 bg pjax-container" role="main">

    <div class="app-header white bg b-b">
          <div class="navbar" data-pjax>
                <a data-toggle="modal" data-target="#aside" class="navbar-item pull-left hidden-lg-up p-r m-a-0">
                  <i class="ion-navicon"></i>
                </a>
                <div class="navbar-item pull-left h5" id="pageTitle">Product List</div>
                <!-- nabar right -->
                <?php include_once 'head.php'; ?>
                <!-- / navbar right -->
          </div>
    </div>

    <?php include_once 'footer.php'; ?>
  <!-- content -->

  <!-- content -->

    <div class="app-body">

<!-- ############ PAGE START-->
<div class="padding">
  <div class="box">
    <div class="box-header">
      <h2>Products List</h2>
    </div>
    <div class="table-responsive" id="datatable">
      <table data-ui-jp="dataTable" data-ui-options="{

          lengthChange: false,
          buttons: [ 'copy', 'excel', 'pdf', 'colvis' ],

          'initComplete': function () {
            this.api().buttons().container()
              .appendTo( '#datatable .col-md-6:eq(0)' );
          }
        }" class="table table-striped b-t b-b">
        <thead>
          <tr>
            <th>Id</th>
            <th>Product Name</th>
            <!-- <th>Description</th> -->
            <th>Images</th>
            <!-- <th>Category</th> -->
            <th>Min Ord</th>
            <th>Price</th>
            <th>Delete</th>
            <th>Edit</th>
          </tr>
        </thead>
        <tbody>

          <?php $list = DB::getInstance()->query("SELECT * FROM `product`");
						$id = 1;
						 ?>
						 <?php foreach ($list->results() as $tst): ?>
		          <tr>
						 	<td><?php echo $tst->id; ?></td>
							<td><?php echo $tst->name; ?></td>
							<!-- <td><?php echo substr($tst->description, 0, 50); ?></td> -->
              <td><img style="border-radius: 50%; height: 30px; border: 2px solid #ccc;" src="<?php echo '../thumb/'.$tst->image;?>"></td>
              <?php $category_name = DB::getInstance()->query("SELECT * FROM `category` WHERE `id` = '$tst->category'"); ?>
              <!-- <td><?php echo $category_name->first()->name; ?></td> -->
              <td><?php echo $tst->min_order; ?></td>
              <td><?php echo $tst->price; ?></td>
							<td><a href="productlist.php?delete=<?php echo $tst->id; ?>">Delete</a></td>
							<td><a href="productadd.php?id=<?php echo $tst->id; ?>">Edit</a></td>
	           </tr>

						 <?php endforeach; ?>


        </tbody>
      </table>
    </div>
  </div>
</div>

<!-- ############ PAGE END-->

    </div>
  </div>
  <?php include_once 'rightsidebar.php'; ?>
  <!-- / -->
</div>


<!-- build:js scripts/app.min.js -->
<!-- jQuery -->
  <script src="libs/jquery/dist/jquery.js"></script>
<!-- Bootstrap -->
  <script src="libs/tether/dist/js/tether.min.js"></script>
  <script src="libs/bootstrap/dist/js/bootstrap.js"></script>
<!-- core -->
  <script src="libs/jQuery-Storage-API/jquery.storageapi.min.js"></script>
  <script src="libs/PACE/pace.min.js"></script>
  <script src="libs/jquery-pjax/jquery.pjax.js"></script>
  <script src="libs/blockUI/jquery.blockUI.js"></script>
  <script src="libs/jscroll/jquery.jscroll.min.js"></script>

  <script src="scripts/config.lazyload.js"></script>
  <script src="scripts/ui-load.js"></script>
  <script src="scripts/ui-jp.js"></script>
  <script src="scripts/ui-include.js"></script>
  <script src="scripts/ui-device.js"></script>
  <script src="scripts/ui-form.js"></script>
  <script src="scripts/ui-modal.js"></script>
  <script src="scripts/ui-nav.js"></script>
  <script src="scripts/ui-list.js"></script>
  <script src="scripts/ui-screenfull.js"></script>
  <script src="scripts/ui-scroll-to.js"></script>
  <script src="scripts/ui-toggle-class.js"></script>
  <script src="scripts/ui-taburl.js"></script>
  <script src="scripts/app.js"></script>
  <script src="scripts/ajax.js"></script>
<!-- endbuild -->
</body>
</html>
