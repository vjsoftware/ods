<?php
  include_once 'core/init.php';

  $link = new Link();
  $table = 'users';
  $date = date('d-m-Y');
  $time = date('h:i A');


  if ($link->isloggedIn()) {
    redirect::to('signin.php');
  }

  if (isset($_POST['submit'])) {
    $validate = new Validate();
    $val_validate = $validate->check($_POST, [
      'username' => [
        'required' => TRUE
      ],
      'password' => [
        'required' => TRUE
      ],
      'passwordagain' => [
        'required' => TRUE,
        'matches' => 'password'
      ]
    ]);

    if ($val_validate->passed()) {
      $salt = Hash::salt(20);
      try {
        $link->add([
          'username' => $_POST['username'],
          'password' => Hash::make($_POST['password']. $salt),
          'date' => $date,
          'time' => $time,
          'salt' => $salt
        ], $table);
        Redirect::to('signin.php');
      } catch (Exception $e) {
        echo $e;
      }
    } else{
      foreach ($val_validate->errors() as $error) {
        echo $error;
      }
    }
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <title>aside - Bootstrap 4 web application</title>
  <meta name="description" content="Responsive, Bootstrap, BS4" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <!-- for ios 7 style, multi-resolution icon of 152x152 -->
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-barstyle" content="black-translucent">
  <link rel="apple-touch-icon" href="images/logo.png">
  <meta name="apple-mobile-web-app-title" content="Flatkit">
  <!-- for Chrome on Android, multi-resolution icon of 196x196 -->
  <meta name="mobile-web-app-capable" content="yes">
  <link rel="shortcut icon" sizes="196x196" href="image/logo.png">

  <!-- style -->
  <link rel="stylesheet" href="css/animate.css/animate.min.css" type="text/css" />
  <link rel="stylesheet" href="css/glyphicons/glyphicons.css" type="text/css" />
  <link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css" type="text/css" />
  <link rel="stylesheet" href="css/material-design-icons/material-design-icons.css" type="text/css" />
  <link rel="stylesheet" href="css/ionicons/css/ionicons.min.css" type="text/css" />
  <link rel="stylesheet" href="css/simple-line-icons/css/simple-line-icons.css" type="text/css" />
  <link rel="stylesheet" href="css/bootstrap/dist/css/bootstrap.min.css" type="text/css" />

  <!-- build:css css/styles/app.min.css -->
  <link rel="stylesheet" href="css/styles/app.css" type="text/css" />
  <link rel="stylesheet" href="css/styles/style.css" type="text/css" />
  <!-- endbuild -->
  <link rel="stylesheet" href="css/styles/font.css" type="text/css" />
</head>
<body>
  <div class="app" id="app">

<!-- ############ LAYOUT START-->

  <div class="padding">
    <div class="navbar">
      <div class="pull-center">
        <!-- brand -->
        <a href="index.php" class="navbar-brand">
        	<div data-ui-include="'image/logo.svg'"></div>
        	<img src="image/logo.png" alt="." class="hide">
        	<span class="hidden-folded inline">Signup</span>
        </a>
        <!-- / brand -->
      </div>
    </div>
  </div>
  <div class="b-t">
    <div class="center-block w-xxl w-auto-xs p-y-md text-center">
      <div class="p-a-md">

        <form name="form" action="" method="post">
          <div class="form-group">
            <input type="text" class="form-control" placeholder="Username" required name="username">
          </div>
          <div class="form-group">
            <input type="password" class="form-control" placeholder="Password" required name="password">
          </div>
          <div class="form-group">
            <input type="password" class="form-control" placeholder="Password again" required name="passwordagain">
          </div>
          <div class="m-b-md text-sm">
            <span class="text-muted">By clicking Sign Up, I agree to the</span>
            <a href="#">Terms of service</a>
            <span class="text-muted">and</span>
            <a href="#">Policy Privacy.</a>
          </div>
          <input type="submit" class="btn btn-lg black p-x-lg" name="submit" value="Sign Up" style="background-color:#f0ad4e;">
        </form>

      </div>
    </div>
  </div>

<!-- ############ LAYOUT END-->
  </div>

<!-- build:js scripts/app.min.js -->
<!-- jQuery -->
  <script src="libs/jquery/dist/jquery.js"></script>
<!-- Bootstrap -->
  <script src="libs/tether/dist/js/tether.min.js"></script>
  <script src="libs/bootstrap/dist/js/bootstrap.js"></script>
<!-- core -->
  <script src="libs/jQuery-Storage-API/jquery.storageapi.min.js"></script>
  <script src="libs/PACE/pace.min.js"></script>
  <script src="libs/jquery-pjax/jquery.pjax.js"></script>
  <script src="libs/blockUI/jquery.blockUI.js"></script>
  <script src="libs/jscroll/jquery.jscroll.min.js"></script>

  <script src="scripts/config.lazyload.js"></script>
  <script src="scripts/ui-load.js"></script>
  <script src="scripts/ui-jp.js"></script>
  <script src="scripts/ui-include.js"></script>
  <script src="scripts/ui-device.js"></script>
  <script src="scripts/ui-form.js"></script>
  <script src="scripts/ui-modal.js"></script>
  <script src="scripts/ui-nav.js"></script>
  <script src="scripts/ui-list.js"></script>
  <script src="scripts/ui-screenfull.js"></script>
  <script src="scripts/ui-scroll-to.js"></script>
  <script src="scripts/ui-toggle-class.js"></script>
  <script src="scripts/ui-taburl.js"></script>
  <script src="scripts/app.js"></script>
  <script src="scripts/ajax.js"></script>
<!-- endbuild -->
</body>
</html>
