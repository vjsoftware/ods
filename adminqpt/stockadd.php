<?php
 include_once 'core/init.php';

 $link = new Link();
 if (!$link->isLoggedIn()) {
   Redirect::to('signin.php');
 }
 $errors = [];
 $date = date('d-m-Y');
 $time = date('h:i A');

 $product_id = $_POST['product'];

 $table1 = 'product';


 if (isset($_POST['enter'])) {
 	$validate = new Validate();
	$valid = $validate->check($_POST, [
		'product' => [
			'required' => TRUE,
        'min' => 1
		],
    'qty_received' => [
      'required' => TRUE,
      'min' => 1
    ]
	]);

	if ($valid->passed()) {
		$table = 'stock';
    try {
			$link-> add([
				'product' => $_POST['product'],
				'qty_received' => $_POST['qty_received'],
        'date' => $date,
        'time' => $time
			], $table);
      $current_stock = DB::getInstance()->query("SELECT * FROM `product` WHERE `id` = '$product_id'");
      $stock_total = $current_stock->first()->stock;
      $stock = $stock_total + $_POST['qty_received'];
      try {
        $link->updates([
          'stock' => $stock
        ], $table1, $product_id);
      } catch (Exception $e) {
        echo $erru = $e->getMessage();
      }
		} catch (Exception $e) {
			die($e);
		}


  } else {
    foreach ($valid->errors() as $error) {
      array_push($errors, $error);
    }
  }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <title>Add Category</title>
  <meta name="description" content="Responsive, Bootstrap, BS4" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <!-- for ios 7 style, multi-resolution icon of 152x152 -->
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-barstyle" content="black-translucent">
  <link rel="apple-touch-icon" href="images/logo.png">
  <meta name="apple-mobile-web-app-title" content="Flatkit">
  <!-- for Chrome on Android, multi-resolution icon of 196x196 -->
  <meta name="mobile-web-app-capable" content="yes">
  <link rel="shortcut icon" sizes="196x196" href="images/logo.png">

  <!-- style -->
  <link rel="stylesheet" href="css/animate.css/animate.min.css" type="text/css" />
  <link rel="stylesheet" href="css/glyphicons/glyphicons.css" type="text/css" />
  <link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css" type="text/css" />
  <link rel="stylesheet" href="css/material-design-icons/material-design-icons.css" type="text/css" />
  <link rel="stylesheet" href="css/ionicons/css/ionicons.min.css" type="text/css" />
  <link rel="stylesheet" href="css/simple-line-icons/css/simple-line-icons.css" type="text/css" />
  <link rel="stylesheet" href="css/bootstrap/dist/css/bootstrap.min.css" type="text/css" />

  <!-- build:css css/styles/app.min.css -->
  <link rel="stylesheet" href="css/styles/app.css" type="text/css" />
  <link rel="stylesheet" href="css/styles/style.css" type="text/css" />
  <!-- endbuild -->
  <link rel="stylesheet" href="css/styles/font.css" type="text/css" />
</head>
<body>
  <div class="app" id="app">

<!-- ############ LAYOUT START-->

  <!-- aside -->
  <div id="aside" class="app-aside fade nav-dropdown black">
    <!-- fluid app aside -->
    <div class="navside dk" data-layout="column">
      <div class="navbar no-radius">
        <!-- brand -->
        <a href="index.html" class="navbar-brand">
        	<!-- <div data-ui-include="'images/logo.svg'"></div>
        	<img src="images/logo.png" alt="." class="hide"> -->
        	<span class="hidden-folded inline">Shop</span>
        </a>
        <!-- / brand -->
      </div>
      <?php include_once 'sidebar.php'; ?>

    </div>
  </div>
  <!-- / -->

  <!-- content -->
  <div id="content" class="app-content box-shadow-z2 bg pjax-container" role="main">
    <div class="app-header white bg b-b">
          <div class="navbar" data-pjax>
                <a data-toggle="modal" data-target="#aside" class="navbar-item pull-left hidden-lg-up p-r m-a-0">
                  <i class="ion-navicon"></i>
                </a>
                <div class="navbar-item pull-left h5" id="pageTitle">Stock</div>
                <!-- nabar right -->
                <?php include_once 'navbar.php'; ?>
                <!-- / navbar right -->
          </div>
    </div>
    <?php include_once 'footer.php' ?>
    <div class="app-body">

<!-- ############ PAGE START-->
<div class="padding">
  <div class="row">

    <div class="box">
      <div class="box-header">
        <h2>Add Stock</h2>

      </div>
      <div class="box-divider m-a-0"></div>
      <div class="box-body p-v-md">
        <form class="form-inline" role="form" action="" method="post" enctype="multipart/form-data">

          <div class="form-group col-md-12">
            <br>
            <label class="col-sm-2 form-control-label" for="exampleInputEmail2">Select Product</label>
            <select class="form-control" name="product" id="product">
              <option value="Select">Select</option>

                    <?php $product_list = DB::getInstance()->query("SELECT * FROM product"); ?>
                    <?php foreach ($product_list->results() as $product): ?>
                      <option value="<?php echo $product->id; ?>"><?php echo $product->name; ?></option>
                    <?php endforeach; ?>
                  </select>
                      <div class="text-warning">

              <br>
            </div>
          </div>

          <div class="form-group col-md-12">
            <br>
            <label class="col-sm-2 form-control-label" for="exampleInputEmail2">Quantity Received</label>
            <input type="text" class="form-control" name="qty_received" id="qty_received" placeholder="Quantity Received">
            <br>
            <?php if (in_array("qty_received is Required", $errors)): ?>
              <span class="text-warning">Quanity is Required</span>
            <?php elseif (in_array("qty_received must be minimum of 3 characters", $errors)): ?>
              <span class="text-warning">Must be atlest 3 Charaters Limit</span>
            <?php endif; ?>
            <div class="text-warning">

              <br>
            </div>
          </div>

          <br><br>
          <div class="form-group">
            <input class="btn btn-warning" type="submit" name="enter" value="Add Stock">
            <div class="text-warning">
              <br>
            </div>
          </div>

        </form>
      </div>
    </div>


  </div>
</div>

<!-- ############ PAGE END-->

    </div>
  </div>
  <!-- / -->


  <!-- ############ SWITHCHER START-->
    <?php include_once 'switcher.php' ?>
  <!-- ############ SWITHCHER END-->

<!-- ############ LAYOUT END-->
  </div>

<!-- build:js scripts/app.min.js -->
<!-- jQuery -->
  <script src="libs/jquery/dist/jquery.js"></script>
<!-- Bootstrap -->
  <script src="libs/tether/dist/js/tether.min.js"></script>
  <script src="libs/bootstrap/dist/js/bootstrap.js"></script>
<!-- core -->
  <script src="libs/jQuery-Storage-API/jquery.storageapi.min.js"></script>
  <script src="libs/PACE/pace.min.js"></script>
  <script src="libs/jquery-pjax/jquery.pjax.js"></script>
  <script src="libs/blockUI/jquery.blockUI.js"></script>
  <script src="libs/jscroll/jquery.jscroll.min.js"></script>

  <script src="scripts/config.lazyload.js"></script>
  <script src="scripts/ui-load.js"></script>
  <script src="scripts/ui-jp.js"></script>
  <script src="scripts/ui-include.js"></script>
  <script src="scripts/ui-device.js"></script>
  <script src="scripts/ui-form.js"></script>
  <script src="scripts/ui-modal.js"></script>
  <script src="scripts/ui-nav.js"></script>
  <script src="scripts/ui-list.js"></script>
  <script src="scripts/ui-screenfull.js"></script>
  <script src="scripts/ui-scroll-to.js"></script>
  <script src="scripts/ui-toggle-class.js"></script>
  <script src="scripts/ui-taburl.js"></script>
  <script src="scripts/app.js"></script>
  <script src="scripts/ajax.js"></script>
<!-- endbuild -->
</body>
</html>
