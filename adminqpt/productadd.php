<?php include_once 'core/init.php';

 $link = new Link();
 $table = 'product';
 $errors = [];
 $getret_id = $_GET['id'];

 if(isset($_FILES['image']) && $_FILES["image"]["size"] > 1) {
    // die("jkjkjk");
    $filetmp = $_FILES["image"]["tmp_name"];
    $filename = $_FILES["image"]["name"];
    $filetype = $_FILES["image"]["type"];
    $filepath1 = "../images/".time().$filename;
    $filepath2 = time().$filename;
    $filepath3 = "../resized/".time().$filename;
    $filepath4 = "../thumb/".time().$filename;
    $kaboom = explode(".", $filename); // Split file name into an array using the dot
    $fileExt = end($kaboom);

    move_uploaded_file($filetmp,$filepath1);

    include_once("ak_php_img_lib_1.0.php");
    $target_file = $filepath1;
    $resized_file = $filepath3;
    $resized_filee = $filepath4;
    $wmax = 226;
    $hmax = 226;
    $wmaxx = 110;
    $hmaxx = 110;
    ak_img_resize($target_file, $resized_file, $wmax, $hmax, $fileExt);
    ak_img_resize($target_file, $resized_filee, $wmaxx, $hmaxx, $fileExt);

    // $sql = "INSERT INTO upload_img (img_name,img_path,img_type) VALUES ('$filename','$filepath','$filetype')";
    // $result = mysql_query($sql);
  } else {
    $filepath1 = '';
    $filepath2 = '';
  }
  unlink($filetmp);
 if (!$link->isLoggedIn()) {
   Redirect::to('signin.php');
 }

 // foreach ($_POST['category'] as $selectedOption) {
 //
 // }

 $selectedOption = implode(", ", $_POST['category']);
 if (isset($_POST['enter'])) {
 	$validation = new Validate();
	$valid = $validation->check($_POST, [
		'name' => [
			'required' => TRUE,
      'min' => 3
		],
    'price' => [
			'required' => TRUE,
      'min' => 1
		]
	]);

	if ($valid->passed()) {
    	try {
			$link-> add([
				'name' => $_POST['name'],
				'slug' => $_POST['slug'],
				'description' => $_POST['description'],
				'specifications' => $_POST['specifications'],
        'category' => $selectedOption,
        'price' => $_POST['price'],
        'f_price' => $_POST['f_price'],
        'status' => $_POST['status'],
        'min_order' =>$_POST['min_order'],
        'return_qty' =>$_POST['return_qty'],
        'image' => $filepath2
			], $table);
      Redirect::to('productlist.php');
		} catch (Exception $e) {
			die($e);
		}

  } else {
    foreach ($valid->errors() as $error) {
      array_push($errors, $error);
    }
  }
} elseif (isset($_POST['Update'])) {
  $validate = new Validate();
  $validation = $validate->check($_POST, [
    'name' => [
      'required' => TRUE
    ]
    ]);

if ($validation->passed()) {
  if ($filepath1 == '') {
    try {
      $link->updates([
        'name' => Input::get('name'),
        'slug' => Input::get('slug'),
        'stock' => Input::get('stock'),
        'description' => Input::get('description'),
        'specifications' => Input::get('specifications'),
        'category' => $selectedOption,
        'min_order' => Input::get('min_order'),
        'price' => Input::get('price'),
        'f_price' => Input::get('f_price'),
        'status' => Input::get('status'),
        'tax' => Input::get('tax'),
        'return_qty' => Input::get('return_qty')
      ], $table, $getret_id);
      Redirect::to('productlist.php');
    } catch (Exception $e) {
      die($e);
    }
  } else {
    try {
      $link->updates([
        'name' => Input::get('name'),
        'slug' => Input::get('slug'),
        'stock' => Input::get('stock'),
        'description' => Input::get('description'),
        'specifications' => Input::get('specifications'),
        'image' => $filepath2,
        'category' => $selectedOption,
        'min_order' => Input::get('min_order'),
        'price' => Input::get('price'),
        'tax' => Input::get('tax'),
        'return_qty' => Input::get('return_qty')
      ], $table, $getret_id);
      Redirect::to('productlist.php');
    } catch (Exception $e) {
      die($e);
    }
  }
} else {
      $errors = [];
      foreach ($validation->errors() as $error) {
        array_push($errors, $error);
        // echo $error. '<br>';
      }
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <title>aside - Bootstrap 4 web application</title>
  <meta name="description" content="Responsive, Bootstrap, BS4" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <!-- for ios 7 style, multi-resolution icon of 152x152 -->
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-barstyle" content="black-translucent">
  <link rel="apple-touch-icon" href="images/logo.png">
  <meta name="apple-mobile-web-app-title" content="Flatkit">
  <!-- for Chrome on Android, multi-resolution icon of 196x196 -->
  <meta name="mobile-web-app-capable" content="yes">
  <link rel="shortcut icon" sizes="196x196" href="images/logo.png">

  <!-- style -->
  <link rel="stylesheet" href="css/animate.css/animate.min.css" type="text/css" />
  <link rel="stylesheet" href="css/glyphicons/glyphicons.css" type="text/css" />
  <link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css" type="text/css" />
  <link rel="stylesheet" href="css/material-design-icons/material-design-icons.css" type="text/css" />
  <link rel="stylesheet" href="css/ionicons/css/ionicons.min.css" type="text/css" />
  <link rel="stylesheet" href="css/simple-line-icons/css/simple-line-icons.css" type="text/css" />
  <link rel="stylesheet" href="css/bootstrap/dist/css/bootstrap.min.css" type="text/css" />

  <!-- build:css css/styles/app.min.css -->
  <link rel="stylesheet" href="css/styles/app.css" type="text/css" />
  <link rel="stylesheet" href="css/styles/style.css" type="text/css" />
  <!-- endbuild -->
  <link rel="stylesheet" href="css/styles/font.css" type="text/css" />
</head>
<body class="  pace-done pace-done" ui-class=""><div class="pace  pace-inactive pace-inactive"><div class="pace-progress" data-progress-text="100%" data-progress="99" style="transform: translate3d(100%, 0px, 0px);">
  <div class="pace-progress-inner"></div>
</div>
<div class="pace-activity"></div></div>
  <div class="app" id="app">

<!-- ############ LAYOUT START-->

  <!-- aside -->
  <div id="aside" class="app-aside fade nav-dropdown black">
    <!-- fluid app aside -->
    <div class="navside dk" data-layout="column">
      <div class="navbar no-radius">
        <!-- brand -->
        <a href="index.html" class="navbar-brand">
        	<!-- <div data-ui-include="'images/logo.svg'"></div>
        	<img src="images/logo.png" alt="." class="hide"> -->
        	<span class="hidden-folded inline">Shop</span>
        </a>
        <!-- / brand -->
      </div>
      <?php include_once 'sidebar.php'; ?>

    </div>
  </div>
  <!-- / -->
  <div id="content" class="app-content box-shadow-z2 bg pjax-container" role="main">

    <div class="app-header white bg b-b">
          <div class="navbar" data-pjax>
                <a data-toggle="modal" data-target="#aside" class="navbar-item pull-left hidden-lg-up p-r m-a-0">
                  <i class="ion-navicon"></i>
                </a>
                <div class="navbar-item pull-left h5" id="pageTitle">Product Add</div>
                <!-- nabar right -->
                <?php include_once 'head.php'; ?>
                <!-- / navbar right -->
          </div>
    </div>

    <?php include_once 'footer.php'; ?>
  <!-- content -->



    <div class="app-body">

<!-- ############ PAGE START-->
<div class="padding">
  <div class="row">




      <div class="box">
        <div class="box-header">
          <h2>Add Product</h2>

        </div>
        <div class="box-divider m-a-0"></div>
        <div class="box-body p-v-md">
          <?php if (isset($_GET['id'])):
            $get_id = $_GET['id'];
            $ret = DB::getInstance()->query("SELECT * FROM `product` WHERE `id`= '$get_id'");
            $name = $ret->first()->name;
            $slug = $ret->first()->slug;
            $stock = $ret->first()->stock;
            $description = $ret->first()->description;
            $specifications = $ret->first()->specifications;
            $category = $ret->first()->category;
            $min_order = $ret->first()->min_order;
            $price = $ret->first()->price;
            $f_price = $ret->first()->f_price;
            $status = $ret->first()->status;
            $tax = $ret->first()->tax;
            $return = $ret->first()->return_qty;
            $image = $ret->first()->image;
            ?>
          <form class="form-inline" role="form" action="" method="post" enctype="multipart/form-data">

              <div class="form-group col-md-6">
                <label class="col-sm-2 form-control-label" for="exampleInputEmail2">Product Name</label>
                <input type="text" class="form-control" name="name" id="name" placeholder="Product Name" value="<?php echo $name; ?>">
                <br>
                <?php if (in_array("name is Required", $errors)): ?>
                  <span class="text-warning">productname is Required</span>
                <?php elseif (in_array("name must be minimum of 3 characters", $errors)): ?>
                  <span class="text-warning">Must be atlest 3 Charaters Limit</span>
                <?php endif; ?>
                <div class="text-warning">
                                    <br>
                                  </div>
              </div>
              <div class="form-group col-md-6">
                <label class="col-sm-3 form-control-label" for="exampleInputEmail2">Slug</label>
                <input type="text" class="form-control" name="slug" id="slug" placeholder="Product Slug" value="<?php echo $slug; ?>">
                <br>
                <?php if (in_array("slug is Required", $errors)): ?>
                  <span class="text-warning">productname is Required</span>
                <?php elseif (in_array("slug must be minimum of 3 characters", $errors)): ?>
                  <span class="text-warning">Must be atlest 3 Charaters Limit</span>
                <?php endif; ?>
                <div class="text-warning">
                                    <br>
                                  </div>
              </div>
              <div class="form-group col-md-12">
                <label class="col-sm-2 form-control-label" for="exampleInputEmail2">Description</label>
                <textarea data-ui-jp="summernote" name="description" id="description" placeholder="Description" class="form-control" rows="3" cols="80" ><?php echo $description; ?></textarea>
                <br>
                <?php if (in_array("description is Required", $errors)): ?>
                  <span class="text-warning">description is Required</span>
                <?php elseif (in_array("description must be minimum of 3 characters", $errors)): ?>
                  <span class="text-warning">Must be atlest 3 Charaters Limit</span>
                <?php endif; ?>
                <div class="text-warning">
                                    <br>
                                  </div>
              </div>
              <div class="form-group col-md-12">
                <label class="col-sm-2 form-control-label" for="exampleInputEmail2">Specifications</label>
                <textarea data-ui-jp="summernote" name="specifications" id="specifications" placeholder="Specifications" class="form-control" rows="3" cols="80" ><?php echo $specifications; ?></textarea>
                <br>
                <div class="text-warning">
                                    <br>
                                  </div>
              </div>
            <div class="form-group col-md-12">
              <label for="inputPassword3" class="col-sm-2 form-control-label">Images</label>
                  		<input name="image" type="file" class="form-control">
  											<?php if (in_array("image already exists", $errors)): ?>
  													<span class="text-warning">image Name already Exists</span>
  											<?php endif; ?>
                <div class="text-warning">
                  <br>
                </div>
            </div>

            <div class="form-group col-md-12">
              <br>
              <label class="col-sm-2 form-control-label" for="exampleInputEmail2">Select Category</label>

                <select id="multiple" name="category[]" class="col-sm-5 select2-multiple" multiple data-ui-jp="select2" data-ui-options="{theme: 'bootstrap'}">
                  <optgroup label="Categories">
                    <?php $product_list = DB::getInstance()->query("SELECT * FROM category"); ?>
                    <?php foreach ($product_list->results() as $product): ?>
                      <?php $cat = explode(", ", $category); ?>
                      <option <?php if (in_array($product->id, $cat)): ?>
                        selected
                      <?php endif; ?> value="<?php echo $product->id; ?>"><?php echo $product->name; ?></option>
                    <?php endforeach; ?>
                  </optgroup>
                </select>



            </div>

            <div class="form-group col-md-6">
              <br>
              <label class="col-sm-4 form-control-label" for="exampleInputEmail2">Minimum Order</label>
              <input type="text" class="form-control" name="min_order" id="min_order" placeholder="Minimum order" value="<?php echo $min_order; ?>">
              <br>
              <?php if (in_array("min_order is Required", $errors)): ?>
                <span class="text-warning">Minimum order is Required</span>
              <?php elseif (in_array("min_order must be minimum of 3 characters", $errors)): ?>
                <span class="text-warning">Must be atlest 3 Charaters Limit</span>
              <?php endif; ?>
              <div class="text-warning">

                <br>
              </div>
            </div>
            <div class="form-group col-md-6">
              <br>
              <label class="col-sm-4 form-control-label" for="exampleInputEmail2">Return Policy</label>
              <input type="text" class="form-control" name="return" id="return" placeholder="Return Policy" value="<?php echo $return; ?>">
              <br>
            </div>
            <div class="col-md-12">

            </div>
            <div class="form-group col-md-6">
              <label class="col-sm-4 form-control-label" for="exampleInputEmail2">Price</label>
              <input type="text" class="form-control" name="price" id="price" value="<?php echo $price; ?>">
              <br>
              <?php if (in_array("price is Required", $errors)): ?>
                <span class="text-warning">price is Required</span>
              <?php elseif (in_array("price must be minimum of 1 characters", $errors)): ?>
                <span class="text-warning">Must be atlest 1 Charaters Limit</span>
              <?php endif; ?>
              <div class="text-warning">
                                <br>
                              </div>
            </div>
            <div class="form-group col-md-6">
              <label class="col-sm-4 form-control-label" for="exampleInputEmail2">F Price</label>
              <input type="text" class="form-control" name="f_price" id="f_price" value="<?php echo $f_price; ?>">
              <br>
              <?php if (in_array("f_price is Required", $errors)): ?>
                <span class="text-warning">price is Required</span>
              <?php elseif (in_array("f_price must be minimum of 1 characters", $errors)): ?>
                <span class="text-warning">Must be atlest 1 Charaters Limit</span>
              <?php endif; ?>
              <div class="text-warning">
                                <br>
                              </div>
            </div>
            <div class="form-group col-md-6">
              <label class="col-sm-3 form-control-label" for="exampleInputEmail2">Status</label>
              <div class="radio">
              <label class="ui-check ui-check-lg">
                <input type="radio" name="status" value="on" class="has-value" required <?php echo ($status == "on") ? "checked" : ''; ?>>
                <i class="dark-white"></i>
                Active
              </label>
              <label class="ui-check ui-check-lg">
                <input type="radio" name="status" value="off" class="has-value" <?php echo ($status == "off") ? "checked" : ''; ?>>
                <i class="dark-white"></i>
                In Active
              </label>
            </div>
              <div class="text-warning">
                                  <br>
                                </div>
            </div>
            <div class="form-group col-md-6">
              <label class="col-sm-4 form-control-label" for="exampleInputEmail2"></label>
              <input class="btn btn-warning" type="submit" name="Update" value="Update Product">
              <div class="text-warning">
                <br>
              </div>
            </div>
            <div class="form-group">

              <div class="text-warning">
                <br><br><br><br><br><br><br>
              </div>
            </div>
          </form>
          <?php else: ?>
            <form class="form-inline" role="form" action="" method="post" enctype="multipart/form-data">

                <div class="form-group col-md-6">
                  <label class="col-sm-2 form-control-label" for="exampleInputEmail2">Product Name</label>
                  <input type="text" class="form-control" name="name" id="name" placeholder="Product Name" value="">
                  <br>
                  <?php if (in_array("name is Required", $errors)): ?>
                    <span class="text-warning">productname is Required</span>
                  <?php elseif (in_array("name must be minimum of 3 characters", $errors)): ?>
                    <span class="text-warning">Must be atlest 3 Charaters Limit</span>
                  <?php endif; ?>
                  <div class="text-warning"> <br> </div>
                </div>
                <div class="form-group col-md-6">
                  <label class="col-sm-2 form-control-label" for="exampleInputEmail2">Slug</label>
                  <input type="text" class="form-control" name="slug" id="slug" placeholder="Product Slug" value="">
                  <br>
                  <?php if (in_array("slug is Required", $errors)): ?>
                    <span class="text-warning">productname is Required</span>
                  <?php elseif (in_array("slug must be minimum of 3 characters", $errors)): ?>
                    <span class="text-warning">Must be atlest 3 Charaters Limit</span>
                  <?php endif; ?>
                  <div class="text-warning">
                                      <br>
                                    </div>
                </div>
                <div class="form-group col-md-12">
                  <label class="col-sm-2 form-control-label" for="exampleInputEmail2">Description</label>
                  <textarea data-ui-jp="summernote" name="description" id="description" placeholder="Description"  rows="3" cols="80"></textarea>
                  <br>
                  <?php if (in_array("description is Required", $errors)): ?>
                    <span class="text-warning">description is Required</span>
                  <?php elseif (in_array("description must be minimum of 3 characters", $errors)): ?>
                    <span class="text-warning">Must be atlest 3 Charaters Limit</span>
                  <?php endif; ?>
                  <div class="text-warning">
                                      <br>
                                    </div>
                </div>
                <div class="form-group col-md-12">
                  <label class="col-sm-2 form-control-label" for="exampleInputEmail2">Specifications</label>
                  <textarea data-ui-jp="summernote" name="specifications" id="specifications" placeholder="Specifications" class="form-control" rows="3" cols="80" ></textarea>
                  <br>
                  <div class="text-warning">
                                      <br>
                                    </div>
                </div>
              <div class="form-group col-md-12">
                <label for="inputPassword3" class="col-sm-2 form-control-label">Images</label>
                    		<input name="image" type="file" class="form-control">
    											<?php if (in_array("image already exists", $errors)): ?>
    													<span class="text-warning">image Name already Exists</span>
    											<?php endif; ?>
                  <div class="text-warning">
                    <br>
                  </div>
              </div>

              <div class="form-group col-md-12">
                <br>
                <label class="col-sm-2 form-control-label" for="exampleInputEmail2">Select Category</label>
                    <select id="category" name="category[]" class="col-sm-5 select2-multiple" multiple data-ui-jp="select2" data-ui-options="{theme: 'bootstrap'}">
                      <optgroup label="Categories">
                        <?php $product_list = DB::getInstance()->query("SELECT * FROM category"); ?>
                        <?php foreach ($product_list->results() as $product): ?>
                          <option <?php if ($category == $product->id): ?>
                            selected
                          <?php endif; ?> value="<?php echo $product->id; ?>"><?php echo $product->name; ?></option>
                        <?php endforeach; ?>
                      </optgroup>
                    </select>
                          <div class="text-warning">

                  <br>
                </div>
              </div>

              <div class="form-group col-md-6">
                <br>
                <label class="col-sm-4 form-control-label" for="exampleInputEmail2">Minimum Order</label>
                <input type="text" class="form-control" name="min_order" id="min_order" placeholder="Minimum order">
                <br>
                <?php if (in_array("min_order is Required", $errors)): ?>
                  <span class="text-warning">Minimum order is Required</span>
                <?php elseif (in_array("min_order must be minimum of 3 characters", $errors)): ?>
                  <span class="text-warning">Must be atlest 3 Charaters Limit</span>
                <?php endif; ?>
                <div class="text-warning">

                  <br>
                </div>
              </div>
              <div class="form-group col-md-6">
                <br>
                <label class="col-sm-4 form-control-label" for="exampleInputEmail2">Return Policy</label>
                <input type="text" class="form-control" name="return" id="return" placeholder="Return Policy">
                <br>
              </div>
              <div class="col-md-12">

              </div>
              <div class="form-group col-md-6">
                <label class="col-sm-4 form-control-label" for="exampleInputEmail2">Price</label>
                <input type="text" class="form-control" name="price" id="price">
                <br>
                <?php if (in_array("price is Required", $errors)): ?>
                  <span class="text-warning">price is Required</span>
                <?php elseif (in_array("price must be minimum of 3 characters", $errors)): ?>
                  <span class="text-warning">Must be atlest 3 Charaters Limit</span>
                <?php endif; ?>
              </div>
              <div class="form-group col-md-6">
                <label class="col-sm-4 form-control-label" for="exampleInputEmail2">F Price</label>
                <input type="text" class="form-control" name="f_price" id="f_price">
                <br>
                <?php if (in_array("f_price is Required", $errors)): ?>
                  <span class="text-warning">price is Required</span>
                <?php elseif (in_array("f_price must be minimum of 1 characters", $errors)): ?>
                  <span class="text-warning">Must be atlest 1 Charaters Limit</span>
                <?php endif; ?>
                <div class="text-warning">
                                  <br>
                                </div>
              </div>
              <div class="form-group col-md-6">
                <label class="col-sm-4 form-control-label" for="exampleInputEmail2">Status</label>
                <div class="radio">
                <label class="ui-check ui-check-lg">
                  <input type="radio" name="status" value="on" class="has-value" required>
                  <i class="dark-white"></i>
                  Active
                </label>
                <label class="ui-check ui-check-lg">
                  <input type="radio" name="status" value="off" class="has-value">
                  <i class="dark-white"></i>
                  In Active
                </label>
              </div>
                <div class="text-warning">
                                    <br>
                                  </div>
              </div>
              <div class="form-group col-md-6">
                <label class="col-sm-4 form-control-label" for="exampleInputEmail2"></label>
                <input class="btn btn-warning" type="submit" name="enter" value="Add Product">
                <div class="text-warning">
                                  <br>
                                </div>
              </div>
              <div class="form-group">

                <div class="text-warning">
                  <br><br><br><br><br><br><br>
                </div>
              </div>
            </form>
            <?php endif; ?>

        </div>
      </div>

  </div>
</div>

</div>
<!-- ############ PAGE END-->

    </div>
  </div>
  <!-- / -->


  <!-- ############ SWITHCHER START-->
    <?php include_once 'rightsidebar.php'; ?>
  <!-- ############ SWITHCHER END-->

<!-- ############ LAYOUT END-->
  </div>

<!-- build:js scripts/app.min.js -->
<!-- jQuery -->
  <script src="libs/jquery/dist/jquery.js"></script>
<!-- Bootstrap -->
  <script src="libs/tether/dist/js/tether.min.js"></script>
  <script src="libs/bootstrap/dist/js/bootstrap.js"></script>
<!-- core -->
  <script src="libs/jQuery-Storage-API/jquery.storageapi.min.js"></script>
  <script src="libs/PACE/pace.min.js"></script>
  <script src="libs/jquery-pjax/jquery.pjax.js"></script>
  <script src="libs/blockUI/jquery.blockUI.js"></script>
  <script src="libs/jscroll/jquery.jscroll.min.js"></script>

  <script src="scripts/config.lazyload.js"></script>
  <script src="scripts/ui-load.js"></script>
  <script src="scripts/ui-jp.js"></script>
  <script src="scripts/ui-include.js"></script>
  <script src="scripts/ui-device.js"></script>
  <script src="scripts/ui-form.js"></script>
  <script src="scripts/ui-modal.js"></script>
  <script src="scripts/ui-nav.js"></script>
  <script src="scripts/ui-list.js"></script>
  <script src="scripts/ui-screenfull.js"></script><script src="libs/screenfull/dist/screenfull.min.js"></script>
  <script src="scripts/ui-scroll-to.js"></script>
  <script src="scripts/ui-toggle-class.js"></script>
  <script src="scripts/ui-taburl.js"></script>
  <script src="scripts/app.js"></script>
  <script src="scripts/ajax.js"></script>
<!-- endbuild -->
  <script type="text/javascript">
  // window.onload = function() {
  //   var src = document.getElementById("one"),
  //       dst = document.getElementById("two");
  //   src.addEventListener('input', function() {
  //       dst.value = src.value;
  //   });
  // };

  // jQuery implementation

  $(function () {
    var $src = $('#name'),
        $dst = $('#slug');
    $src.on('input', function () {
      console.log('op');
      var $main_val = $src.val();
          $converted = $main_val.replace(/ /g, "-");
          // high = $main_val.replace(" ", "-");
        $dst.val($converted);
    });
  });
  </script>

</body>
</html>
