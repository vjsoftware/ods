<?php
 include_once 'core/init.php';

 $link = new Link();
 $errors = [];
 $getret_id = $_GET['id'];
 $table = 'category';

 if (!$link->isLoggedIn()) {
   Redirect::to('signin.php');
 }

 if (isset($_POST['enter'])) {
 	$validate = new Validate();
	$valid = $validate->check($_POST, [
		'name' => [
			'required' => TRUE,
        'min' => 3,
        'unique' => 'category'
		],
    'sort_no' => [
      'required' => TRUE,
      'min' => 1,
      'unique' => 'category'
    ]
	]);

	if ($valid->passed()) {
    	try {
			$link-> add([
				'name' => $_POST['name'],
				'sort_no' => $_POST['sort_no']
			], $table);
Redirect::to('categorylist.php');
		} catch (Exception $e) {
			die($e);
		}

  } else {
    foreach ($valid->errors() as $error) {
      array_push($errors, $error);
    }
  }
} elseif (isset($_POST['Update'])) {
  $validate = new Validate();
  $validation = $validate->check($_POST, [
    'name' => [
      'required' => TRUE
    ]
    ]);

if ($validation->passed()) {
  if ($filepath1 == '') {
    try {
      $link->updates([
        'name' => Input::get('name'),
        'sort_no' => Input::get('sort_no')
      ], $table, $getret_id);
      Redirect::to('categorylist.php');
    } catch (Exception $e) {
      die($e);
    }
  } else {
    try {
      $link->updates([
        'name' => Input::get('name'),
        'sort_no' => Input::get('sort_no')
      ], $table, $getret_id);
      Redirect::to('categorylist.php');
    } catch (Exception $e) {
      die($e);
    }
  }
} else {
      $errors = [];
      foreach ($validation->errors() as $error) {
        array_push($errors, $error);
        // echo $error. '<br>';
      }
    }
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <title>Add Category</title>
  <meta name="description" content="Responsive, Bootstrap, BS4" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <!-- for ios 7 style, multi-resolution icon of 152x152 -->
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-barstyle" content="black-translucent">
  <link rel="apple-touch-icon" href="images/logo.png">
  <meta name="apple-mobile-web-app-title" content="Flatkit">
  <!-- for Chrome on Android, multi-resolution icon of 196x196 -->
  <meta name="mobile-web-app-capable" content="yes">
  <link rel="shortcut icon" sizes="196x196" href="images/logo.png">

  <!-- style -->
  <link rel="stylesheet" href="css/animate.css/animate.min.css" type="text/css" />
  <link rel="stylesheet" href="css/glyphicons/glyphicons.css" type="text/css" />
  <link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css" type="text/css" />
  <link rel="stylesheet" href="css/material-design-icons/material-design-icons.css" type="text/css" />
  <link rel="stylesheet" href="css/ionicons/css/ionicons.min.css" type="text/css" />
  <link rel="stylesheet" href="css/simple-line-icons/css/simple-line-icons.css" type="text/css" />
  <link rel="stylesheet" href="css/bootstrap/dist/css/bootstrap.min.css" type="text/css" />

  <!-- build:css css/styles/app.min.css -->
  <link rel="stylesheet" href="css/styles/app.css" type="text/css" />
  <link rel="stylesheet" href="css/styles/style.css" type="text/css" />
  <!-- endbuild -->
  <link rel="stylesheet" href="css/styles/font.css" type="text/css" />
</head>
<body>
  <div class="app" id="app">

<!-- ############ LAYOUT START-->

  <!-- aside -->
  <div id="aside" class="app-aside fade nav-dropdown black">
    <!-- fluid app aside -->
    <div class="navside dk" data-layout="column">
      <div class="navbar no-radius">
        <!-- brand -->
        <a href="index.html" class="navbar-brand">
        	<!-- <div data-ui-include="'images/logo.svg'"></div>
        	<img src="images/logo.png" alt="." class="hide"> -->
        	<span class="hidden-folded inline">Shop</span>
        </a>
        <!-- / brand -->
      </div>
      <?php include_once 'sidebar.php'; ?>

    </div>
  </div>
  <!-- / -->

  <!-- content -->
  <div id="content" class="app-content box-shadow-z2 bg pjax-container" role="main">
    <div class="app-header white bg b-b">
          <div class="navbar" data-pjax>
                <a data-toggle="modal" data-target="#aside" class="navbar-item pull-left hidden-lg-up p-r m-a-0">
                  <i class="ion-navicon"></i>
                </a>
                <div class="navbar-item pull-left h5" id="pageTitle">Categories</div>
                <!-- nabar right -->
                <?php include_once 'navbar.php'; ?>
                <!-- / navbar right -->
          </div>
    </div>
    <?php include_once 'footer.php' ?>
    <div class="app-body">

<!-- ############ PAGE START-->
<div class="padding">
  <div class="row">


    <div class="col-lg-12">
      <div class="box">
        <div class="box-header">
          <h2>Add Category</h2> <?php //var_dump($errors); ?>
          <!-- <small>Use bootstrap grid framework to control the input size.</small> -->
        </div>
        <div class="box-divider m-a-0"></div>
        <div class="box-body p-v-md">
          <?php if (isset($_GET['id'])):
            $get_id = $_GET['id'];
            $ret = DB::getInstance()->query("SELECT * FROM `category` WHERE `id`= '$get_id'");
            $name = $ret->first()->name;
            $sortno = $ret->first()->sort_no;
            ?>
            <form class="form-inline" role="form" action="" method="post">
              <div class="form-group">
                <label class="sr-only" for="exampleInputEmail2">Category Name</label>
                <input type="text" class="form-control" name="name" id="name" placeholder="Category Name" value="<?php echo $name; ?>">
                <br>
                <?php if (in_array("name is Required", $errors)): ?>
                  <span class="text-warning">Category Name is Required</span>
                <?php elseif (in_array("name must be minimum of 3 characters", $errors)): ?>
                  <span class="text-warning">Name Must be atlest 3 Charaters</span>
                <?php elseif (in_array("name already exists", $errors)): ?>
                  <span class="text-warning">Name already exists</span>
                <?php endif; ?>
                </div>

              <div class="form-group">
                <label class="sr-only" for="exampleInputEmail2">S.No</label>
                <input type="number" class="form-control" name="sort_no" id="sort_no" placeholder="Sort Number" value="<?php echo $sortno; ?>" >
                <br>
                <?php if (in_array("sort_no is Required", $errors)): ?>
                  <span class="text-warning">Sort Number is Required</span>
                <?php elseif (in_array("sort_no already exists", $errors)): ?>
                  <span class="text-warning">Sort Number already exists</span>
                <?php endif; ?>
              </div>

              <input class="btn btn-warning" type="submit" name="Update" value="Update Category">
              <!-- <button type="submit" class="btn white">Sign in</button> -->
            </form>
          <?php else: ?>
            <form class="form-inline" role="form" action="" method="post">
              <div class="form-group">
                <label class="sr-only" for="exampleInputEmail2">Category Name</label>
                <input type="text" class="form-control" name="name" id="name" placeholder="Category Name" value="<?php echo $_POST[name]; ?>">
                <br>
                <?php if (in_array("name is Required", $errors)): ?>
                  <span class="text-warning">Category Name is Required</span>
                <?php elseif (in_array("name must be minimum of 3 characters", $errors)): ?>
                  <span class="text-warning">Name Must be atlest 3 Charaters</span>
                <?php elseif (in_array("name already exists", $errors)): ?>
                  <span class="text-warning">Name already exists</span>
                <?php endif; ?>
                </div>

              <div class="form-group">
                <label class="sr-only" for="exampleInputEmail2">S.No</label>
                <input type="number" class="form-control" name="sort_no" id="sort_no" placeholder="Sort Number" value="<?php echo $_POST[sort_no]; ?>" >
                <br>
                <?php if (in_array("sort_no is Required", $errors)): ?>
                  <span class="text-warning">Sort Number is Required</span>
                <?php elseif (in_array("sort_no already exists", $errors)): ?>
                  <span class="text-warning">Sort Number already exists</span>
                <?php endif; ?>
              </div>

              <input class="btn btn-warning" type="submit" name="enter" value="Add Category">
              <!-- <button type="submit" class="btn white">Sign in</button> -->
            </form>
          <?php endif; ?>

        </div>
      </div>

    </div>
  </div>
</div>

<!-- ############ PAGE END-->

    </div>
  </div>
  <!-- / -->


  <!-- ############ SWITHCHER START-->
    <?php include_once 'switcher.php' ?>
  <!-- ############ SWITHCHER END-->

<!-- ############ LAYOUT END-->
  </div>

<!-- build:js scripts/app.min.js -->
<!-- jQuery -->
  <script src="libs/jquery/dist/jquery.js"></script>
<!-- Bootstrap -->
  <script src="libs/tether/dist/js/tether.min.js"></script>
  <script src="libs/bootstrap/dist/js/bootstrap.js"></script>
<!-- core -->
  <script src="libs/jQuery-Storage-API/jquery.storageapi.min.js"></script>
  <script src="libs/PACE/pace.min.js"></script>
  <script src="libs/jquery-pjax/jquery.pjax.js"></script>
  <script src="libs/blockUI/jquery.blockUI.js"></script>
  <script src="libs/jscroll/jquery.jscroll.min.js"></script>

  <script src="scripts/config.lazyload.js"></script>
  <script src="scripts/ui-load.js"></script>
  <script src="scripts/ui-jp.js"></script>
  <script src="scripts/ui-include.js"></script>
  <script src="scripts/ui-device.js"></script>
  <script src="scripts/ui-form.js"></script>
  <script src="scripts/ui-modal.js"></script>
  <script src="scripts/ui-nav.js"></script>
  <script src="scripts/ui-list.js"></script>
  <script src="scripts/ui-screenfull.js"></script>
  <script src="scripts/ui-scroll-to.js"></script>
  <script src="scripts/ui-toggle-class.js"></script>
  <script src="scripts/ui-taburl.js"></script>
  <script src="scripts/app.js"></script>
  <script src="scripts/ajax.js"></script>
<!-- endbuild -->
</body>
</html>
