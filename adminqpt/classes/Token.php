<?php

  /**
   *
   */
  class Token
  {

    public static function generate() {
      return Session::put(Config::get('session/token_name'), md5(uniqid()));
    }

    public static function check($token) {
      $tokenName = Config::get('session/token_name');

      if (session::exists($tokenName) && $token === session::get($tokenName)) {
        session::delete($tokenName);
        return TRUE;
      }
      return FALSE;
    }
  }
