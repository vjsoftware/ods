<?php
  /**
   *
   */
  class Link
  {
    private $_db;
    private $_data;
    private $_sessionName;
    private $_cookieName;
    private $_isLoggedIn;

    function __construct($user = null)
    {
      $this->_db = DB::getInstance();
      $this->_sessionName = Config::get('session/session_name');
      $this->_cookieName = Config::get('remember/cookie_name');

      if (!$user) {
        if (Session::exists($this->_sessionName)) {
          $user = Session::get($this->_sessionName);

          if ($this->find($user)) {
            $this->_isLoggedIn = TRUE;
          } else {
            // Precess Logout
          }
        }
      } else {
        $this->find($user);
      }
    }

    public function update($fields = [], $id = null)
    {
      // if (!$id && $this->isLoggedIn()) {
      //   $id = $this->data()->id;
      // }

      if (!$this->_db->update('users', $id, $fields)) {
        throw new Exception("There was an problem updating");

      }
    }

    public function updates($fields = [], $table, $id = null)
    {

      if (!$this->_db->update($table, $id, $fields)) {
        throw new Exception("There was an problem updating");

      }
    }

    public function update_sales($fields = [], $table, $id = null)
    {

      if (!$this->_db->update_sales($table, $id, $fields)) {
        throw new Exception("There was an problem updating");

      }
    }

    public function create($fields = [])
    {
      if (!$this->_db->insert('users', $fields)) {
        throw new Exception("There was a problem creating an account.");
      }
    }

    public function add($fields = [], $table)
    {
      if (!$this->_db->insert($table, $fields)) {
        throw new Exception("There was a problem creating an account.");
      }
    }

    public function delete_user($id = null)
    {

      $this->_db->delete('users', ['id', '=', $id]);

    }

    public function deletes($table, $id = null)
    {

      $this->_db->delete($table , ['id', '=', $id]);

    }

    public function find($user = null) {

      if ($user) {
        $field = (is_numeric($user)) ? 'id' : 'username';
        $data = $this->_db->get('users', [$field, '=', $user]);

        if ($data->count()) {
          $this->_data = $data->first();
          return TRUE;
        }
      }
      return FALSE;
    }

    public function login($username = null, $password = null, $remember = FALSE)
    {

      if (!$username && !$password && $this->exists()) {
        Session::put($this->_sessionName, $this->data()->id);
      } else {
      $user = $this->find($username);
        if ($user) {
          if ($this->data()->password === Hash::make($password, $this->data()->salt)) {
            Session::put($this->_sessionName, $this->data()->id);

            if ($remember) {
              $hash = Hash::unique();
              $hashCheck = $this->_db->get('users_session', ['user_id', '=', $this->data()->id]) ;

              if (!$hashCheck->count()) {
                $this->_db->insert('users_session', [
                  'user_id' => $this->data()->id,
                  'hash' => $hash
                  ]);
              } else {
                $hash = $hashCheck->first()->hash;
              }
              Cookie::put($this->_cookieName, $hash, Config::get('remember/cookie_expiry'));
            }
            return TRUE;
          }
        }
      }
      return FALSE;
    }

    public function hasPermission($key)
    {
      $group = $this->_db->get('groups', ['id', '=', $this->data()->group]);

      if ($group->count()) {
        $permissions = json_decode($group->first()->permissions, TRUE);

        if ($permissions[$key] == TRUE) {
          return TRUE;
        }
      }
      return FALSE;
    }

    public function exists() {
      return (!empty($this->_data)) ? TRUE : FALSE;
    }

    public function data() {
      return $this->_data;
    }

    public function isLoggedIn()
    {
      return $this->_isLoggedIn;
    }

    public function logout()
    {
      $this->_db->delete('users_session', ['user_id', '=', $this->data()->id]);

      Session::delete($this->_sessionName);
      Cookie::delete($this->_cookieName);
    }

  }
