<!-- <div class="header-bottom bg-1"> -->
<div class="col-md-12 menu_sticky" id="menu_sticky">
  <div class="header-bottom">
    <div class="container">
      <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-3">
          <div class="categories-menu text-uppercase click bg-7">
            <i class="fa fa-list-ul"></i>
            <span>All Categories</span>
          </div>

          <div class="menu-container toggole">
            <ul>

              <?php $cat = DB::getInstance()->query("SELECT * FROM `category` ORDER BY `sort_no` ASC"); ?>
              <?php foreach ($cat->results() as $val): ?>
                <li><a href="<?php echo $uri; ?>/products/<?php echo $val->name; ?>"><i class="<?php echo $val->icon; ?>"></i> <?php echo $val->name; ?></a></li>
                <!-- <a href="<?php echo $uri; ?>/category.php?id=<?php echo $val->id; ?>"><option value=""><?php echo $val->name; ?></option></a> -->
              <?php endforeach; ?>


            </ul>
          </div>

        </div>
        <div class="col-lg-9 col-md-9 col-sm-7 hidden-xs hidden-sm">
          <div class="mainmenu hover-bg dropdown">
            <nav>
              <ul>
                <li <?php if ($page_id == 1): ?>
                  class="active"
                <?php endif; ?>><a href="<?php echo $uri; ?>/">Home</a></li>
                <!-- <li><a href="<?php echo $uri; ?>/about.html">about us</a></li>
                <li><a href="<?php echo $uri; ?>/faq.html">F.A.Q.s</a></li>
                <li><a href="<?php echo $uri; ?>/contact.html">contact us</a></li> -->
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </div>

    <div class="mobail-menu-area">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 hidden-lg hidden-md col-sm-6">
            <div class="mobail-menu-active">
              <nav>
                <ul>
                  <li <?php if ($page_id == 1): ?>
                    class="active"
                  <?php endif; ?>><a href="<?php echo $uri; ?>/index.php">Home</a></li>
                  <!-- <li><a href="<?php echo $uri; ?>/about.html">about us</a></li>
                  <li><a href="<?php echo $uri; ?>/faq.html">F.A.Q.s</a></li>
                  <li><a href="<?php echo $uri; ?>/contact.html">contact us</a></li> -->
                </ul>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
