<?php
  include_once 'core/init.php';
  $link = new Link();
  $mail = new PHPMailer();
  $table = 'users';
  $errors = [];

  if ($link->isloggedIn()) {
     redirect::to('index.php');
   }

  if (isset($_POST['reset'])) {

    $validate = new Validate();
    $validation = $validate->check($_POST, [
      "password" => [
        'required' => TRUE,
        'min' => 8
      ],
      'password_again' => [
        'matches' => 'password'
      ]
    ]);

    $update_id = $_POST['user_id'];

    if ($validation->passed()) {
      $salt = Hash::salt(20);
      try {
        $link->update([
          'password' => Hash::make($_POST['password']. $salt),
          'salt' => $salt,
          'reset' => ''
        ], $update_id);
        Redirect::to('account');
      } catch (Exception $e) {
        echo $e;
      }

    } else {
      foreach ($validation->errors() as $error) {
        array_push($errors, $error);
      }
    }
  }

?>
<!doctype html>
<html class="no-js" lang="">


<head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Account</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- Favicon -->
		<link rel="shortcut icon" type="image/x-icon" href="<?php echo $uri; ?>/img/favicon.ico">

		<!-- All css files are included here. -->
		<!-- animate css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/animate.min.css">
		<!-- Bootstrap fremwork main css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/bootstrap.min.css">
		<!-- font-awesome css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/font-awesome.min.css">
		<!-- nivo-slider css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/nivo-slider.css">
		<!-- owl carousel css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/owl.carousel.min.css">
		<!-- icofont css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/icofont.css">
		<!-- meanmenu css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/meanmenu.css">
		<!-- jquery-ui css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/jquery-ui.min.css">
		<!-- magnific-popup css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/magnific-popup.css">
		<!-- percircle css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/percircle.css">
		<!-- Theme main style -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/style.css">
		<!-- Responsive css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/responsive.css">

		<!-- Modernizr JS -->
        <script src="<?php echo $uri; ?>/js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="<?php echo $uri; ?>/http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

		<!-- header start -->
    <?php include_once 'slider.php'; ?>

		<!-- header end -->
		<div class="main-container shop-bg">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="woocommerce-breadcrumb mtb-15">
							<div class="menu">
								<ul>
									<li><a href="<?php echo $uri; ?>/index.php">Home</a></li>
									<li class="active"><a href="<?php echo $uri; ?>/account">Password Reset</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="account-title mb-20 text-center">
							<h1>Password Reset</h1>
						</div>
					</div>
          <?php if (isset($_GET['email']) && isset($_GET['uniq'])): ?>
            <?php
              $email = $_GET['email'];
              $uniq = $_GET['uniq'];
              $email_check = DB::getInstance()->query("SELECT * FROM `users` WHERE `email` = '$email' AND `reset` = '$uniq'");
            ?>
            <?php if ($email_check->count()): ?>
              <div class="col-lg-6">
                <div class="account-heading mb-25">
                  <h2>Recovery</h2>
                </div>
                <div class="account-form form-style p-20 mb-30 bg-fff box-shadow">
                  <form action="" method="post">
                    <b>Password <span>*</span></b>
                    <input required type="hidden" name="user_id" id="user_id" value="<?php echo $email_check->first()->id; ?>">
                    <input required type="password" name="password" id="password">
                    <br>
                    <b>Confirm Password<span>*</span></b>
                    <input required type="password" name="password" id="password_again">
                    <?php if (in_array("password is Required", $errors)): ?>
                      <span class="text-warning">Password is Required</span>
                    <?php elseif (in_array("password must be minimum of 8 characters", $errors)): ?>
                      <span class="text-warning">Must be atlest 8 Charaters Limit</span>
                    <?php endif; ?>

                    <div class="text-warning">
                    <br>
                    </div>
                    <div class="login-button">
                    <button class="" type="submit" name="reset">Update</button>
                  </div>
                  </form>

                </div>
              </div>
            <?php else: ?>
              <div class="account-heading mb-25 text-center">
                <h2>Link Not Valid</h2>
              </div>
            <?php endif; ?>
          <?php else: ?>
            <div class="account-heading mb-25 text-center">
              <h2>Please Login to your Account</h2>
            </div>
          <?php endif; ?>



				</div>
			</div>
		</div>


		<!-- footer-area start -->
		<?php include_once 'footer.php'; ?>
		<!-- footer-area end -->

		<!-- Placed js at the end of the document so the pages load faster -->
		<!-- jquery latest version -->
        <script src="<?php echo $uri; ?>/js/vendor/jquery-1.12.4.min.js"></script>
		<!-- magnific popup js -->
        <script src="<?php echo $uri; ?>/js/jquery.magnific-popup.min.js"></script>
		<!-- mixitup js -->
        <script src="<?php echo $uri; ?>/js/jquery.mixitup.min.js"></script>
		<!-- jquery-ui price-->
        <script src="<?php echo $uri; ?>/js/jquery-ui.min.js"></script>
		<!-- ScrollUp Js -->
        <script src="<?php echo $uri; ?>/js/jquery.scrollUp.min.js"></script>
		<!-- countDown Js -->
        <script src="<?php echo $uri; ?>/js/jquery.countdown.min.js"></script>
		<!-- nivo slider js -->
        <script src="<?php echo $uri; ?>/js/jquery.nivo.slider.pack.js"></script>
		<!-- mobail menu js -->
        <script src="<?php echo $uri; ?>/js/jquery.meanmenu.js"></script>
		<!-- Bootstrap framework js -->
        <script src="<?php echo $uri; ?>/js/bootstrap.min.js"></script>
		<!-- owl carousel js -->
        <script src="<?php echo $uri; ?>/js/owl.carousel.min.js"></script>
		<!-- All js plugins included in this file. -->
        <script src="<?php echo $uri; ?>/js/plugins.js"></script>
		<!-- Main js file that contents all jQuery plugins activation. -->
        <script src="<?php echo $uri; ?>/js/main.js"></script>
    </body>


</html>
