<?php
  include_once 'core/init.php';
  $link = new Link();
  $table = 'sales';

  // $cs = $prod_id = $url->segment(3);

  if (isset($_POST['update_status'])) {
    $uniq = $_POST['uniq'];
    try {
      $link->update_sales([
        'status' => 3
      ], $table, $uniq);
      // die($link);
      Redirect::to('my_orders/cs');
    } catch (Exception $e) {
      echo $erru = $e->getMessage();
    }
  }
?>

<!doctype html>
<html class="no-js" lang="">


<head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>My Orders | OneDayShop</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- Favicon -->
		<link rel="shortcut icon" type="image/x-icon" href="<?php echo $uri; ?>/img/favicon.ico">

		<!-- All css files are included here. -->
		<!-- animate css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/animate.min.css">
		<!-- Bootstrap fremwork main css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/bootstrap.min.css">
		<!-- font-awesome css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/font-awesome.min.css">
		<!-- nivo-slider css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/nivo-slider.css">
		<!-- owl carousel css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/owl.carousel.min.css">
		<!-- icofont css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/icofont.css">
		<!-- meanmenu css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/meanmenu.css">
		<!-- jquery-ui css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/jquery-ui.min.css">
		<!-- magnific-popup css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/magnific-popup.css">
		<!-- percircle css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/percircle.css">
		<!-- Theme main style -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/style.css">
		<!-- Responsive css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/responsive.css">
        <link rel="stylesheet" href="<?php echo $uri; ?>/dist/sweetalert.css">


		<!-- Modernizr JS -->
        <script src="<?php echo $uri; ?>/js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="<?php echo $uri; ?>/http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

		<!-- header start -->
    <?php include_once 'header.php'; ?>

    <?php include_once 'menu.php'; ?>

    <div class="f-a-q-s-area">
			<div class="container">
        <div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="woocommerce-breadcrumb mtb-15">
							<div class="menu">
								<ul>
									<li><a href="<?php echo $uri; ?>/">Home</a></li>
									<li><a href="<?php echo $uri; ?>/orders">My Orders</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="account-title mtb-20 text-center">
              <h1>My Orders</h1>
            </div>
          </div>



        </div>
        <div class="row">
          <div class="col-lg-12">
            <div class="simple-product-tab box-shadow">

              <div class="tab-content bg-fff ">
                <div class="tab-pane active" id="cart">
                  <form class="" action="" method="post">
                  <div class="row">
                    <div class="col-lg-12">

                        <div class="checkout-area review-form form-style">
                          <ul>
                            <?php
                              $products_list = DB::getInstance()->query("SELECT * FROM `sales` WHERE `user_id` = '$user_id' AND (`status` = 1 OR `status` = 2)  ORDER BY `id` DESC");
                              $total = 0;
                              $total_main = 0;
                              $uniq = [];
                              $count = $products_list->count();
                              $i = 1;
                              $count = $products_list->count();
                              $tot_qty = '';
                              $modalid = 1;
                            ?>

                            <?php foreach ($products_list->results() as $product): ?>
                              <?php $tot_qty += $product->qty; ?>
                              <?php // $total += $product->qty * $product->price; ?>
                            <?php endforeach; ?>
                            <?php foreach ($products_list->results() as $product): ?>
                              <?php $product_details = DB::getInstance()->query("SELECT * FROM `product` WHERE `id` = '$product->product_id' AND `status` = 'on'"); ?>
                                <li id="<?php echo $modalid; ?>">
                                  <div class="review mt-20 col-lg-12">
                                    <div class="checkout-content col-lg-12" style="margin-left: 0;">

                                      <div class="checkout-img col-lg-4 p-15">
                                        <?php if (file_exists('resized/'.$product_details->first()->image)): ?>
                                          <img src="<?php echo $uri; ?>/resized/<?php echo $product_details->first()->image; ?>" alt="<?php echo $product_details->first()->name; ?>" />
                                        <?php else: ?>
                                          <img src="<?php echo $uri; ?>/images/<?php echo $product_details->first()->image; ?>" alt="<?php echo $product_details->first()->name; ?>" />
                                        <?php endif; ?>
                                      </div>
                                      <div class="col-lg-6">
                                        <div class="col-lg-12 mt-50">

                                        </div>
                                        <div class="col-lg-12">
                                          <b>Order ID: <?php echo $product->uniq; ?></b>
                                        </div>
                                        <div class="col-lg-12">
                                          <b><?php echo substr($product_details->first()->name, 0, 15); ?> -</b> <b class="pull-right modalClick" data-toggle="modal" data-target="#myModal<?php echo $modalid; ?>" style="cursor: pointer; background-color: #ebebeb; padding: 0 10px;">&nbsp; Cancel &nbsp;</b>
                                        </div>
                                        <div class="col-lg-12">
                                          <p>Price: <span class="text-warning"><?php echo $product->price; ?></span></p>
                                          <p>Qty &nbsp;&nbsp;: <?php echo $product->qty; ?></p>
                                          <p>Total:
                                          <?php if ($product->coupoun): ?>
                                            <del class="text-warning"><?php echo $product->qty * $product->price; ?></del>
                                            <?php
                                              $coupoun_details = DB::getInstance()->query("SELECT * FROM `coupons` WHERE `code` = '$product->coupoun'");
                                              if ($coupoun_details->count()) {
                                                $cop_type = $coupoun_details->first()->type;
                                                $cop_value = $coupoun_details->first()->value;
                                                $tot_before = $product->qty * $product->price;
                                                if ($cop_type == 1) { ?>
                                                <span> <?php echo $tot_before - $cop_value; ?>  </span>
                                                <?php $total_main += $tot_before - $cop_value; ?>
                                                <?php } elseif ($cop_type == 2) { ?>
                                                <span><?php echo $tot_before - ($tot_before * ($cop_value / 100)); ?></span>
                                                <?php $total_main += $tot_before - ($tot_before * ($cop_value / 100)); ?>
                                                <?php }
                                              }
                                            ?>
                                          <?php else: ?>
                                            <span class="text-warning"><?php echo $product->qty * $product->price; ?></span>
                                            <?php $total_main += $product->qty * $product->price; ?>
                                          <?php endif; ?></p>
                                        </div>

                                      </div>

                                    </div>
                                  </div>

                                </li>
                                <!-- Modal Start -->
                                <div class="modal fade" id="myModal<?php echo $modalid; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content" style="border-radius: 0;">
                                            <div class="modal-header">
                                                <!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button> -->
                                                <h4 class="modal-title" id="myModalLabel">Request Cencellation<b class="pull-right" data-dismiss="modal">X</b></h4>
                                                </div>
                                                <?php $product_details = DB::getInstance()->query("SELECT * FROM `product` WHERE `id` = '$product->product_id' AND `status` = 'on'"); ?>
                                            <div class="modal-body">
                                                <div class="row">
                                                  <div class="col-md-3">
                                                    <img src="<?php if (file_exists('thumb/'.$product_details->first()->image)): ?>
                                                      <?php echo $uri; ?>/thumb/<?php echo $product_details->first()->image; ?>" alt="<?php echo $product_details->first()->name; ?>
                                                    <?php else: ?>
                                                      <?php echo $uri; ?>/images/<?php echo $product_details->first()->image; ?>" alt="<?php echo $product_details->first()->name; ?>
                                                    <?php endif; ?>" width="80" height="80" border="0" class="img-circle"></a>
                                                  </div>
                                                  <div class="col-md-9">
                                                    <div class="col-md-4">
                                                      <span><b>Item Name</b></span>
                                                    </div>
                                                    <div class="col-md-2">
                                                      <span><b>Qty</b></span>
                                                    </div>
                                                    <div class="col-md-3">
                                                      <span><b>Total</b></span>
                                                    </div>
                                                    <div class="col-md-12">

                                                    </div>
                                                    <div class="col-md-4">
                                                      <span><?php echo $product_details->first()->name; ?> -</span>
                                                    </div>
                                                    <div class="col-md-2">
                                                      <span><?php echo $product->qty; ?></span>
                                                    </div>
                                                    <div class="col-md-3">
                                                      <span>Rs. <?php if ($product->coupoun): ?>
                                                        <?php
                                                          $coupoun_details = DB::getInstance()->query("SELECT * FROM `coupons` WHERE `code` = '$product->coupoun'");
                                                          if ($coupoun_details->count()) {
                                                            $cop_type = $coupoun_details->first()->type;
                                                            $cop_value = $coupoun_details->first()->value;
                                                            $tot_before = $product->qty * $product->price;
                                                            if ($cop_type == 1) { ?>
                                                            <span> <?php echo $tot_before - $cop_value; ?>  </span>
                                                            <?php $total += $tot_before - $cop_value; ?>
                                                            <?php } elseif ($cop_type == 2) { ?>
                                                            <span><?php echo $tot_before - ($tot_before * ($cop_value / 100)); ?></span>
                                                            <?php $total += $tot_before - ($tot_before * ($cop_value / 100)); ?>
                                                            <?php }
                                                          }
                                                        ?>
                                                      <?php else: ?>
                                                        <span class="text-warning"><?php echo $product->qty * $product->price; ?></span>
                                                        <?php $total += $product->qty * $product->price; ?>
                                                      <?php endif; ?>
                                                    </span>
                                                    </div>
                                                  </div>
                                                <!-- <span><strong>Order ID: </strong></span>
                                                    <span class="label label-warning" style="font-family: sans-serif;">87946546</span> -->
                                                </div>
                                                <hr>
                                                <div class="row">
                                                  <div class="col-md-4">
                                                    <label for="">Reason for cancellation*</label>
                                                  </div>
                                                  <div class="col-md-8">
                                                    <select class="form-control" name="reason" id="reason<?php echo $modalid; ?>" style="border-radius: 0;">
                                                      <option value="">Select</option>
                                                      <option value="1">Order Placed by Mistake</option>
                                                      <option value="2">Expected Delivery time Is too Long</option>
                                                      <option value="3">I'm not Available at Given Delivery Time</option>
                                                      <option value="4">Price Of Product is High</option>
                                                      <option value="5">Others</option>
                                                    </select>
                                                    <div class="text-warning hidden" id="hide<?php echo $modalid; ?>">
                                                      This Field is Required
                                                    </div>
                                                  </div>
                                                  <div class="col-md-12">
                                                    <br>
                                                  </div>
                                                  <div class="col-md-4">
                                                    <label for="">Comments</label>
                                                  </div>
                                                  <div class="col-md-8">
                                                    <textarea class="form-control" name="comment" id="comment<?php echo $modalid; ?>" rows="3" style="border-radius: 0; height: 75px;"></textarea>
                                                    <input type="hidden" name="delete_id" id="delete_id<?php echo $modalid; ?>" value="<?php echo $modalid; ?>">
                                                    <input type="hidden" name="delete_order_id" id="delete_order_id<?php echo $modalid; ?>" value="<?php echo $product->uniq; ?>">
                                                  </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <div class="col-md-6">

                                                </div>
                                                <div class="col-md-2">
                                                    <button type="button" data-dismiss="modal">Close</button>
                                                </div>
                                                <div class="col-md-4">
                                                  <button class="cancel_order" type="button" onclick="delete_item(<?php echo $modalid; ?>)" value="<?php echo $modalid; ?>">Confirm Cancel</button>
                                                  <!-- <span class="cancel_order">Confirm Cancel</span> -->
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Modal End -->
                              <?php array_push($uniq, $product->uniq); ?>

                              <?php $modalid++; ?>
                            <?php endforeach; ?>

                          </ul>




                        </div>


                    </div>
                  </div>
                  </form>


                </div>


              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <br>
          </div>
        </div>
			</div>
		</div>

		<!-- header end -->

		</div>


		<!-- footer-area start -->
		<?php include_once 'footer.php'; ?>
    <script src="<?php echo $uri; ?>/dist/sweetalert.min.js"> </script>
		<!-- footer-area end -->
    <script type="text/javascript">
      $('.modalClick').click(function() {
        $('#menu_sticky').removeClass('menu_sticky');
        $('#menu_sticky').removeClass('sticky');
        $('#menu_sticky').removeAttr("style");
      })
    </script>
    <script type="text/javascript">
    // Order Cancel Start
      function delete_item(orderId) {
        // var tt = $(this).val();
        // console.log(orderId);
        var res = "#reason" + orderId;
        var reason = $(res).val();
        var comment = $('#comment'+orderId).val();
        var delete_id = $('#delete_id'+orderId).val();
        var delete_order_id = $('#delete_order_id'+orderId).val();
        var hide = '#hide'+orderId;
        if (reason != '') {
          $(hide).addClass('hidden');
          $.ajax({
                  url: "cancel_order_main.php",
                  type: "post",
                  dataType : 'json',
                  data: {reason: reason, comment: comment, delete_id: delete_id, delete_order_id: delete_order_id} ,
                  success: function (response) {
                     // you will get response from your php page (what you echo or print)
                    //  console.log("Success");
                    //  console.log(response.delete_id);
                     $('#'+response.delete_id).remove();
                     $('#myModal'+response.delete_id).modal('toggle');
                     swal("Order Canceled Successfully", "Continue ...!", "success");
                    //  console.log(response.less);
                  },
                  error: function(jqXHR, textStatus, errorThrown) {
                     console.log(textStatus, errorThrown);
                  }


              });
        } else {
          $(hide).removeClass('hidden');
        }

      };
    </script>

    </body>


</html>
