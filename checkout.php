<?php
include_once 'core/init.php';

$link = new Link();

$user_id = $link->data()->id;

if (isset($_POST['cod'])) {
  $order_id = $_POST['order_id'];
  $addr = $_POST['chooseee'];

  if ($order_id != '') {
    Redirect::to("order_received.php?order_id=".$order_id.'&address='.$addr);
  } else {
    $eror = "Please ADD products to cart";
  }
}

$MERCHANT_KEY = "zcnui9R8";

// Merchant Salt as provided by Payu
$SALT = "cmfcC7re41";


// End point - change to https://secure.payu.in for LIVE mode
$PAYU_BASE_URL = "https://test.payu.in";

$action = '';

$posted = array();
if(!empty($_POST)) {
  // print_r($_POST);
foreach($_POST as $key => $value) {
  $posted[$key] = $value;

}
}

$formError = 0;

if(empty($posted['txnid'])) {
// Generate random transaction id
$txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
} else {
$txnid = $posted['txnid'];
}
$hash = '';

// Hash Sequence
$hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
if(empty($posted['hash']) && sizeof($posted) > 0) {

if(
        empty($posted['key'])
        || empty($posted['txnid'])
        || empty($posted['amount'])
        || empty($posted['firstname'])
        || empty($posted['email'])
        || empty($posted['phone'])
        || empty($posted['productinfo'])
        || empty($posted['surl'])
        || empty($posted['furl'])
    || empty($posted['service_provider'])
) {
  $formError = 1;

} else {

  //$posted['productinfo'] = json_encode(json_decode('[{"name":"tutionfee","description":"","value":"500","isRequired":"false"},{"name":"developmentfee","description":"monthly tution fee","value":"1500","isRequired":"false"}]'));
$hashVarsSeq = explode('|', $hashSequence);
  $hash_string = '';
foreach($hashVarsSeq as $hash_var) {
    $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
    $hash_string .= '|';
  }

  $hash_string .= $SALT;


  $hash = strtolower(hash('sha512', $hash_string));
  $action = $PAYU_BASE_URL . '/_payment';
  // die('lop');
  // Redirect::to($action);
}
} elseif(!empty($posted['hash'])) {
$hash = $posted['hash'];
$action = $PAYU_BASE_URL . '/_payment';
}




$date = date('d-m-Y');
$time = date('h:i A');
$table = 'address';

if (isset($_POST['add']) && !empty($_POST['address'])) {
  $validate = new Validate();
  $val_validate = $validate->check($_POST, [
    'address' => [
      'required' => TRUE
    ]
  ]);

  if ($val_validate->passed()) {
    try {
      $link->add([
        'user_id' => $user_id,
        'address' => $_POST['address'],
        'address_street' => $_POST['address_street'],
        'town' => $_POST['town'],
        'pincode' => $_POST['pincode'],
        'date' => $date,
        'time' => $time
      ], $table);
      Redirect::to('checkout');
    } catch (Exception $e) {
      echo $e;
    }
  } else {

    }
}




?>

<!doctype html>
<html class="no-js" lang="">

<!-- Mirrored from devitems.com/html/oneclick-preview/oneclick/checkout.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 17 Jan 2017 10:59:16 GMT -->
<head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Checkout</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- Favicon -->
		<link rel="shortcut icon" type="image/x-icon" href="<?php echo $uri; ?>/img/favicon.ico">

		<!-- All css files are included here. -->
		<!-- animate css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/animate.min.css">
		<!-- Bootstrap fremwork main css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/bootstrap.min.css">
		<!-- font-awesome css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/font-awesome.min.css">
		<!-- nivo-slider css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/nivo-slider.css">
		<!-- owl carousel css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/owl.carousel.min.css">
		<!-- icofont css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/icofont.css">
		<!-- meanmenu css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/meanmenu.css">
		<!-- jquery-ui css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/jquery-ui.min.css">
		<!-- magnific-popup css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/magnific-popup.css">
		<!-- percircle css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/percircle.css">
		<!-- Theme main style -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/style.css">
		<!-- Responsive css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/responsive.css">

        <style media="screen">
        @media (min-width: 768px) {
          .col-lg-4 {
            width: 30%;
          }
         }

          /* Medium devices (desktops, 992px and up) */
          @media (min-width: 992px) {
            .col-lg-4 {
              width: 30%;
            }
           }

          /* Large devices (large desktops, 1200px and up) */
          @media (min-width: 1200px) {
            .col-lg-4 {
              width: 30%;
            }
           }
        </style>

		<!-- Modernizr JS -->
        <script src="<?php echo $uri; ?>/js/vendor/modernizr-2.8.3.min.js"></script>
        <script>
    var hash = '<?php echo $hash ?>';
    function submitPayuForm() {
      if(hash == '') {
        return;
      }
      var payuForm = document.forms.payuForm;
      payuForm.submit();
    }
  </script>
    </head>
    <body onload="submitPayuForm()">
		<!-- header start -->
		<header>

      <?php include_once 'header.php'; ?>

      <?php include_once 'menu.php'; ?>

		</header>
		<!-- header end -->
		<!-- start checkout -->
		<div class="main-container">
			<div class="container">
				<div class="row" id="breadcrum">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="menu mtb-15">
							<ul>
								<li><a href="<?php echo $uri; ?>/">Home</a></li>
								<li class="active"><a href="<?php echo $uri; ?>/checkout">Checkout</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="checkout-title text-center mtb-20">
              <?php if ($eror == ''): ?>
                <h1 id="title">Checkout</h1>
              <?php else: ?>
                <h1><?php echo $eror; ?></h1>
              <?php endif; ?>
						</div>
					</div>
				</div>
        <div class="row">
          <div class="col-lg-12">
            <div class="simple-product-tab box-shadow">
              <div class="checkout-tab-menu clear">
                <ul>
                  <li class="active"><a href="<?php echo $uri; ?>/#cart" data-toggle="tab">Cart</a></li>
                  <div style="width: 1px; background-color: #ebe9eb; display: inline-block; position: absolute; top: 10px;">
                    &nbsp;
                  </div>
                  <li id="credit"><a href="<?php echo $uri; ?>/#pay_now" data-toggle="tab">Credit / Debit / Net Banking</a></li>
                  <div style="width: 1px; background-color: #ebe9eb; display: inline-block; position: absolute; top: 10px;">
                    &nbsp;
                  </div>
                  <li id="cod"><a href="<?php echo $uri; ?>/#cash_on_delivery" data-toggle="tab">Cash On Delivery</a></li>
                </ul>
              </div>
              <div class="tab-content bg-fff ">
                <div class="tab-pane active" id="cart">
                  <div class="row">
                    <div class="col-lg-12">

                        <div class="checkout-area">
                          <ul>
                            <?php
                              $products_list = DB::getInstance()->query("SELECT * FROM `cart` WHERE `user_id` = '$user_id'");
                              $total = 0;
                              $uniq = [];
                              $count = $products_list->count();
                              $i = 1;
                              $count = $products_list->count();
                              $tot_qty = '';
                            ?>
                            <?php foreach ($products_list->results() as $product): ?>
                              <?php $tot_qty += $product->qty; ?>
                              <?php // $total += $product->qty * $product->price; ?>
                            <?php endforeach; ?>
                            <?php foreach ($products_list->results() as $product): ?>
                              <?php $product_details = DB::getInstance()->query("SELECT * FROM `product` WHERE `id` = '$product->product_id' AND `status` = 'on'"); ?>
                              <li>
                                <div class="review mb-20" style="">
                                  <div class="checkout-content p-15 col-lg-4">
                                    <b><?php echo substr($product_details->first()->name, 0, 15); ?> -</b>

                                    <div class="checkout-img">
                                      <?php if (file_exists('thumb/'.$product_details->first()->image)): ?>
                                        <img src="<?php echo $uri; ?>/thumb/<?php echo $product_details->first()->image; ?>" alt="<?php echo $product_details->first()->name; ?>" />
                                      <?php else: ?>
                                        <img src="<?php echo $uri; ?>/images/<?php echo $product_details->first()->image; ?>" alt="<?php echo $product_details->first()->name; ?>" />
                                      <?php endif; ?>
                                    </div>
                                    <p style="margin-bottom: 0;">Price: <span class="text-warning"><?php echo $product->price; ?></span><br> Qty &nbsp;&nbsp;: <?php echo $product->qty; ?>
                                      <br>
                                      Total:
                                      <?php if ($product->coupoun): ?>
                                        <del class="text-warning"><?php echo $product->qty * $product->price; ?></del>
                                        <?php
                                          $coupoun_details = DB::getInstance()->query("SELECT * FROM `coupons` WHERE `code` = '$product->coupoun'");
                                          if ($coupoun_details->count()) {
                                            $cop_type = $coupoun_details->first()->type;
                                            $cop_value = $coupoun_details->first()->value;
                                            $tot_before = $product->qty * $product->price;
                                            if ($cop_type == 1) { ?>
                                            <span> <?php echo $tot_before - $cop_value; ?>  </span>
                                            <?php $total += $tot_before - $cop_value; ?>
                                            <?php } elseif ($cop_type == 2) { ?>
                                            <span><?php echo $tot_before - ($tot_before * ($cop_value / 100)); ?></span>
                                            <?php $total += $tot_before - ($tot_before * ($cop_value / 100)); ?>
                                            <?php }
                                          }
                                        ?>
                                      <?php else: ?>
                                        <span class="text-warning"><?php echo $product->qty * $product->price; ?></span>
                                        <?php $total += $product->qty * $product->price; ?>
                                      <?php endif; ?>
                                  </div>
                                </div>
                              </li>

                              <?php array_push($uniq, $product->uniq); ?>
                            <?php endforeach; ?>
                            <div class="col-lg-12">

                            </div>
                            <li>
                              <div class="review mb-20" style="">
                                <div class="col-md-8">

                                </div>
                                <div class="checkout-content p-15 col-md-3">
                                  <b>Total Amount -</b>

                                  <p style="margin-bottom: 0;">Items: <span class="text-warning"><?php echo $count; ?></span><br> Units: <?php echo $tot_qty; ?> <br>Total: <span class="text-warning"><?php echo $total; ?></span></p>
                                </div>
                              </div>
                            </li>
                          </ul>
                        </div>


                    </div>
                  </div>



                </div>
                <div class="tab-pane" id="pay_now">
                  <div class="product-reviews p-20">
                    <form action="<?php echo $action; ?>" method="post" name="payuForm">
                    <div class="row">
                      <div class="col-lg-8">
                        <div class="checkout-area-address">
                          <div class="col-lg-1"> </div>
                          <h4>Select Shipping Address</h4>
                          <ul>
                            <?php $address_details = DB::getInstance()->query("SELECT * FROM `address` WHERE `user_id` = '$user_id' ORDER BY `id` ASC"); ?>
                            <?php $ii = 1; ?>
                              <?php foreach ($address_details->results() as $address): ?>
                                <li class="checkout_paynow">
                                  <input required type="radio" title="Please Select Address" name="choose" value="<?php echo $address->id; ?>" id="r<?php echo $ii; ?>" />
                                  <label class="radio" for="r<?php echo $ii; ?>">
                                  <div class="review mb-20">
                                    <div class="checkout-address-content p-15">
                                      <?php echo $address->address; ?>
                                      <br>
                                      <?php echo $address->address_street; ?>
                                      <br>
                                      <?php echo $address->town; ?>
                                    </div>
                                  </div>
                                </label>
                                <input type="hidden" name="key" value="<?php echo $MERCHANT_KEY; ?>" />
                                <input type="hidden" name="hash" value="<?php echo $hash ?>"/>
                                <input type="hidden" name="txnid" value="<?php echo $txnid ?>" />
                                <input type="hidden" name="amount" value="<?php echo (int)$total; ?>" />
                                <input type="hidden" name="firstname" id="firstname" value="<?php echo $link->data()->username; ?>" />
                                <input type="hidden" name="email" id="email" value="<?php echo $link->data()->email; ?>" />
                                <input type="hidden" name="phone" value="<?php echo $link->data()->phone; ?>" />
                                <?php $choose = $_POST['choose']; ?>
                                <?php $rand = implode(",",$uniq); ?>
                                <input type="hidden" name="productinfo" value="<?php echo $rand; ?>" size="64" />
                                <input type="hidden" name="addr" value="<?php echo $choose; ?>" size="64" />
                                <input type="hidden" name="surl" value="<?php echo $uri."/pymntsuccess.php?order_id=$rand"."&address=$choose"; ?>" />
                                <input type="hidden" name="furl" value="<?php echo $uri."/pymntfailure.php"; ?>" />
                                <input type="hidden" name="service_provider" value="payu_paisa" size="64" />
                                </li>
                                <?php $ii++; ?>
                              <?php endforeach; ?>



                          </ul>
                        </div>

                      </div>
                      <div class="col-lg-4">
                        <div class="review-form form-style">
                          <h2>Add Address </h2>

                            <p>Door Number *</p>
                            <textarea style="height: 60px;" class="form-control" name="address" id="#" cols="10" rows="5"></textarea>
                            <p>Street *</p>
                            <input class="form-control" type="text" name="address_street" />
                            <p>City *</p>
                            <input class="form-control" type="text" name="town"/>
                            <p>Pincode *</p>
                            <input class="form-control" type="text" name="pincode"/>
                            <button name="add">Submit</button>

                        </div>
                      </div>
                      <br>
                      <div class="col-lg-12">
                        <!-- <div class="col-md-1"> </div> -->
                        <div class="col-lg-8">
                          <div class="form-group">
                            <input type="submit" class="form-control btn btn-warning" name="pay_now" value="Pay Now">
                          </div>
                        </div>
                      </div>
                    </div>
                  </form>
                  </div>
                </div>
                <div class="tab-pane" id="cash_on_delivery">
                  <div class="product-reviews p-20">
                    <form action="" method="post" name="">
                    <div class="row">
                      <div class="col-lg-8">
                        <div class="checkout-area-address">
                          <div class="col-lg-1"> </div>
                          <h4>Select Shipping Address</h4>
                          <ul>
                            <?php $address_details = DB::getInstance()->query("SELECT * FROM `address` WHERE `user_id` = '$user_id' ORDER BY `id` ASC"); ?>
                            <?php $iy = 111; ?>
                              <?php foreach ($address_details->results() as $address): ?>
                                <li class="checkout_paynow">
                                  <input required type="radio" name="chooseee" title="Please Select Address" value="<?php echo $address->id; ?>" id="r<?php echo $iy; ?>" />
                                  <label class="radio" for="r<?php echo $iy; ?>">
                                  <div class="review mb-20">
                                    <div class="checkout-address-content p-15">
                                      <?php echo $address->address; ?>
                                      <br>
                                      <?php echo $address->address_street; ?>
                                      <br>
                                      <?php echo $address->town; ?>
                                    </div>
                                  </div>
                                </label>
                                <?php $rand = implode(",",$uniq); ?>
                                <input type="hidden" name="order_id" value="<?php echo $rand; ?>" size="64" />
                                </li>
                                <?php $iy++; ?>
                              <?php endforeach; ?>



                          </ul>
                        </div>

                      </div>
                      <div class="col-lg-4">
                        <div class="review-form form-style">
                          <h2>Add Address </h2>

                            <p>Door Number *</p>
                            <textarea style="height: 60px;" class="form-control" name="address" id="#" cols="10" rows="5"></textarea>
                            <p>Street *</p>
                            <input class="form-control" type="text" name="address_street" />
                            <p>City *</p>
                            <input class="form-control" type="text" name="town"/>
                            <p>Pincode *</p>
                            <input class="form-control" type="text" name="pincode"/>
                            <button name="add">Submit</button>

                        </div>
                      </div>
                      <br>
                      <div class="col-lg-12">
                        <!-- <div class="col-md-1"> </div> -->
                        <div class="col-lg-8">
                          <div class="form-group">
                            <input type="submit" class="form-control btn btn-warning" name="cod" value="Place Order">
                          </div>
                        </div>
                      </div>
                    </div>
                  </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <br>


        <div class="col-md-12">
          <br><br>
        </div>
			</div>
		</div>

		<!-- footer-area start -->
		<?php include_once 'footer.php'; ?>
		<!-- footer-area end -->
    </body>
    <script type="text/javascript">
    $("#cart, #credit, #cod").click(function() {
      // alert('op');
      $('html,body').animate({
        scrollTop: $("#breadcrum").offset().top},
        'slow');
      });
      </script>
      <script>
        function validate(evt) {
          var theEvent = evt || window.event;
          var key = theEvent.keyCode || theEvent.which;
          key = String.fromCharCode( key );
          var regex = /[0-9]|\./;
          if( !regex.test(key) ) {
            theEvent.returnValue = false;
            if(theEvent.preventDefault) theEvent.preventDefault();
          }
        }
      </script>
<!-- Mirrored from devitems.com/html/oneclick-preview/oneclick/checkout.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 17 Jan 2017 10:59:16 GMT -->
</html>
