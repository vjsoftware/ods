<?php

  require_once 'core/init.php';

  $link = new Link();

  $user_id = $link->data()->id;

  $table = 'sales';
  $table2 = 'cart';


  $total = 0;
  $product_qty = 0;

  $result = [];
  if (isset($_POST['reason']) && !empty($_POST['reason'])) {
    $reason = $_POST['reason'];
    $comment = $_POST['comment'];
    $delete_id = $_POST['delete_id'];
    $delete_order_id = $_POST['delete_order_id'];

    try {
      $link->delete_cart($table, $delete_order_id);
      $link->delete_cart($table2, $delete_order_id);
      $cart_check = DB::getInstance()->query("SELECT * FROM `cart` WHERE `user_id` = '$user_id'");
      foreach ($cart_check->results() as $cart_val) {
        $product_qty += $cart_val->qty;
        $product_qty_less = $cart_val->qty;
        $product_id = $cart_val->product_id;
        $product_coupon = $cart_val->coupoun;
        $product_details = DB::getInstance()->query("SELECT * FROM `product` WHERE `id` = '$product_id' AND `status` = 'on'");
        $product_price = $product_details->first()->price;
        if ($cart_val->coupoun) {
          $coupon_chech = DB::getInstance()->query("SELECT * FROM `coupons` WHERE `code` = '$product_coupon'");
          if ($coupon_chech->count()) {
            $cop_type = $coupon_chech->first()->type;
            $cop_value = $coupon_chech->first()->value;
            $tot_before = $product_qty_less * $product_price;
            if ($cop_type == 1) {
              $total += $tot_before - $cop_value;
            } elseif ($cop_type == 2) {
              $total += $tot_before - ($tot_before * ($cop_value / 100));
            }
          }
        } else {
          $total += $product_qty_less * $product_price;
        }
      }
      $result['less']  = $total;
      $result['qty']  = $product_qty;

      $result['delete_id']  = $delete_id;
      $result['status']  = 'ok';
    } catch (Exception $e) {
      echo $e;
    }

  }

  echo json_encode($result);
?>
