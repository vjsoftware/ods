<footer class="bg-fff bt">
  <div class="footer-top-area ptb-35 bb">
    <div class="container">
      <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
          <div class="footer-widget">
            <div class="footer-logo mb-25">
              <img src="<?php echo $uri; ?>/img/1dayshopfooterlogo.png" alt="" />
            </div>
            <div class="footer-content">
              <p>OneDayShop Offers unique range of products with up to 80% discount on every Product</p>

            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
          <div class="footer-widget">
            <h3 class="footer-title bb mb-20 pb-15">About Us</h3>
            <ul>
              <li>
                <div class="contuct-content">
                  <!-- <div class="contuct-icon">
                    <i class="fa fa-map-marker"></i>
                  </div> -->
                  <!-- <div class="contuct-info">
                    <span>#20-48-M26-228, Up Stairs Opp. municipal Park, Tirupati-517501</span>
                  </div> -->
                </div>
              </li>
              <li>
                <div class="contuct-content">
                  <div class="contuct-icon">
                    <i class="fa fa-fax"></i>
                  </div>
                  <div class="contuct-info">
                    <span>0877-656-1199</span>
                  </div>
                </div>
              </li>
              <li>
                <div class="contuct-content">
                  <div class="contuct-icon">
                    <i class="fa fa-envelope"></i>
                  </div>
                  <div class="contuct-info">
                    <span>support@onedayshop.in</span>
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
          <div class="footer-widget">
            <h3 class="footer-title bb mb-20 pb-15">Information</h3>
            <div class="footer-menu home3-hover">
              <ul>
                <li><a href="<?php echo $uri; ?>/#">Our Blog</a></li>
                <li><a href="<?php echo $uri; ?>/#">About Our Shop</a></li>
                <li><a href="<?php echo $uri; ?>/#">Secure Shopping</a></li>
                <li><a href="<?php echo $uri; ?>/#">Privacy Policy</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
          <div class="footer-widget">
            <h3 class="footer-title bb mb-20 pb-15">My account</h3>
            <div class="footer-menu home3-hover">
              <ul>
                <li><a href="<?php echo $uri; ?>/account">My Account</a></li>
                <li><a href="<?php echo $uri; ?>/checkout">Checkout</a></li>
                <li><a href="<?php echo $uri; ?>/cart">Shopping Cart</a></li>
                <!-- <li><a href="<?php echo $uri; ?>/wishlist.html">Wishlist</a></li> -->
              </ul>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
          <div class="footer-widget">
            <h3 class="footer-title bb mb-20 pb-15">Social Media</h3>
            <div class="footer-menu">

              <ul>
                <li>
                  <a href="https://www.facebook.com/1onedayshop/" target="_blank" data-toggle="tooltip" title="Facebook"><i class="fa fa-facebook"></i> Facebook</a>
                </li>
                <li>
                  <a href="#" data-toggle="tooltip" title="Twitter"><i class="fa fa-twitter"></i> Twitter</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="footer-bottom ptb-20">
    <div class="container">
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="copyright">
          <span>Copyright &copy; 2017 <a href="http://shuklait.com/">Shukla IT Solutions</a> All Rights Reserved.</span>
        </div>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="mayment text-right">
          <a href="<?php echo $uri; ?>/#">
            <img src="<?php echo $uri; ?>/img/p14.png" alt="" />
          </a>
        </div>
      </div>
    </div>
  </div>
</footer>

<!-- Placed js at the end of the document so the pages load faster -->
<!-- jquery latest version -->
    <script src="<?php echo $uri; ?>/js/vendor/jquery-1.12.4.min.js"></script>
<!-- magnific popup js -->
    <script src="<?php echo $uri; ?>/js/jquery.magnific-popup.min.js"></script>
<!-- mixitup js -->
    <script src="<?php echo $uri; ?>/js/jquery.mixitup.min.js"></script>
<!-- jquery-ui price-->
    <script src="<?php echo $uri; ?>/js/jquery-ui.min.js"></script>
<!-- ScrollUp Js -->
    <script src="<?php echo $uri; ?>/js/jquery.scrollUp.min.js"></script>
<!-- countDown Js -->
    <script src="<?php echo $uri; ?>/js/jquery.countdown.min.js"></script>
<!-- nivo slider js -->
    <script src="<?php echo $uri; ?>/js/jquery.nivo.slider.pack.js"></script>
<!-- mobail menu js -->
    <script src="<?php echo $uri; ?>/js/jquery.meanmenu.js"></script>
<!-- Bootstrap framework js -->
    <script src="<?php echo $uri; ?>/js/bootstrap.min.js"></script>
<!-- owl carousel js -->
    <script src="<?php echo $uri; ?>/js/owl.carousel.min.js"></script>
<!-- All js plugins included in this file. -->
    <script src="<?php echo $uri; ?>/js/plugins.js"></script>
<!-- Main js file that contents all jQuery plugins activation. -->
    <script src="<?php echo $uri; ?>/js/main.js"></script>
    <?php if ($page_id != 'account'): ?>
      <!-- <script src="<?php echo $uri; ?>/js/scroll.js"></script> -->
      <script src="<?php echo $uri; ?>/sticky/jquery.stickyNavbar.min.js"> </script>
      <script>

        $(function () {
           $('.menu_sticky').stickyNavbar({
             stickyModeClass: "sticky",      // Class that will be applied to 'this' in sticky mode
             unstickyModeClass: "unsticky"   // Class that will be applied to 'this' in non-sticky mode
           });
        });
      </script>
    <?php endif; ?>
    <script src="<?php echo $uri; ?>/js/typeahead.bundle.js"></script>
    <script src="<?php echo $uri; ?>/js/globalTypeAhead.js"> </script>
