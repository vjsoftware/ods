<?php

    require_once 'core/init.php';

    $link = new Link();

    $user_id = $link->data()->id;

    $table = 'cart';
    $table1 = 'sales';
      if (isset($_POST['remove'])) {
        $uniq = $_POST['uniq'];
        try {
          $link->delete_cart($table, $uniq);
          $link->delete_cart($table1, $uniq);
        } catch (Exception $e) {
          die($e);
        }

      }
      if (isset($_POST['apply_coupon']) && !empty($_POST['apply_coupon'])) {
        $coupon_code = $_POST['coupon_code'];
        $coupon_check = DB::getInstance()->query("SELECT * FROM `coupons` WHERE `code` = '$coupon_code'");
        $total_amount = $coupon_check->first()->total_amount;
        if ($_POST['coupon_total'] >= $total_amount) {
          $product_id = $coupon_check->first()->product;
          if ($coupon_check->count()) {
            if ($coupon_check->first()->product == 0) {
              $category_id = $coupon_check->first()->category;
              $category_check = DB::getInstance()->query("SELECT * FROM 'product' WHERE `category` = '$category_id'");
              if ($category_check->count()) {
                foreach ($category_check->results() as $ups) {
                  $products_check = DB::getInstance()->query("SELECT * FROM `cart` WHERE `user_id` = '$user_id' AND `product_id` = '$ups->id'");
                  if ($products_check->count()) {
                    $uniq_code = $products_check->first()->uniq;
                    try {
                     $link->update_sales([
                       'coupoun' => $coupon_code
                     ], $table, $uniq_code);
                     $sales_check = DB::getInstance()->query("SELECT * FROM `sales` WHERE `uniq` = '$uniq_code'");
                     if ($sales_check->count()) {
                       try {
                         $link->update_sales([
                           'coupoun' => $coupon_code
                         ], $table1, $uniq_code);
                       } catch (Exception $e) {

                       }
                     }
                   } catch (Exception $e) {

                   }
                  }
                }
              }
            } else {
              $products_check = DB::getInstance()->query("SELECT * FROM `cart` WHERE `user_id` = '$user_id' AND `product_id` = '$product_id'");
              if ($products_check->count()) {
                $uniq_code = $products_check->first()->uniq;
                try {
                 $link->update_sales([
                   'coupoun' => $coupon_code
                 ], $table, $uniq_code);
                 $sales_check = DB::getInstance()->query("SELECT * FROM `sales` WHERE `uniq` = '$uniq_code'");
                 if ($sales_check->count()) {
                  //  var_dump($sales_check);
                   try {
                     $link->update_sales([
                       'coupoun' => $coupon_code
                     ], $table1, $uniq_code);
                    //  die($sales_check);
                   } catch (Exception $e) {
                    //  var_dump($e);
                   }
                 }
               } catch (Exception $e) {

               }
              }
            }

          } else {
            $coupoun_error = "Coupoun Code does not Exist";
          }
        } else {
          $cart_value = "Cart Value Should be At Least ".$total_amount;
        }

      }
      if (isset($_POST['update_cart'])) {
        $uniq = $_POST['uniq'];
        $cur_qty = $_POST['cur_qty'];
        $qty = $_POST['quantity'];
        $qtyy = $cur_qty + $qty;
        try {
          $link->update_sales([
            'qty' => $qtyy
          ], $table, $uniq);
          try {
            $link->update_sales([
              'qty' => $qtyy
            ], $table1, $uniq);
          } catch (Exception $e) {

          }

        } catch (Exception $e) {

        }


      }
?>
<!doctype html>
<html class="no-js" lang="">

<!-- Mirrored from devitems.com/html/oneclick-preview/oneclick/cart.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 17 Jan 2017 10:59:16 GMT -->
<head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Cart</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- Favicon -->
		<link rel="shortcut icon" type="image/x-icon" href="<?php echo $uri; ?>/img/favicon.ico">

		<!-- All css files are included here. -->
		<!-- animate css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/animate.min.css">
		<!-- Bootstrap fremwork main css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/bootstrap.min.css">
		<!-- font-awesome css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/font-awesome.min.css">
		<!-- nivo-slider css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/nivo-slider.css">
		<!-- owl carousel css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/owl.carousel.min.css">
		<!-- icofont css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/icofont.css">
		<!-- meanmenu css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/meanmenu.css">
		<!-- jquery-ui css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/jquery-ui.min.css">
		<!-- magnific-popup css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/magnific-popup.css">
		<!-- percircle css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/percircle.css">
		<!-- Theme main style -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/style.css">
		<!-- Responsive css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/responsive.css">

		<!-- Modernizr JS -->
        <script src="<?php echo $uri; ?>/js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
		<!-- header start -->
		<header>

      <?php include_once 'header.php'; ?>

      <?php include_once 'menu.php'; ?>

		</header>
		<!-- header end -->
		<!-- cart-area start -->
		<div class="cart-main-container shop-bg">
			<div class="cart-area">
				<div class="container">
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="woocommerce-breadcrumb mtb-15">
								<div class="menu">
									<ul>
										<li><a href="<?php echo $uri; ?>/">Home</a></li>
										<li class="active"><a href="<?php echo $uri; ?>/cart">cart</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="account-title mtb-20 text-center">
                <?php if ($coupoun_error): ?>
                  <h1><?php echo $coupoun_error; ?></h1>
                <?php endif; ?>
                <?php if ($cart_value): ?>
                  <h1><?php echo $cart_value; ?></h1>
                <?php endif; ?>
								<h1>Cart</h1>
							</div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="cart-table mb-50 bg-fff">

									<div class="table-content table-responsive">
										<table>
											<thead>
												<tr>
													<th class="product-thumbnail">Image</th>
													<th class="product-name">Product</th>
													<th class="product-price">Price</th>
													<th class="product-quantity">Quantity</th>
													<th class="product-subtotal">Total</th>
                          <th class="product-remove">Action</th>
												</tr>
											</thead>
											<tbody>
                        <?php
                          $user_id = $link->data()->id;
                          $products_list = DB::getInstance()->query("SELECT * FROM `cart` WHERE `user_id` = '$user_id'");
                          $total = 0;
                        ?>
                        <?php foreach ($products_list->results() as $product): ?>
                          <?php $product_details = DB::getInstance()->query("SELECT * FROM `product` WHERE `id` = '$product->product_id' AND `status` = 'on'"); ?>
                          <tr class="cart-item">
  													<td class="product-thumbnail">
  														<a href="<?php echo $uri; ?>/#">
  															<img src="<?php echo $uri; ?>/<?php echo $product_details->first()->image; ?>" alt="" />
  														</a>
  													</td>
  													<td class="product-name">
  														<a href="<?php echo $uri; ?>/#"><?php echo $product_details->first()->name; ?> </a>
  													</td>
  													<td class="product-price">
  														<span class="amounte"><?php echo $product->price; ?></span>
  													</td>
  													<td class="product-quantity">
  														<!-- <input value="1" type="number"> -->
                              <form action="" method="post">
          											<input name="uniq" value="<?php echo $product->uniq; ?>" type="hidden" required="">
          											<input name="cur_qty" value="<?php echo $product->qty; ?>" type="hidden" required="">
          											<input style="padding-left: 7px;" min="1" max="1000" name="quantity" value="<?php echo $product->qty; ?>" type="number" required="">
          											<button class="button cursor-not" type="submit" name="update_cart">Update Cart</button>
          										</form>
                              <!-- <span class="amounte"><?php echo $product->qty; ?></span> -->
  													</td>
  													<td class="product-subtotal">
  														<span class="sub-total"><?php echo $product->qty * $product->price; ?></span>
  													</td>
                            <td class="product-remove">
                              <form class="" action="" method="post">
                                <input type="hidden" name="uniq" value="<?php echo $product->uniq; ?>">
                                <button class="cart-remove" type="submit" name="remove"><i class="fa fa-times"></i></button>
                                <!-- <div > </div> -->
                              </form>
                            </td>
  												</tr>
                          <?php $total += $product->qty * $product->price; ?>
                        <?php endforeach; ?>



												<tr>
													<td colspan="6" class="actions clear">
                            <form class="" action="" method="post">
                              <div class="coupon mb-10 floatleft">
                                <input type="hidden" name="coupon_total" value="<?php echo $total; ?>">
  															<input name="coupon_code" class="input-text" id="coupon_code" value="" placeholder="Coupon code" type="text">
  															<input class="button" name="apply_coupon" value="Apply Coupon" type="submit">
  														</div>
  														<div class="floatright mb-10">
  															<!-- <input class="button cursor-not" name="update_cart_coupon" value="Update Cart" type="submit"> -->
  														</div>
                            </form>
													</td>
												</tr>
											</tbody>
										</table>
									</div>

							</div>
						</div>
					</div>
					<div class="row">
            <div class="col-md-6">

            </div>
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 right">
							<div class="cart_totals">
								<div class="cart-total-taitle mb-30 text-uppercase">
									<h3>Cart Total</h3>
								</div>
							</div>
							<div class="table-content table-responsive mb-30">
								<table>
									<tr>
										<td><strong>Subtotal</strong></td>
										<td><b class="text-warning"><?php echo $total; ?></b></td>
									</tr>
									<tr>
										<td><strong>Total</strong></td>
										<td><b class="text-warning"><?php echo $total; ?></b></td>
									</tr>
								</table>
							</div>
							<div class="simple-product-form contuct-form mb-30">

									<a href="<?php echo $uri; ?>/checkout"><button>Proceed to Checkout</button></a>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- footer-area start -->
		<?php include_once 'footer.php'; ?>
		<!-- footer-area end -->
    </body>

<!-- Mirrored from devitems.com/html/oneclick-preview/oneclick/cart.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 17 Jan 2017 10:59:16 GMT -->
</html>
