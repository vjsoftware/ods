<header>
    <div class="header-top-area bb hidden-xs">
      <div class="container">
        <div class="row">
          <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
            <!-- <div class="language-menu dropdown">
              <ul>
                <li><a href="<?php echo $uri; ?>/#">eng <i class="fa fa-angle-down"></i></a>
                  <ul>
                    <li><a href="<?php echo $uri; ?>/#">France</a></li>
                    <li><a href="<?php echo $uri; ?>/#">Germany</a></li>
                    <li><a href="<?php echo $uri; ?>/#">Japanese</a></li>
                  </ul>
                </li>
                <li><a href="<?php echo $uri; ?>/#">usd <i class="fa fa-angle-down"></i></a>
                  <ul>
                    <li><a href="<?php echo $uri; ?>/#">EUR - Euro</a></li>
                    <li><a href="<?php echo $uri; ?>/#">GBP - British Pound</a></li>
                    <li><a href="<?php echo $uri; ?>/#">INR - Indian Rupee</a></li>
                  </ul>
                </li>
              </ul>
            </div> -->
          </div>
          <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
            <div class="header-top-right">
              <p class="pull-left h2-color mtb-10 hidden-sm hidden-xs">Welcome to OneDayShop! </p>
              <div class="account-menu text-right pull-right mt-10">
                <ul>

                  <!-- <li><a href="<?php echo $uri; ?>/wishlist.html">Wishlist</a></li> -->
                  <?php if (!$link->isLoggedin()): ?>
                    <li><a href="<?php echo $uri; ?>/account">Login/Register</a></li>
                  <?php else: ?>
                    <li><a href="<?php echo $uri; ?>/orders">My Orders</a></li>
                    <li><a href="<?php echo $uri; ?>/cart">Shopping Cart</a></li>
                    <li><a href="<?php echo $uri; ?>/checkout">Checkout</a></li>
                    <li><a href="<?php echo $uri; ?>/profile">Profile</a></li>
                    <li><a href="<?php echo $uri; ?>/signout">signout</a></li>
                  <?php endif; ?>

                </ul>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="header-middle-area ptb-30">
      <div class="container">
        <div class="row">
          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <div class="logo">
              <a href="<?php echo $uri; ?>/index">
                <img src="<?php echo $uri; ?>/img/logo.png" alt="" style="width:200px;height:80px;"/>
              </a>
            </div>
          </div>
          <div class="col-lg-7 col-md-6 col-sm-8 col-xs-12">
            <div class="search-box">
              <form action="#">
                <select name="#" id="select">
                  <option value="">All categories</option>
                  <?php $cat = DB::getInstance()->query("SELECT * FROM `category`"); ?>
                  <?php foreach ($cat->results() as $val): ?>
                    <option value="1"><?php echo $val->name; ?></option>
                  <?php endforeach; ?>

                </select>
                <input type="text" placeholder="Search Products..."/>
                <button><i class="fa fa-search"></i></button>
              </form>
              <p class="hidden-sm hidden-md hidden-xs">top search bundle product, Enter keywords to search, morbi, Tablets, computer,funiture...</p>
            </div>
          </div>
          <div class="col-lg-2  col-md-3 col-sm-4  col-xs-12">
            <?php
              $user_id = $link->data()->id;
              $cart_list = DB::getInstance()->query("SELECT * FROM `cart` WHERE `user_id` = '$user_id'");
              $grand_total = 0;
            ?>
            <?php if ($cart_list->count()): ?>
              <div class="top-cart bg-5">
                <div class="cart">
                  <i class="icofont icofont-bag"></i>
                  <a href="<?php echo $uri; ?>/#"> <?php echo $cart_list->count(); ?> Items
                    -
                    <?php foreach ($cart_list->results() as $tot) {
                      $grand_total +=  $tot->qty * $tot->price;
                    } ?>
                     <strong><?php echo $grand_total; ?> </strong>
                    <i class="icofont icofont-rounded-down"></i>
                  </a>
                </div>
                <ul>
                  <li>
                    <div class="cart-items">
                      <?php $tot = 0; ?>
                      <?php foreach ($cart_list->results() as $list): ?>
                        <?php
                          $product_details = DB::getInstance()->query("SELECT * FROM `product` WHERE `id` = '$list->product_id' AND `status` = 'on'");
                        ?>
                        <div class="cart-item bb mt-10">
                          <div class="cart-img">
                            <a href="<?php echo $uri; ?>/#">
                              <img src="<?php echo $uri; ?>/<?php echo $product_details->first()->image; ?>" alt="" />
                            </a>
                          </div>
                          <div class="cart-content">
                            <a href="<?php echo $uri; ?>/#"><?php echo $product_details->first()->name; ?></a>
                            <form class="" action="" method="post">
                              <input type="hidden" name="uniq" value="<?php echo $list->uniq; ?>">
                              <button class="pull-right cart-remove" type="submit" name="remove"><i class="fa fa-times"></i></button>
                              <!-- <div > </div> -->
                            </form>
                            <span><?php echo $list->qty; ?> x <?php echo $list->price; ?></span>
                            <?php $tot += $list->qty * $list->price; ?>
                          </div>
                        </div>
                      <?php endforeach; ?>



                      <div class="total mt-10">
                        <span class="pull-left">Subtotal:</span>
                        <span class="pull-right"><?php echo $tot; ?></span>
                      </div>
                      <div class="cart-btn mb-20">
                        <a href="<?php echo $uri; ?>/cart">view cart</a>
                        <a href="<?php echo $uri; ?>/checkout">Checkout</a>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
            <?php endif; ?>

          </div>
        </div>
      </div>
    </div>
</header>
