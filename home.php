<?php
  require_once 'core/init.php';
  $link = new Link();
  $page_id = '1';
?>
<!doctype html>
<html class="no-js" lang="">


<head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Online Shopping India Shop Online for Electronics, Mobiles, Fashion & Home at OneDayShop</title>
        <meta name="description" content="">
        <!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->

		<!-- Favicon -->
		<link rel="shortcut icon" type="image/x-icon"  href="<?php echo $uri; ?>/img/favicon.ico">

		<!-- All css files are included here. -->
		<!-- animate css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/animate.min.css">
		<!-- Bootstrap fremwork main css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/bootstrap.min.css">
		<!-- font-awesome css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/font-awesome.min.css">
		<!-- nivo-slider css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/nivo-slider.css">
		<!-- owl carousel css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/owl.carousel.min.css">
		<!-- icofont css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/icofont.css">
		<!-- meanmenu css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/meanmenu.css">
		<!-- jquery-ui css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/jquery-ui.min.css">
		<!-- magnific-popup css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/magnific-popup.css">
		<!-- percircle css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/percircle.css">
		<!-- Theme main style -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/style.css">
		<!-- Responsive css -->
        <!-- <link rel="stylesheet" href="<?php echo $uri; ?>/css/responsive.css"> -->

		<!-- Modernizr JS -->
        <script src="<?php echo $uri; ?>/js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
		<!-- header start -->
    <header>

      <?php include_once 'header.php'; ?>

      <?php include_once 'menu.php'; ?>

    </header>
		<!-- header end -->
		<!-- slider area start -->
		<?php include_once 'slide.php'; ?>
		<!-- slider area end -->
		<!-- product area start -->
		<div class="product-area ptb-35">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-xs-12 col-sm-12 col-md-12">
						<div class="product box-shadow bg-fff">
							<div class="product-title bg-1 text-uppercase">
								<i class="fa fa-star-o icon bg-4"></i>
								<h3>best selling</h3>
							</div>
							<div class="product-active left-right-angle">
                <?php
                  $product_details = DB::getInstance()->query("SELECT * FROM `product` WHERE `status` = 'on' ORDER BY `id` DESC LIMIT 0, 6");
                ?>
                <?php foreach ($product_details->results() as $product): ?>
                  <div class="product-wrapper bb bl">
  									<div class="product-img" style="min-height: 240px; max-height: 240px;">
  										<a href="<?php echo $uri; ?>/product/<?php echo $product->slug . '/' . $product->id; ?>">
                        <?php if (file_exists('resized/'.$product->image)): ?>
                          <img src="<?php echo $uri; ?>/resized/<?php echo $product->image; ?>" alt="<?php echo $product->name; ?>" class="" style="margin-top: 15px;"/>
                        <?php else: ?>
                          <img src="<?php echo $uri; ?>/images/<?php echo $product->image; ?>" alt="<?php echo $product->name; ?>" class="" style="margin-top: 15px;"/>
                        <?php endif; ?>
  											<!-- <img src="<?php echo $uri; ?>/img/product/2.jpg" alt="" class="secondary"/> -->
  										</a>
  										<div class="product-icon c-fff hover-bg">
  											<!-- <ul>
  												<li><a href="<?php echo $uri; ?>/#" data-toggle="tooltip" title="Add to cart"><i class="fa fa-shopping-cart"></i></a></li>
  												<li><a href="<?php echo $uri; ?>/#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a></li>
  												<li><a href="<?php echo $uri; ?>/#" data-toggle="tooltip" title="Compare"><i class="fa fa-comments"></i></a></li>
  												<li><a href="<?php echo $uri; ?>/#" data-toggle="tooltip" title="Accumsan eli"><i class="fa fa-search"></i></a></li>
  											</ul> -->
  										</div>
  										<!-- <span class="sale">Sale</span> -->
  									</div>
  									<div class="product-content">
  										<h3><a href="<?php echo $uri; ?>/product/<?php echo $product->slug . '/' . $product->id; ?>"><?php echo $product->name; ?></a></h3>
  										<ul>
  											<li><i class="fa fa-star"></i></li>
  											<li><i class="fa fa-star"></i></li>
  											<li><i class="fa fa-star"></i></li>
  											<li><i class="fa fa-star"></i></li>
  											<li><i class="fa fa-star"></i></li>
  										</ul>
  										<span style="font-family: monospace; font-size: 18px;">Rs <?php echo $product->price; ?></span>
  									</div>
  								</div>
                <?php endforeach; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- product area end -->
		<!-- banner-area start -->
		<div class="banner-area">
			<div class="container">
				<div class="row">
					<div class="col-lg-4  col-md-4 col-sm-6 col-xs-12">
						<div class="single-banner">
							<a href="<?php echo $uri; ?>/products/Deals">
								<img src="<?php echo $uri; ?>/img/banner/1.jpg" alt="">
							</a>
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
						<div class="single-banner">
							<a href="<?php echo $uri; ?>/#">
								<img src="<?php echo $uri; ?>/img/banner/2.jpg" alt="">
							</a>
						</div>
					</div>
					<div class="col-lg-4 col-md-4 hidden-sm col-xs-12">
						<div class="single-banner">
							<a href="<?php echo $uri; ?>/#">
								<img src="<?php echo $uri; ?>/img/banner/3.jpg" alt="">
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- banner-area end -->
		<!-- all-product-area start -->
		<div class="all-product-area mtb-45">
			<div class="container">
				<div class="row">
					<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
						<div class="latest-deals box-shadow mb-35 bg-fff">
							<div class="row">
								<div class="col-sm-12">
									<div class="product-title bg-2 text-uppercase">
										<i class="fa fa-history icon bg-3"></i>
										<h3>latest deals</h3>
									</div>
								</div>
							</div>
							<div class="latest-deals-active">
								<div class="product-wrapper">
									<div class="product-img" style="max-height: 300px;">
										<a href="<?php echo $uri; ?>/#">
											<img src="<?php echo $uri; ?>/img/product/1.jpg" alt="" class="primary"/>
											<img src="<?php echo $uri; ?>/img/product/2.jpg" alt="" class="secondary"/>
										</a>
										<div class="product-icon hover-bg">
											<!-- <ul>
												<li><a href="<?php echo $uri; ?>/#" data-toggle="tooltip" title="Add to cart"><i class="fa fa-shopping-cart"></i></a></li>
												<li><a href="<?php echo $uri; ?>/#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a></li>
												<li><a href="<?php echo $uri; ?>/#" data-toggle="tooltip" title="Compare"><i class="fa fa-comments"></i></a></li>
												<li><a href="<?php echo $uri; ?>/#" data-toggle="tooltip" title="Accumsan eli"><i class="fa fa-search"></i></a></li>
											</ul> -->
										</div>
										<span class="sale">Sale</span>
										<div class="deal-count">
											<div class="timer">
												<div data-countdown="2017/06/01"></div>
											</div>
										</div>
									</div>
									<div class="product-content">
										<h3><a href="<?php echo $uri; ?>/#">Adipiscing cursus eu</a></h3>
										<ul>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
										</ul>
										<span style="font-family: monospace; font-size: 18px;">Rs 20000</span>
									</div>
								</div>


							</div>
						</div>
						<!-- featured-area start -->
						<div class="featured-area box-shadow bg-fff">
							<div class="product-title bg-1 text-uppercase">
								<i class="fa fa-star-o icon bg-4"></i>
								<h3>featured</h3>
							</div>
							<div class="feautred-active right left-right-angle">
								<div class="featured-wrapper">
                  <?php $product_details = DB::getInstance()->query("SELECT * FROM `product` WHERE `status` = 'on' LIMIT 0, 4"); ?>
                  <?php foreach ($product_details->results() as $product): ?>
                    <div class="product-wrapper single-featured mtb-15">
  										<div class="product-img floatleft">
  											<a href="<?php echo $uri; ?>/product/<?php echo $product->slug . '/' . $product->id; ?>">
                          <?php if (file_exists('thumb/'.$product->image)): ?>
                            <img src="<?php echo $uri; ?>/thumb/<?php echo $product->image; ?>" alt="<?php echo $product->name; ?>" />
                          <?php else: ?>
                            <img src="<?php echo $uri; ?>/images/<?php echo $product->image; ?>" alt="<?php echo $product->name; ?>" />
                          <?php endif; ?>
  												<!-- <img src="<?php echo $uri; ?>/img/product/2.jpg" alt="" class="secondary"/> -->
  											</a>
  										</div>
  										<div class="product-content floatright">
  											<h3><a href="<?php echo $uri; ?>/product/<?php echo $product->slug . '/' . $product->id; ?>"><?php echo $product->name; ?></a></h3>
  											<ul>
  												<li><i class="fa fa-star"></i></li>
  												<li><i class="fa fa-star"></i></li>
  												<li><i class="fa fa-star"></i></li>
  												<li><i class="fa fa-star"></i></li>
  												<li><i class="fa fa-star"></i></li>
  											</ul>
  											<span style="font-family: monospace; font-size: 18px;">Rs <?php echo $product->price; ?></span>
  										</div>
  									</div>
                  <?php endforeach; ?>
								</div>



							</div>
						</div>
						<!-- featured-area end -->
						<!-- testimonils-area start -->

						<!-- testimonils-area end -->
						<!-- blog-area start -->

						<!-- blog-area end -->
						<!-- newsletter-area start -->

						<!-- newsletter-area start -->
					</div>
					<!-- product-area start -->
					<div class="col-lg-9  col-md-9 col-sm-12 col-xs-12">
						<div class="product box-shadow bg-fff">
							<div class="product-title bg-1 text-uppercase">
								<i class="fa fa-paper-plane-o icon bg-4"></i>
								<h3>New Products</h3>
							</div>
							<div class="new-product-active left left-right-angle">

                  <div class="product-items">
                    <?php $products_list = DB::getInstance()->query("SELECT * FROM `product` WHERE `status` = 'on' ORDER BY `id` DESC LIMIT 0, 2"); ?>
                    <?php foreach ($products_list->results() as $products): ?>

  									<div class="product-wrapper bb bl">
  										<div class="product-img" style="max-height: 300px;">
  											<a href="<?php echo $uri; ?>/product/<?php echo $products->slug . '/' . $products->id; ?>">
                          <?php if (file_exists('resized/'.$products->image)): ?>
                            <img src="<?php echo $uri; ?>/resized/<?php echo $products->image; ?>" alt="<?php echo $products->name; ?>" class="" style="margin-top: 15px;"/>
                          <?php else: ?>
                            <img src="<?php echo $uri; ?>/images/<?php echo $products->image; ?>" alt="<?php echo $products->name; ?>" class="" style="margin-top: 15px;"/>
                          <?php endif; ?>
  											</a>
  											<div class="product-icon c-fff hover-bg">
  											</div>
  										</div>
  										<div class="product-content">
  											<h3 style="height: 30px;"><a href="<?php echo $uri; ?>/product/<?php echo $product->slug . '/' . $product->id; ?>"><?php echo substr($products->name, 0, 45); ?></a></h3>
  											<ul>
  												<li><i class="fa fa-star"></i></li>
  												<li><i class="fa fa-star"></i></li>
  												<li><i class="fa fa-star"></i></li>
  												<li><i class="fa fa-star"></i></li>
  												<li><i class="fa fa-star"></i></li>
  											</ul>
  											<span style="font-family: monospace; font-size: 18px;">Rs <?php echo $products->price; ?></span>
  										</div>
  									</div>
                  <?php endforeach; ?>
  								</div>
                  <div class="product-items">
                    <?php $products_list = DB::getInstance()->query("SELECT * FROM `product` WHERE `status` = 'on' ORDER BY `id` DESC LIMIT 2, 2"); ?>

                    <?php foreach ($products_list->results() as $products): ?>

  									<div class="product-wrapper bb bl">
  										<div class="product-img" style="max-height: 300px;">
  											<a href="<?php echo $uri; ?>/product/<?php echo $products->slug . '/' . $products->id; ?>">
                          <?php if (file_exists('resized/'.$products->image)): ?>
                            <img src="<?php echo $uri; ?>/resized/<?php echo $products->image; ?>" alt="<?php echo $products->name; ?>" class="" style="margin-top: 15px;"/>
                          <?php else: ?>
                            <img src="<?php echo $uri; ?>/images/<?php echo $products->image; ?>" alt="<?php echo $products->name; ?>" class="" style="margin-top: 15px;"/>
                          <?php endif; ?>

  											</a>
  											<div class="product-icon c-fff hover-bg">
  												<!-- <ul>
  													<li><a href="<?php echo $uri; ?>/#" data-toggle="tooltip" title="Add to cart"><i class="fa fa-shopping-cart"></i></a></li>
  													<li><a href="<?php echo $uri; ?>/#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a></li>
  													<li><a href="<?php echo $uri; ?>/#" data-toggle="tooltip" title="Compare"><i class="fa fa-comments"></i></a></li>
  													<li><a href="<?php echo $uri; ?>/#" data-toggle="tooltip" title="Accumsan eli"><i class="fa fa-search"></i></a></li>
  												</ul> -->
  											</div>
  										</div>
  										<div class="product-content">
  											<h3 style="height: 30px;"><a href="<?php echo $uri; ?>/#"><?php echo substr($products->name, 0, 45); ?></a></h3>
  											<ul>
  												<li><i class="fa fa-star"></i></li>
  												<li><i class="fa fa-star"></i></li>
  												<li><i class="fa fa-star"></i></li>
  												<li><i class="fa fa-star"></i></li>
  												<li><i class="fa fa-star"></i></li>
  											</ul>
  											<span style="font-family: monospace; font-size: 18px;">Rs <?php echo $products->price; ?></span>
  										</div>
  									</div>
                  <?php endforeach; ?>
  								</div>
                  <div class="product-items">
                    <?php $products_list = DB::getInstance()->query("SELECT * FROM `product` WHERE `status` = 'on' ORDER BY `id` DESC LIMIT 4, 2"); ?>

                    <?php foreach ($products_list->results() as $products): ?>

  									<div class="product-wrapper bb bl">
  										<div class="product-img" style="max-height: 300px;">
  											<a href="<?php echo $uri; ?>/product/<?php echo $products->slug . '/' . $products->id; ?>">
                          <?php if (file_exists('resized/'.$products->image)): ?>
                            <img src="<?php echo $uri; ?>/resized/<?php echo $products->image; ?>" alt="<?php echo $products->name; ?>" class="" style="margin-top: 15px;"/>
                          <?php else: ?>
                            <img src="<?php echo $uri; ?>/images/<?php echo $products->image; ?>" alt="<?php echo $products->name; ?>" class="" style="margin-top: 15px;"/>
                          <?php endif; ?>
  											</a>
  											<div class="product-icon c-fff hover-bg">
  												<!-- <ul>
  													<li><a href="<?php echo $uri; ?>/#" data-toggle="tooltip" title="Add to cart"><i class="fa fa-shopping-cart"></i></a></li>
  													<li><a href="<?php echo $uri; ?>/#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a></li>
  													<li><a href="<?php echo $uri; ?>/#" data-toggle="tooltip" title="Compare"><i class="fa fa-comments"></i></a></li>
  													<li><a href="<?php echo $uri; ?>/#" data-toggle="tooltip" title="Accumsan eli"><i class="fa fa-search"></i></a></li>
  												</ul> -->
  											</div>
  										</div>
  										<div class="product-content">
  											<h3 style="height: 30px;"><a href="<?php echo $uri; ?>/#"><?php echo substr($products->name, 0, 45); ?></a></h3>
  											<ul>
  												<li><i class="fa fa-star"></i></li>
  												<li><i class="fa fa-star"></i></li>
  												<li><i class="fa fa-star"></i></li>
  												<li><i class="fa fa-star"></i></li>
  												<li><i class="fa fa-star"></i></li>
  											</ul>
  											<span style="font-family: monospace; font-size: 18px;">Rs <?php echo $products->price; ?></span>
  										</div>
  									</div>
                  <?php endforeach; ?>
  								</div>
                  <div class="product-items">
                    <?php $products_list = DB::getInstance()->query("SELECT * FROM `product` WHERE `status` = 'on' ORDER BY `id` DESC LIMIT 6, 2"); ?>

                    <?php foreach ($products_list->results() as $products): ?>

  									<div class="product-wrapper bb bl">
  										<div class="product-img" style="max-height: 300px;">
  											<a href="<?php echo $uri; ?>/product/<?php echo $products->slug . '/' . $products->id; ?>">
                          <?php if (file_exists('resized/'.$products->image)): ?>
                            <img src="<?php echo $uri; ?>/resized/<?php echo $products->image; ?>" alt="<?php echo $products->name; ?>" class="" style="margin-top: 15px;"/>
                          <?php else: ?>
                            <img src="<?php echo $uri; ?>/images/<?php echo $products->image; ?>" alt="<?php echo $products->name; ?>" class="" style="margin-top: 15px;"/>
                          <?php endif; ?>
  											</a>
  											<div class="product-icon c-fff hover-bg">
  												<!-- <ul>
  													<li><a href="<?php echo $uri; ?>/#" data-toggle="tooltip" title="Add to cart"><i class="fa fa-shopping-cart"></i></a></li>
  													<li><a href="<?php echo $uri; ?>/#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a></li>
  													<li><a href="<?php echo $uri; ?>/#" data-toggle="tooltip" title="Compare"><i class="fa fa-comments"></i></a></li>
  													<li><a href="<?php echo $uri; ?>/#" data-toggle="tooltip" title="Accumsan eli"><i class="fa fa-search"></i></a></li>
  												</ul> -->
  											</div>
  										</div>
  										<div class="product-content">
  											<h3 style="height: 30px;"><a href="<?php echo $uri; ?>/#"><?php echo substr($products->name, 0, 45); ?></a></h3>
  											<ul>
  												<li><i class="fa fa-star"></i></li>
  												<li><i class="fa fa-star"></i></li>
  												<li><i class="fa fa-star"></i></li>
  												<li><i class="fa fa-star"></i></li>
  												<li><i class="fa fa-star"></i></li>
  											</ul>
  											<span style="font-family: monospace; font-size: 18px;">Rs <?php echo $products->price; ?></span>
  										</div>
  									</div>
                  <?php endforeach; ?>
  								</div>
                  <div class="product-items">
                    <?php $products_list = DB::getInstance()->query("SELECT * FROM `product` WHERE `status` = 'on' ORDER BY `id` DESC LIMIT 8, 2"); ?>

                    <?php foreach ($products_list->results() as $products): ?>

  									<div class="product-wrapper bb bl">
  										<div class="product-img" style="max-height: 300px;">
  											<a href="<?php echo $uri; ?>/product/<?php echo $products->slug . '/' . $products->id; ?>">
                          <?php if (file_exists('resized/'.$products->image)): ?>
                            <img src="<?php echo $uri; ?>/resized/<?php echo $products->image; ?>" alt="<?php echo $products->name; ?>" class="" style="margin-top: 15px;"/>
                          <?php else: ?>
                            <img src="<?php echo $uri; ?>/images/<?php echo $products->image; ?>" alt="<?php echo $products->name; ?>" class="" style="margin-top: 15px;"/>
                          <?php endif; ?>
  											</a>
  											<div class="product-icon c-fff hover-bg">
  												<!-- <ul>
  													<li><a href="<?php echo $uri; ?>/#" data-toggle="tooltip" title="Add to cart"><i class="fa fa-shopping-cart"></i></a></li>
  													<li><a href="<?php echo $uri; ?>/#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a></li>
  													<li><a href="<?php echo $uri; ?>/#" data-toggle="tooltip" title="Compare"><i class="fa fa-comments"></i></a></li>
  													<li><a href="<?php echo $uri; ?>/#" data-toggle="tooltip" title="Accumsan eli"><i class="fa fa-search"></i></a></li>
  												</ul> -->
  											</div>
  										</div>
  										<div class="product-content">
  											<h3 style="height: 30px;"><a href="<?php echo $uri; ?>/#"><?php echo substr($products->name, 0, 45); ?></a></h3>
  											<ul>
  												<li><i class="fa fa-star"></i></li>
  												<li><i class="fa fa-star"></i></li>
  												<li><i class="fa fa-star"></i></li>
  												<li><i class="fa fa-star"></i></li>
  												<li><i class="fa fa-star"></i></li>
  											</ul>
  											<span style="font-family: monospace; font-size: 18px;">Rs <?php echo $products->price; ?></span>
  										</div>
  									</div>
                  <?php endforeach; ?>
  								</div>
                  <div class="product-items">
                    <?php $products_list = DB::getInstance()->query("SELECT * FROM `product` WHERE `status` = 'on' ORDER BY `id` DESC LIMIT 10, 2"); ?>

                    <?php foreach ($products_list->results() as $products): ?>

  									<div class="product-wrapper bb bl">
  										<div class="product-img" style="max-height: 300px;">
  											<a href="<?php echo $uri; ?>/product/<?php echo $products->slug . '/' . $products->id; ?>">
                          <?php if (file_exists('resized/'.$products->image)): ?>
                            <img src="<?php echo $uri; ?>/resized/<?php echo $products->image; ?>" alt="<?php echo $products->name; ?>" class="" style="margin-top: 15px;"/>
                          <?php else: ?>
                            <img src="<?php echo $uri; ?>/images/<?php echo $products->image; ?>" alt="<?php echo $products->name; ?>" class="" style="margin-top: 15px;"/>
                          <?php endif; ?>
  											</a>
  											<div class="product-icon c-fff hover-bg">
  												<!-- <ul>
  													<li><a href="<?php echo $uri; ?>/#" data-toggle="tooltip" title="Add to cart"><i class="fa fa-shopping-cart"></i></a></li>
  													<li><a href="<?php echo $uri; ?>/#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a></li>
  													<li><a href="<?php echo $uri; ?>/#" data-toggle="tooltip" title="Compare"><i class="fa fa-comments"></i></a></li>
  													<li><a href="<?php echo $uri; ?>/#" data-toggle="tooltip" title="Accumsan eli"><i class="fa fa-search"></i></a></li>
  												</ul> -->
  											</div>
  										</div>
  										<div class="product-content">
  											<h3 style="height: 30px;"><a href="<?php echo $uri; ?>/#"><?php echo substr($products->name, 0, 45); ?></a></h3>
  											<ul>
  												<li><i class="fa fa-star"></i></li>
  												<li><i class="fa fa-star"></i></li>
  												<li><i class="fa fa-star"></i></li>
  												<li><i class="fa fa-star"></i></li>
  												<li><i class="fa fa-star"></i></li>
  											</ul>
  											<span style="font-family: monospace; font-size: 18px;">Rs <?php echo $products->price; ?></span>
  										</div>
  									</div>
                  <?php endforeach; ?>
  								</div>

<?php // var_dump($products_list); ?>




							</div>
						</div>
						<!-- banner-area start -->
						<div class="banner-area mtb-35">
							<div class="row">
								<div class="col-lg-6  col-md-6 col-sm-6">
									<div class="single-banner">
										<a href="<?php echo $uri; ?>/#">
											<img src="<?php echo $uri; ?>/img/banner/4.jpg" alt="">
										</a>
									</div>
								</div>
								<div class="col-lg-6  col-md-6 col-sm-6">
									<div class="single-banner">
										<a href="<?php echo $uri; ?>/#">
											<img src="<?php echo $uri; ?>/img/banner/5.jpg" alt="">
										</a>
									</div>
								</div>
							</div>
						</div>
						<!-- banner-area end -->
						<!-- tab-area start -->

						<!-- mostviewed-area start -->

						<!-- mostviewed-area end -->
					</div>
					<!-- product-area end -->
				</div>
			</div>
		</div>
		<!-- all-product-area end -->
		<!-- brand-area start -->
		<!-- <div class="brand-area mb-35">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="brand-active  box-shadow p-15  bg-fff">
							<div class="single-brand">
								<a href="<?php echo $uri; ?>/#">
									<img src="<?php echo $uri; ?>/img/brand/1.jpg" alt="" />
								</a>
							</div>
							<div class="single-brand">
								<a href="<?php echo $uri; ?>/#">
									<img src="<?php echo $uri; ?>/img/brand/2.jpg" alt="" />
								</a>
							</div>
							<div class="single-brand">
								<a href="<?php echo $uri; ?>/#">
									<img src="<?php echo $uri; ?>/img/brand/3.jpg" alt="" />
								</a>
							</div>
							<div class="single-brand">
								<a href="<?php echo $uri; ?>/#">
									<img src="<?php echo $uri; ?>/img/brand/1.jpg" alt="" />
								</a>
							</div>
							<div class="single-brand">
								<a href="<?php echo $uri; ?>/#">
									<img src="<?php echo $uri; ?>/img/brand/4.jpg" alt="" />
								</a>
							</div>
							<div class="single-brand">
								<a href="<?php echo $uri; ?>/#">
									<img src="<?php echo $uri; ?>/img/brand/5.jpg" alt="" />
								</a>
							</div>
							<div class="single-brand">
								<a href="<?php echo $uri; ?>/#">
									<img src="<?php echo $uri; ?>/img/brand/6.jpg" alt="" />
								</a>
							</div>
							<div class="single-brand">
								<a href="<?php echo $uri; ?>/#">
									<img src="<?php echo $uri; ?>/img/brand/7.jpg" alt="" />
								</a>
							</div>
							<div class="single-brand">
								<a href="<?php echo $uri; ?>/#">
									<img src="<?php echo $uri; ?>/img/brand/8.jpg" alt="" />
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div> -->
		<!-- brand-area end -->
		<!-- order-area start -->
		<div class="order-area ptb-30 bt bg-fff">
			<div class="container">
				<div class="row">

					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
						<div class="single-order c-fff bg-1 p-20">
							<div class="order-icon">
								<span class="fa fa-refresh"></span>
							</div>
							<div class="order-content">
								<h5>7 Days Return</h5>
								<span>Moneyback guarantee</span>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
						<div class="single-order c-fff bg-1 p-20">
							<div class="order-icon">
								<span class="fa fa-umbrella"></span>
							</div>
							<div class="order-content">
								<h5>SUPPORT 24/7</h5>
								<span>Call us: 0877-656-1199</span>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
						<div class="single-order c-fff bg-1 p-20">
							<div class="order-icon">
								<span class="fa fa-user"></span>
							</div>
							<div class="order-content">
								<h5>DISCOUNT</h5>
								<span>Upto 80% on all Products</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- order-area end -->
		<!-- footer-area start -->
		<?php include_once 'footer.php'; ?>
		<!-- footer-area end -->
    </body>

<!-- Mirrored from devitems.com/html/oneclick-preview/oneclick/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 17 Jan 2017 10:58:24 GMT -->
</html>
