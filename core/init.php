<?php
  error_reporting(0);
  session_start();
  date_default_timezone_set('Asia/Kolkata');

  function url(){
    if(isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off")){
        $protocol = 'https';
    }
    else{
        $protocol = 'http';
    }
    return $protocol . "://" . $_SERVER['HTTP_HOST'] . '/shop';
}
  //
  $uri = url();
  // $uri = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http'.'://'.$_SERVER['SERVER_NAME']. '/shop';

  $GLOBALS['config'] = [
    'mysql' => [
      'host' => 'localhost',
      'username' => 'root',
      'password' => '',
      'db' => 'shop'
    ],
    'remember' => [
      'cookie_name' => 'hash',
      'cookie_expiry' => 15552000
    ],
    'session' => [
      'session_name' => 'user',
      'token_name' => 'token'
    ]
  ];

    spl_autoload_register(function($class){

      require_once('classes/' . $class . '.php');

    });

    require_once('functions/sanitize.php');

    if (Cookie::exists(Config::get('remember/cookie_name')) && !Session::exists(Config::get('session/session_name'))) {
      $hash = Cookie::get(Config::get('remember/cookie_name'));
      $hashCheck = DB::getInstance()->get('users_session', ['hash', '=', $hash]);

      if ($hashCheck->count()) {
        $user = new User($hashCheck->first()->user_id);
        $user->login();
      }
    }
