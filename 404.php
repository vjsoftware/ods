<?php
  require_once 'core/init.php';
  $link = new Link();
 ?>
<!doctype html>
<html class="no-js" lang="">

<!-- Mirrored from devitems.com/html/oneclick-preview/oneclick/404.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 17 Jan 2017 10:59:36 GMT -->
<head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>404 | OneDayShop</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- Favicon -->
		<link rel="shortcut icon" type="image/x-icon" href="<?php echo $uri; ?>/img/favicon.ico">

		<!-- All css files are included here. -->
		<!-- animate css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/animate.min.css">
		<!-- Bootstrap fremwork main css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/bootstrap.min.css">
		<!-- font-awesome css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/font-awesome.min.css">
		<!-- nivo-slider css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/nivo-slider.css">
		<!-- owl carousel css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/owl.carousel.min.css">
		<!-- icofont css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/icofont.css">
		<!-- meanmenu css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/meanmenu.css">
		<!-- jquery-ui css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/jquery-ui.min.css">
		<!-- magnific-popup css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/magnific-popup.css">
		<!-- percircle css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/percircle.css">
		<!-- Theme main style -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/style.css">
		<!-- Responsive css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/responsive.css">

		<!-- Modernizr JS -->
        <script src="<?php echo $uri; ?>/js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

		 <!-- header start  -->
		<div class="main-container">
      <?php include_once 'header.php'; ?>

      <?php include_once 'menu.php'; ?>
			<!-- 404-area -->
			<div class="404-area ptb-140 bg-img-2">
				<div class="container">
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="page-not-found text-center">
								<h1>404</h1>
								<h2>Oops! Page Not Found.</h2>
								<p>It looks like nothing was found at this location. Maybe try one of the links below or a search?</p>
								<div class="input-src">
									<form action="#">
										<input type="text" placeholder="Search..........."/>
										<button>Search</button>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- footer-area start -->
		<?php require_once 'footer.php'; ?>
		<!-- footer-area end -->

		<!-- Placed js at the end of the document so the pages load faster -->
		<!-- jquery latest version -->
        <script src="<?php echo $uri; ?>/js/vendor/jquery-1.12.4.min.js"></script>
		<!-- magnific popup js -->
        <script src="<?php echo $uri; ?>/js/jquery.magnific-popup.min.js"></script>
		<!-- mixitup js -->
        <script src="<?php echo $uri; ?>/js/jquery.mixitup.min.js"></script>
		<!-- jquery-ui price-->
        <script src="<?php echo $uri; ?>/js/jquery-ui.min.js"></script>
		<!-- ScrollUp Js -->
        <script src="<?php echo $uri; ?>/js/jquery.scrollUp.min.js"></script>
		<!-- countDown Js -->
        <script src="<?php echo $uri; ?>/js/jquery.countdown.min.js"></script>
		<!-- nivo slider js -->
        <script src="<?php echo $uri; ?>/js/jquery.nivo.slider.pack.js"></script>
		<!-- mobail menu js -->
        <script src="<?php echo $uri; ?>/js/jquery.meanmenu.js"></script>
		<!-- Bootstrap framework js -->
        <script src="<?php echo $uri; ?>/js/bootstrap.min.js"></script>
		<!-- owl carousel js -->
        <script src="<?php echo $uri; ?>/js/owl.carousel.min.js"></script>
		<!-- All js plugins included in this file. -->
        <script src="<?php echo $uri; ?>/js/plugins.js"></script>
		<!-- Main js file that contents all jQuery plugins activation. -->
        <script src="<?php echo $uri; ?>/js/main.js"></script>
    </body>

<!-- Mirrored from devitems.com/html/oneclick-preview/oneclick/404.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 17 Jan 2017 10:59:36 GMT -->
</html>
