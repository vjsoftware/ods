$(document).ready(function(){
  // var cars = ['Audi', 'BMW', 'Bugatti', 'Ferrari', 'Ford', 'Lamborghini', 'Mercedes Benz', 'Porsche', 'Rolls-Royce', 'Volkswagen'];
//   var cars = new Bloodhound({
//   datumTokenizer: Bloodhound.tokenizers.whitespace,
//   queryTokenizer: Bloodhound.tokenizers.whitespace,
//   local: cars
// });
    var search = new Bloodhound({
      datumTokenizer: Bloodhound.tokenizers.obj.whitespace('search'),
      queryTokenizer: Bloodhound.tokenizers.whitespace,
      remote: {
        url: 'search_ajax.php?query=%QUERY',
        wildcard: '%QUERY'
      }
    });

    search.initialize();
    $('.typeahead').typeahead({
      hint: true,
      // highlight: true,
      minLength: 1
    }, {
      name: 'cars',
      displayKey: 'name',
      source: search.ttAdapter()
    });
});
