<?php
  include_once 'core/init.php';
  $uri_count = count($url->segments());
  $prod_id = $url->segment(4);

  $link = new Link();
  if ($uri_count == 5) {
    $product_check = DB::getInstance()->query("SELECT * FROM `product` WHERE `id` = $prod_id");
    if (!$product_check->count()) {
      Redirect::to($uri.'/404.php');
    } elseif ($product_check->first()->slug != $url->segment(3)) {
      Redirect::to($uri.'/product/'.$product_check->first()->slug.'/'.$product_check->first()->id);
    } else {
      $_GET['id'] = $prod_id;
    }
  } else {
    Redirect::to($uri.'/404.php');
  }

  $table = 'cart';
  $table1 = 'sales';
  $date = date('d-m-Y');
  $time = date('h:i A');
  $rand = rand(11111, 999999999);

  $product_id = $_GET['id'];

  $errors = [];

  if (isset($_POST['remove'])) {
    $user_id = $link->data()->id;
    $product_id = $_GET['id'];
    $cart_check = DB::getInstance()->query("SELECT * FROM `cart` WHERE `user_id` = '$user_id' AND `product_id` = '$product_id'");
    if ($cart_check->count()) {
      $uniq = $cart_check->first()->uniq;
      try {
        $link->delete_cart($table, $uniq);
        $link->delete_cart($table1, $uniq);
      } catch (Exception $e) {
        die($e);
      }

    }
  }

  if (isset($_POST['add_to_cart'])) {
    if ($link->isLoggedin()) {
      $validate = new Validate();
      $validation = $validate->check($_POST, [
        'quantity' => [
          'requierd' => TRUE,
          'min' => 1
        ]
      ]);
      if ($validation->passed()) {
        $user_id = $link->data()->id;
        $product_id = $_GET['id'];
        $product_details = DB::getInstance()->query("SELECT * FROM `product` WHERE `id` = '$product_id'");
        $price = $product_details->first()->price;
        try {
          $link->add([
            'uniq' => $rand,
            'user_id' => $user_id,
            'product_id' => $product_id,
            'qty' => $_POST['quantity'],
            'price' => $price,
            'return_qty' => $_POST['return_qty'],
            'date' => $date,
            'time' => $time
          ], $table);
        } catch (Exception $e) {
          die($e);
        }

        try {
          $link->add([
            'uniq' => $rand,
            'user_id' => $user_id,
            'product_id' => $product_id,
            'qty' => $_POST['quantity'],
            'price' => $price,
            'return_qty' => $_POST['return_qty'],
            'date' => $date,
            'time' => $time
          ], $table1);
          Redirect::to($uri."/cart");
        } catch (Exception $e) {
          die($e);
        }

      } else {
        foreach ($validation->errors() as $error) {
          die($error);
        }
      }
    } else {
      Redirect::to($uri.'/account');
    }
  }


  ?>

<?php $product_name = DB::getInstance()->query("SELECT * FROM `product` WHERE `id` = '$product_id'"); ?>

<!doctype html>
<html class="no-js" lang="en">

<head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title><?php echo $product_name->first()->name; ?> | OneDayShop</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- Favicon -->
		<link rel="shortcut icon" type="image/x-icon" href="<?php echo $uri; ?>/img/favicon.ico">

		<!-- All css files are included here. -->
		<!-- animate css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/animate.min.css">
		<!-- Bootstrap fremwork main css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/bootstrap.min.css">
		<!-- font-awesome css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/font-awesome.min.css">
		<!-- nivo-slider css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/nivo-slider.css">
		<!-- owl carousel css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/owl.carousel.min.css">
		<!-- icofont css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/icofont.css">
		<!-- meanmenu css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/meanmenu.css">
		<!-- jquery-ui css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/jquery-ui.min.css">
		<!-- magnific-popup css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/magnific-popup.css">
		<!-- percircle css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/percircle.css">
		<!-- Theme main style -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/style.css">
		<!-- Responsive css -->
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/responsive.css">
        <link rel="stylesheet" href="<?php echo $uri; ?>/css/glasscase.min.css">

		<!-- Modernizr JS -->
        <script src="<?php echo $uri; ?>/js/vendor/modernizr-2.8.3.min.js"></script>
        <style>
        .table{
          border: none;
          width: 70%;
          margin-left: 40px;
        }
        .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th {
          border: none;
          /*border: 1px solid #ccc;*/
        }
        table>tbody>tr>td:first-child {
          width: 25%;
        }
        </style>
    </head>
    <body>
		<!-- header start -->
		<header>

			<?php include_once 'header.php'; ?>

      <?php include_once 'menu.php'; ?>

		</header>
		<!-- header end -->
		<!-- simple product area start -->
    <?php
      $erd = $_GET['id'];
      $upd = DB::getInstance()->query("SELECT * FROM `product` WHERE `id`='$erd'");
      $image = $upd->first()->image;
      $name = $upd->first()->name;
      $price = $upd->first()->price;
      $category = $upd->first()->category;
      $description = $upd->first()->description;
      $specifications = $upd->first()->specifications;
    ?>
		<div class="simple-product-area">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="woocommerce-breadcrumb mtb-15">
							<div class="menu">
								<ul>
									<li><a href="<?php echo $uri; ?>/">Home</a></li>
									<li><a href="<?php echo $uri; ?>/product/<?php echo $upd->first()->name . '/' . $upd->first()->id?>"><?php echo $upd->first()->name; ?></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-lg-9">


						<div class="row">
							<div class="col-lg-5">
								<div class="symple-product mb-20">
									<div class="single-product-tab  box-shadow mb-20">

										<div class="tab-content">
											<div role="tabpanel" class="tab-pane active">
												<ul id="one" class="gc-start">

												  <li>
                            <img src="<?php echo $uri; ?>/images/<?php echo $image;?>" alt="<?php echo $name; ?>"/>
                          </li>
												</ul>

											</div>

										</div>
									</div>

									<div class="single-product-menu mb-30 box-shadow" style="background-color: #FFF; padding: 10px;">
										<div class="single-product-active clear">
											<div class="single-img floatleft">
												<a href="<?php echo $uri; ?>/#one" data-toggle="tab">
                          <?php if (file_exists('thumb/'.$product->image)): ?>
                            <img src="<?php echo $uri; ?>/images/<?php echo $image; ?>" alt="<?php echo $name; ?>" />
                          <?php else: ?>
                            <img src="<?php echo $uri; ?>/images/<?php echo $image; ?>" alt="<?php echo $name; ?>" />
                          <?php endif; ?>
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-7">
								<div class="symple-product box-shadow bg-fff p-15 mb-30">
									<h3><?php echo $name; ?></h3>
									<div class="product-content simple-product-content mb-10">
										<ul>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
										</ul>
										<a href="<?php echo $uri; ?>/#reviews">(3 customer reviews)</a><br/>
										<?php if ($upd->first()->f_price != 0 && !empty($upd->first()->f_price)): ?>
                      <span>Rs. <del><?php echo $upd->first()->f_price ?></del></span>
										  <?php endif; ?>
                      <span>Rs. <?php echo $upd->first()->price; ?></span>
									</div>
									<!-- <p>it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more</p> -->
                  <?php
                    $user_id = $link->data()->id;
                    $product_id = $_GET['id'];
                    $cart_check = DB::getInstance()->query("SELECT * FROM `cart` WHERE `user_id` = '$user_id' AND `product_id` = '$product_id'");
                  ?>
                  <?php if (!$cart_check->count()): ?>
                    <?php
                      // $product_id = $_GET['id'];
                      $product_details = DB::getInstance()->query("SELECT * FROM `product` WHERE `id` = '$product_id'");
                    ?>
                    <p><?php echo substr(strip_tags($product_details->first()->description), 0, 250); ?></p>
                    Stock: <span class="text-warning"><?php echo $product_details->first()->stock; ?></span>
                    <div class="simple-product-form contuct-form mtb-20 review-form form-style">
  										<form action="" method="post" class="col-lg-12">
                        <input type="hidden" name="price" value="<?php echo $product_details->first()->price; ?>">
                        <input type="hidden" name="return" value="<?php echo $product_details->first()->return_qty; ?>">
                        <div class="col-lg-2">
                          <input min="1" max="1000" name="quantity" value="1" type="number" required="">
                        </div>
                        <div class="col-lg-5 text-black">
                          <button type="submit" name="add_to_cart">Add To Cart</button>
                        </div>

  										</form>
  									</div>
                  <?php else: ?>
                    <div class="simple-product-form contuct-form mtb-20 review-form form-style">
  										<form action="" method="post" class="col-lg-12">
  											<!-- <input min="1" max="1000" name="quantity" value="1" type="number" required=""> -->
                        <div class="col-lg-6">
                          <button type="submit" name="remove">Remove from Cart</button>
                        </div>
  										</form>
  									</div>
                  <?php endif; ?>

									<!-- <div class="simple-product-icon c-fff hover-bg pb-20 mb-10 bb">
										<ul>
											<li><a href="<?php echo $uri; ?>/#" data-toggle="tooltip" title="Browser Wishlist"><i class="fa fa-heart-o"></i></a></li>
											<li><a href="<?php echo $uri; ?>/#" data-toggle="tooltip" title="Compare"><i class="fa fa-comments"></i></a></li>
										</ul>
									</div> -->
                  <?php $cats = explode(', ', $category); ?>
                  <?php $cat = DB::getInstance()->query("SELECT * FROM `category` WHERE `id` IN ($category)") ?>
									<div class="product_meta">
                    <div class="col-lg-12">
                      <br>
                    </div>
										<b>SKU:</b> <span>W-hat-8</span>
										<div class="category mb-10">
											<b>Categorie:</b>
                      <?php foreach ($cat->results() as $categ): ?>
                        <a href="<?php echo $uri; ?>/products/<?php echo $categ->name; ?>">
                          <?php echo $categ->name; ?>
                        </a>
                      <?php endforeach; ?>
										</div>
										<!-- <div class="single-blog-tag category bb pb-10">
											<b>Tags:</b>
											<a href="<?php echo $uri; ?>/#">fashion,</a>
										</div> -->

										<div class="footer-content pt-15 text-uppercase">
										<p>Share this product</p>

										<ul>
											<!-- <li>
												<a data-toggle="tooltip" title="Facebook" class="fb-share-button"
                        data-href="<?php echo $uri; ?>/http://www.your-domain.com/your-page.html"
                        data-layout="button_count"><i class="fa fa-facebook"></i></a>
											</li> -->
											<li>
                        <?php $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>
												<a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $actual_link; ?>"><i class="fa fa-facebook"></i></a>
											</li>
											<!-- <li>
												<a href="<?php echo $uri; ?>/#" data-toggle="tooltip" title="Twetter"><i class="fa fa-twitter"></i></a>
											</li> -->
										</ul>
									</div>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-lg-12">
								<div class="simple-product-tab box-shadow">
									<div class="simple-product-tab-menu clear">
										<ul>
											<li class="active"><a href="#description" data-toggle="tab">Description</a></li>
											<li><a href="#specifications" data-toggle="tab">Specifications</a></li>
											<li><a href="#reviews" data-toggle="tab">Reviews (3)</a></li>
										</ul>
									</div>
									<div class="tab-content  bg-fff">
										<div class="tab-pane active" id="description">
											<div class="product-description p-20">
												<h2>Product Description</h2>
												<p><?php echo $description; ?></p>
											</div>
										</div>
										<div class="tab-pane" id="specifications">
											<div class="product-description p-20">
                        <div class="row">
                          <!-- <h2>Product Specifications</h2> -->
                          <p><?php echo $specifications; ?></p>
                        </div>
											</div>
										</div>
										<div class="tab-pane" id="reviews">
											<div class="product-reviews p-20">
												<div class="row">
													<div class="col-lg-8">
														<div class="review-area">
															<h2>3 reviews for Cras nec nisl ut erat</h2>
															<ul>
																<li class="review-1">
																	<div class="review mb-20">
																		<div class="review-img">
																			<img class="img-thumbnail" src="<?php echo $uri; ?>/img/comment/1.png" alt="" />
																		</div>
																		<div class="review-content p-15">
																			<b>admin -</b>
																			<div class="review-rating product-content simple-product-content pull-right">
																				<ul>
																					<li><i class="fa fa-star"></i></li>
																					<li><i class="fa fa-star"></i></li>
																					<li><i class="fa fa-star"></i></li>
																					<li><i class="fa fa-star"></i></li>
																					<li><i class="fa fa-star"></i></li>
																				</ul>
																			</div>
																			<p>December 16, 2015:</p>
																			<p>like</p>
																		</div>
																	</div>
																</li>
																<li>
																	<div class="review mb-20">
																		<div class="review-img">
																			<img class="img-thumbnail" src="<?php echo $uri; ?>/img/comment/1.png" alt="" />
																		</div>
																		<div class="review-content p-15">
																			<b>alex -</b>
																			<div class="review-rating product-content simple-product-content pull-right">
																				<ul>
																					<li><i class="fa fa-star"></i></li>
																					<li><i class="fa fa-star"></i></li>
																					<li><i class="fa fa-star"></i></li>
																					<li><i class="fa fa-star"></i></li>
																					<li><i class="fa fa-star"></i></li>
																				</ul>
																			</div>
																			<span> December 18, 2015: </span>
																			<p>google</p>
																		</div>
																	</div>
																</li>
																<li>
																	<div class="review mb-20">
																		<div class="review-img">
																			<img class="img-thumbnail" src="<?php echo $uri; ?>/img/comment/1.png" alt="" />
																		</div>
																		<div class="review-content p-15">
																			<b>name -</b>
																			<div class="review-rating product-content simple-product-content pull-right">
																				<ul>
																					<li><i class="fa fa-star"></i></li>
																					<li><i class="fa fa-star"></i></li>
																					<li><i class="fa fa-star"></i></li>
																					<li><i class="fa fa-star"></i></li>
																					<li><i class="fa fa-star"></i></li>
																				</ul>
																			</div>
																			<span>August 11, 2016: </span>
																			<p>my rating</p>
																		</div>
																	</div>
																</li>
															</ul>
														</div>
													</div>
													<div class="col-lg-4">
														<div class="review-form form-style">
															<h2>Add a review</h2>
															<p>Add a review</p>
															<div class="review-rating product-content simple-product-content">
																<ul>
																	<li><i class="fa fa-star"></i></li>
																	<li><i class="fa fa-star"></i></li>
																	<li><i class="fa fa-star"></i></li>
																	<li><i class="fa fa-star"></i></li>
																	<li><i class="fa fa-star"></i></li>
																</ul>
															</div>
															<form action="#">
																<p>Your Review</p>
																<textarea name="#" id="#" cols="30" rows="10"></textarea>
																<p>Name *</p>
																<input type="text" />
																<p>Emai *</p>
																<input type="email" />
																<button>Submit</button>
															</form>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="product box-shadow mtb-50 bg-fff">
							<div class="product-title home2-product-title home2-bg-1 text-uppercase">
								<i class="fa fa-star-o icon bg-4"></i>
								<h3>Up-Sells Products</h3>
							</div>
							<div class="new-product-active left-right-angle home2">
                <?php $product_list = DB::getInstance()->query("SELECT * FROM `product` ORDER BY `id` DESC LIMIT 0, 7"); ?>
                <?php foreach ($product_list->results() as $product): ?>
                  <div class="product-wrapper bl">
  									<div class="product-img" style="max-height: 300px; margin-top: 15px;">
  										<a href="<?php echo $uri; ?>/product/<?php echo $product->slug . '/' . $product->id; ?>">
                        <?php if (file_exists('resized/'.$product->image)): ?>
                          <img src="<?php echo $uri; ?>/resized/<?php echo $product->image; ?>" alt="<?php echo $product->name; ?>" />
                        <?php else: ?>
                          <img src="<?php echo $uri; ?>/images/<?php echo $product->image; ?>" alt="<?php echo $product->name; ?>" />
                        <?php endif; ?>
  											<!-- <img src="img/product/2.jpg" alt="" class="secondary"/> -->
  										</a>
  										<div class="product-icon c-fff hover-bg">
  											<!-- <ul>
  												<li><a href="<?php echo $uri; ?>/#" data-toggle="tooltip" title="Add to cart"><i class="fa fa-shopping-cart"></i></a></li>
  												<li><a href="<?php echo $uri; ?>/#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a></li>
  												<li><a href="<?php echo $uri; ?>/#" data-toggle="tooltip" title="Compare"><i class="fa fa-comments"></i></a></li>
  												<li><a href="<?php echo $uri; ?>/#" data-toggle="tooltip" title="Accumsan eli"><i class="fa fa-search"></i></a></li>
  											</ul> -->
  										</div>
  										<!-- <span class="sale">Sale</span> -->
  									</div>
  									<div class="product-content">
  										<h3 style="height: 30px;"><a href="<?php echo $uri; ?>/product/<?php echo $product->slug . '/' . $product->id; ?>"><?php echo substr($product->name, 0, 45); ?></a></h3>
  										<ul>
  											<li><i class="fa fa-star"></i></li>
  											<li><i class="fa fa-star"></i></li>
  											<li><i class="fa fa-star"></i></li>
  											<li><i class="fa fa-star"></i></li>
  											<li><i class="fa fa-star"></i></li>
  										</ul>
  										<span><?php echo $product->price; ?></span>
  									</div>
  								</div>
                <?php endforeach; ?>
							</div>
						</div>
						<div class="product box-shadow mb-50 bg-fff">
							<div class="product-title home2-product-title home2-bg-1 text-uppercase">
								<i class="fa fa-star-o icon bg-4"></i>
								<h3>Related Products</h3>
							</div>
              <div class="new-product-active left-right-angle home2">
                <?php $product_list = DB::getInstance()->query("SELECT * FROM `product` ORDER BY `id` ASC LIMIT 0, 7"); ?>
                <?php foreach ($product_list->results() as $product): ?>
                  <div class="product-wrapper bl">
  									<div class="product-img" style="max-height: 300px; margin-top: 15px;">
  										<a href="<?php echo $uri; ?>/product/<?php echo $product->slug . '/' . $product->id; ?>">
                        <?php if (file_exists('resized/'.$product->image)): ?>
                          <img src="<?php echo $uri; ?>/resized/<?php echo $product->image; ?>" alt="<?php echo $product->name; ?>" />
                        <?php else: ?>
                          <img src="<?php echo $uri; ?>/images/<?php echo $product->image; ?>" alt="<?php echo $product->name; ?>" />
                        <?php endif; ?>
  											<!-- <img src="img/product/2.jpg" alt="" class="secondary"/> -->
  										</a>
  										<div class="product-icon c-fff hover-bg">
  											<!-- <ul>
  												<li><a href="<?php echo $uri; ?>/#" data-toggle="tooltip" title="Add to cart"><i class="fa fa-shopping-cart"></i></a></li>
  												<li><a href="<?php echo $uri; ?>/#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a></li>
  												<li><a href="<?php echo $uri; ?>/#" data-toggle="tooltip" title="Compare"><i class="fa fa-comments"></i></a></li>
  												<li><a href="<?php echo $uri; ?>/#" data-toggle="tooltip" title="Accumsan eli"><i class="fa fa-search"></i></a></li>
  											</ul> -->
  										</div>
  										<!-- <span class="sale">Sale</span> -->
  									</div>
  									<div class="product-content">
  										<h3 style="height: 30px;"><a href="<?php echo $uri; ?>/product/<?php echo $product->slug . '/' . $product->id; ?>"><?php echo substr($product->name, 0, 45); ?></a></h3>
  										<ul>
  											<li><i class="fa fa-star"></i></li>
  											<li><i class="fa fa-star"></i></li>
  											<li><i class="fa fa-star"></i></li>
  											<li><i class="fa fa-star"></i></li>
  											<li><i class="fa fa-star"></i></li>
  										</ul>
  										<span><?php echo $product->price; ?></span>
  									</div>
  								</div>
                <?php endforeach; ?>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
						<!-- categories-area start -->
						<div class="categories-area box-shadow bg-fff">
							<div class="product-title home2-bg-1 text-uppercase home2-product-title">
								<i class="fa fa-bookmark icon bg-4"></i>
								<h3>categories</h3>
							</div>
							<div class="shop-categories-menu p-20 mb-30">
								<ul>
                  <?php $category_list = DB::getInstance()->query("SELECT * FROM `category` LIMIT 0, 6"); ?>
                  <?php foreach ($category_list->results() as $category): ?>
                    <li><a href="<?php echo $uri; ?>/products/<?php echo $category->name; ?>"><?php echo $category->name; ?></a><span></span></li>
                  <?php endforeach; ?>
								</ul>
							</div>
						</div>
						<!-- featured-area start -->
						<div class="featured-area bg-fff box-shadow mb-30">
							<div class="product-title home2-bg-1 text-uppercase home2-product-title">
								<i class="fa fa-bookmark icon bg-4"></i>
								<h3>New Products</h3>
							</div>
							<div class="featured-wrapper p-20">
                <?php $product_list = DB::getInstance()->query("SELECT * FROM `product` ORDER BY `id` DESC LIMIT 0, 5"); ?>
                <?php foreach ($product_list->results() as $products): ?>
                  <div class="product-wrapper single-featured">
  									<div class="product-content floatleft">
  										<h3 style="height: 10px;"><a href="<?php echo $uri; ?>/product/<?php echo $products->slug . '/' . $products->id; ?>"><?php echo substr($products->name, 0, 17); ?></a></h3>
  										<ul>
  											<li><i class="fa fa-star"></i></li>
  											<li><i class="fa fa-star"></i></li>
  											<li><i class="fa fa-star"></i></li>
  											<li><i class="fa fa-star"></i></li>
  											<li><i class="fa fa-star"></i></li>
  										</ul>
  										<!-- <small>by name</small> -->
  									</div>
  									<div class="product-img floatright">
  										<a href="<?php echo $uri; ?>/product/<?php echo $products->slug . '/' . $products->id; ?>">
                        <?php if (file_exists('thumb/'.$products->image)): ?>
                          <img src="<?php echo $uri; ?>/thumb/<?php echo $products->image; ?>" alt="<?php echo $products->name; ?>" />
                        <?php else: ?>
                          <img src="<?php echo $uri; ?>/images/<?php echo $products->image; ?>" alt="<?php echo $products->name; ?>" />
                        <?php endif; ?>
  										</a>
  									</div>
  								</div>
                <?php endforeach; ?>
							</div>
						</div>
						<!-- product-tags-area start -->
						<!-- <div class="product-tags-area bg-fff box-shadow mtb-30">
							<div class="product-title home2-bg-1 text-uppercase home2-product-title">
								<i class="fa fa-bookmark icon bg-4"></i>
								<h3>Product Tags</h3>
							</div>
							<div class="tags tag-menu hover-bg p-20">
								<ul>
									<li><a href="<?php echo $uri; ?>/#">commodo</a></li>
									<li><a href="<?php echo $uri; ?>/#">enim</a></li>
									<li><a href="<?php echo $uri; ?>/#">fashion</a></li>
									<li><a href="<?php echo $uri; ?>/#">Fly</a></li>
									<li><a href="<?php echo $uri; ?>/#">Glasses</a></li>
									<li><a href="<?php echo $uri; ?>/#">Hats</a></li>
									<li><a href="<?php echo $uri; ?>/#">Hoodies</a></li>
									<li><a href="<?php echo $uri; ?>/#">libero</a></li>
									<li><a href="<?php echo $uri; ?>/#">men</a></li>
									<li><a href="<?php echo $uri; ?>/#">Men's</a></li>
									<li><a href="<?php echo $uri; ?>/#">Nam</a></li>
									<li><a href="<?php echo $uri; ?>/#">Popular</a></li>
									<li><a href="<?php echo $uri; ?>/#">Product</a></li>
									<li><a href="<?php echo $uri; ?>/#">version</a></li>
									<li><a href="<?php echo $uri; ?>/#">women</a></li>
								</ul>
							</div>
						</div> -->
						<!-- compare-area start -->
						<!-- <div class="compare-area bg-fff box-shadow mb-30">
							<div class="product-title home2-bg-1 text-uppercase home2-product-title">
								<i class="fa fa-bookmark icon bg-4"></i>
								<h3>Compare</h3>
							</div>
							<div class="compare-menu p-20">
								<p>No products to compare</p>
								<a href="<?php echo $uri; ?>/#">Clear all</a>
								<a href="<?php echo $uri; ?>/#" data-toggle="tooltip" title="Compare" class="pull-right compare text-uppercase" >Compare </a>
							</div>
						</div> -->
					</div>
				</div>
			</div>
		</div>
		<!-- simple product area end -->
		<!-- footer-area start -->
		<?php include_once 'footer.php'; ?>
		<!-- footer-area end -->
    <!-- Modernizr a JavaScript library that detects HTML5 and CSS3 features in the user’s browser -->
    <script src="<?php echo $uri; ?>/js/modernizr.custom.js" type="text/javascript"></script>
    <!-- GlassCase plugin's JS script file -->
    <script src="<?php echo $uri; ?>/js/jquery.glasscase.min.js" type="text/javascript"></script>
    <!-- Calling the GlassCase plugin -->
    <script type="text/javascript">

        $(document).ready( function () {
            //If your <ul> has the id "glasscase"
            $('#one').glassCase({
              'heightDisplay': '400',
              // 'zoomPosition': 'inner'
            });
        });

    </script>
    <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=324542377587309";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));
    </script>

    </body>

<!-- Mirrored from devitems.com/html/oneclick-preview/oneclick/shop-simple-product.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 17 Jan 2017 10:59:23 GMT -->
</html>
