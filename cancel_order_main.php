<?php

  require_once 'core/init.php';

  $link = new Link();

  $user_id = $link->data()->id;

  $table = 'sales';
  $table2 = 'cart';


  $total = 0;
  $product_qty = 0;

  $result = [];
  if (isset($_POST['reason']) && !empty($_POST['reason'])) {
    $reason = $_POST['reason'];
    $comment = $_POST['comment'];
    $delete_id = $_POST['delete_id'];
    $delete_order_id = $_POST['delete_order_id'];

    try {
      $link->delete_cart($table, $delete_order_id);
      $link->delete_cart($table2, $delete_order_id);

      $result['delete_id']  = $delete_id;
      $result['status']  = 'ok';
    } catch (Exception $e) {
      echo $e;
    }

  }

  echo json_encode($result);
?>
