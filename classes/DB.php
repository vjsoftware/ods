<?php

  /**
   *
   */
  class DB
  {

    private static $_instance = null;

    private $_pdo,
            $_query,
            $_error = FALSE,
            $_results,
            $_count = 0;

    private function __construct()
    {
      try {

        $this->_pdo = new PDO('mysql:host=' . Config::get('mysql/host') . ';dbname=' . Config::get('mysql/db'), Config::get('mysql/username'), Config::get('mysql/password'));

      } catch (PDOException $e) {
        die($e->getMessage());
      }

    }

    public static function getInstance() {

        if(!isset(self::$_instance)) {

          self::$_instance = new DB();

        }

        return self::$_instance;

    }

    public function query($sql, $params = []) {

        $this->_error = false;

        if ($this->_query = $this->_pdo->prepare($sql)) {

          $x = 1;

          if (count($params)) {
            foreach ($params as $param) {
              $this->_query->bindValue($x, $param);
              $x++;
            }
          }

          if ($this->_query->execute()) {
            $this->_results = $this->_query->fetchall(PDO::FETCH_OBJ);
            $this->_count = $this->_query->rowCount();
          } else {
            $this->_error = TRUE;
          }

        }

        return $this;

    }

    private function action($action, $table, $where = []) {

      if (count($where) === 3) {
        $operators = ['=', '>', '<', '>=', '<='];

        $field = $where[0];
        $operator = $where[1];
        $value = $where[2];

        if (in_array($operator, $operators)) {
          $sql = "{$action} FROM {$table} WHERE {$field} {$operator} ?";

          if (!$this->query($sql, [$value])->error()) {
            return $this;
          }
        }

      }
      return FALSE;
    }

    public function get($table, $where) {

      return $this->action('SELECT *', $table, $where);

    }

    public function delete($table, $where) {

      return $this->action('DELETE', $table, $where);

    }

    public function insert($table, $fields = []) {

      if (count($fields)) {

        $keys = array_keys($fields);
        $values = '';
        $x = 1;

        foreach ($fields as $field) {
          $values .= '?' ;

          if ($x < count($fields)) {
            $values .= ', ';
          }
          $x++;
        }

        $sql = "INSERT INTO {$table} (`" . implode('`, `', $keys) . "`) VALUES ({$values})";

        if (!$this->query($sql, $fields)->error()) {
          return TRUE;
        }

      }
      return FALSE;
    }

    public function update($table, $id, $fields) {

      $set = '';
      $x = 1;

      foreach ($fields as $name => $value) {
        $set .= "`{$name}` = ?";
        if ($x < count($fields)) {
          $set .= ', ';
        }
        $x++;
      }

      $sql = "UPDATE {$table} SET {$set} WHERE `id` = {$id}";

      if (!$this->query($sql, $fields)->error()) {
        return TRUE;
      }
      return FALSE;
    }

    public function update_sales($table, $id, $fields) {

      $set = '';
      $x = 1;

      foreach ($fields as $name => $value) {
        $set .= "`{$name}` = ?";
        if ($x < count($fields)) {
          $set .= ', ';
        }
        $x++;
      }

      $sql = "UPDATE {$table} SET {$set} WHERE `uniq` = {$id}";

      if (!$this->query($sql, $fields)->error()) {
        return TRUE;
      }
      return FALSE;
    }

    public function update_order_id($table, $id, $fields) {

      $set = '';
      $x = 1;

      foreach ($fields as $name => $value) {
        $set .= "`{$name}` = ?";
        if ($x < count($fields)) {
          $set .= ', ';
        }
        $x++;
      }

      $sql = "UPDATE {$table} SET {$set} WHERE `order_id` = '{$id}'";

      if (!$this->query($sql, $fields)->error()) {
        return TRUE;
      }
      return FALSE;
    }

    public function registration_update($table, $id, $fields) {

      $set = '';
      $x = 1;

      foreach ($fields as $name => $value) {
        $set .= "{$name} = ?";
        if ($x < count($fields)) {
          $set .= ', ';
        }
        $x++;
      }

      $sql = "UPDATE {$table} SET {$set} WHERE id = {$id}";

      if (!$this->query($sql, $fields)->error()) {
        return TRUE;
      }
      return FALSE;
    }

    public function vision_report_update($table, $id, $fields) {

      $set = '';
      $x = 1;

      foreach ($fields as $name => $value) {
        $set .= "{$name} = ?";
        if ($x < count($fields)) {
          $set .= ', ';
        }
        $x++;
      }

      $sql = "UPDATE {$table} SET {$set} WHERE visit_id = {$id}";

      if (!$this->query($sql, $fields)->error()) {
        return TRUE;
      }
      return FALSE;
    }

    public function visit_log_update($table, $id, $fields) {

      $set = '';
      $x = 1;

      foreach ($fields as $name => $value) {
        $set .= "{$name} = ?";
        if ($x < count($fields)) {
          $set .= ', ';
        }
        $x++;
      }

      $sql = "UPDATE {$table} SET {$set} WHERE id = {$id}";

      if (!$this->query($sql, $fields)->error()) {
        return TRUE;
      }
      return FALSE;
    }

    public function supplier_edit($table, $id, $fields) {

      $set = '';
      $x = 1;

      foreach ($fields as $name => $value) {
        $set .= "{$name} = ?";
        if ($x < count($fields)) {
          $set .= ', ';
        }
        $x++;
      }

      $sql = "UPDATE {$table} SET {$set} WHERE id = {$id}";

      if (!$this->query($sql, $fields)->error()) {
        return TRUE;
      }
      return FALSE;
    }

    public function medical_insurance_edit($table, $id, $fields) {

      $set = '';
      $x = 1;

      foreach ($fields as $name => $value) {
        $set .= "{$name} = ?";
        if ($x < count($fields)) {
          $set .= ', ';
        }
        $x++;
      }

      $sql = "UPDATE {$table} SET {$set} WHERE id = {$id}";

      if (!$this->query($sql, $fields)->error()) {
        return TRUE;
      }
      return FALSE;
    }

    public function eye_doctor_report_update($table, $mrid_id, $visit_id, $fields) {

      $set = '';
      $x = 1;

      foreach ($fields as $name => $value) {
        $set .= "{$name} = ?";
        if ($x < count($fields)) {
          $set .= ', ';
        }
        $x++;
      }

      $sql = "UPDATE {$table} SET {$set} WHERE mrid = {$mrid_id} AND visit_id = {$visit_id}";

      if (!$this->query($sql, $fields)->error()) {
        return TRUE;
      }
      return FALSE;
    }

    public function new_order_update($table, $rand_id, $item_id, $fields) {

      $set = '';
      $x = 1;

      foreach ($fields as $name => $value) {
        $set .= "{$name} = ?";
        if ($x < count($fields)) {
          $set .= ', ';
        }
        $x++;
      }

      $sql = "UPDATE {$table} SET {$set} WHERE rand_id = {$rand_id} AND item_id = {$item_id}";

      if (!$this->query($sql, $fields)->error()) {
        return TRUE;
      }
      return FALSE;
    }

    public function opticals_new_order_update($table, $rand_id, $item_id, $fields) {

      $set = '';
      $x = 1;

      foreach ($fields as $name => $value) {
        $set .= "{$name} = ?";
        if ($x < count($fields)) {
          $set .= ', ';
        }
        $x++;
      }

      $sql = "UPDATE {$table} SET {$set} WHERE rand_id = {$rand_id} AND item_id = {$item_id}";

      if (!$this->query($sql, $fields)->error()) {
        return TRUE;
      }
      return FALSE;
    }

    public function pharmacy_new_order_update2($table, $id, $fields) {

      $set = '';
      $x = 1;

      foreach ($fields as $name => $value) {
        $set .= "{$name} = ?";
        if ($x < count($fields)) {
          $set .= ', ';
        }
        $x++;
      }

      $sql = "UPDATE {$table} SET {$set} WHERE id = {$id}";

      if (!$this->query($sql, $fields)->error()) {
        return TRUE;
      }
      return FALSE;
    }

    public function pharmacy_new_order_update3($table, $id, $fields) {

      $set = '';
      $x = 1;

      foreach ($fields as $name => $value) {
        $set .= "{$name} = ?";
        if ($x < count($fields)) {
          $set .= ', ';
        }
        $x++;
      }

      $sql = "UPDATE {$table} SET {$set} WHERE rand_id = {$id} AND status = 1";

      if (!$this->query($sql, $fields)->error()) {
        return TRUE;
      }
      return FALSE;
    }

    public function pharmacy_update_nn($table, $visit_id, $item_id, $fields) {

      $set = '';
      $x = 1;

      foreach ($fields as $name => $value) {
        $set .= "{$name} = ?";
        if ($x < count($fields)) {
          $set .= ', ';
        }
        $x++;
      }

      $sql = "UPDATE {$table} SET {$set} WHERE visit_id = {$visit_id} AND item_id = {$item_id}";

      if (!$this->query($sql, $fields)->error()) {
        return TRUE;
      }
      return FALSE;
    }

    public function opticals_update_nn($table, $visit_id, $item_id, $fields) {

      $set = '';
      $x = 1;

      foreach ($fields as $name => $value) {
        $set .= "{$name} = ?";
        if ($x < count($fields)) {
          $set .= ', ';
        }
        $x++;
      }

      $sql = "UPDATE {$table} SET {$set} WHERE visit_id = {$visit_id} AND item_id = {$item_id}";

      if (!$this->query($sql, $fields)->error()) {
        return TRUE;
      }
      return FALSE;
    }

    public function results() {

      return $this->_results;

    }

    public function first() {

      return $this->results()[0];

    }

    public function error() {

      return $this->_error;

    }

    public function count() {

      return $this->_count;

    }

  }
