<?php

  /**
   *
   */
  class User
  {
    private $_db;
    private $_data;
    private $_sessionName;
    private $_cookieName;
    private $_isLoggedIn;

    function __construct($user = null)
    {
      $this->_db = DB::getInstance();
      $this->_sessionName = Config::get('session/session_name');
      $this->_cookieName = Config::get('remember/cookie_name');

      if (!$user) {
        if (Session::exists($this->_sessionName)) {
          $user = Session::get($this->_sessionName);

          if ($this->find($user)) {
            $this->_isLoggedIn = TRUE;
          } else {
            // Precess Logout
          }
        }
      } else {
        $this->find($user);
      }
    }

    public function update($fields = [], $id = null)
    {
      // if (!$id && $this->isLoggedIn()) {
      //   $id = $this->data()->id;
      // }

      if (!$this->_db->update('users', $id, $fields)) {
        throw new Exception("There was an problem updating");

      }
    }

    public function update_sales($fields = [], $table, $id = null)
    {
      // if (!$id && $this->isLoggedIn()) {
      //   $id = $this->data()->id;
      // }

      if (!$this->_db->update_sales($table, $id, $fields)) {
        throw new Exception("There was an problem updating");

      }
    }

    public function updates($fields = [], $table, $id = null)
    {

      if (!$this->_db->update($table, $id, $fields)) {
        throw new Exception("There was an problem updating");

      }
    }

    public function create($fields = [])
    {
      if (!$this->_db->insert('users', $fields)) {
        throw new Exception("There was a problem creating an account.");
      }
    }

    public function add($fields = [], $table)
    {
      if (!$this->_db->insert($table, $fields)) {
        throw new Exception("There was a problem creating an account.");
      }
    }

    public function create_pharmacy($fields = [])
    {
      if (!$this->_db->insert('pharmacy', $fields)) {
        throw new Exception("There was a problem creating an account.");
      }
    }

    public function create_opticals_payments($fields = [])
    {
      if (!$this->_db->insert('opticals_payments', $fields)) {
        throw new Exception("There was a problem creating an account.");
      }
    }

    public function visit_log($fields = [])
    {
      if (!$this->_db->insert('visit_log', $fields)) {
        throw new Exception("There was a problem creating an account.");
      }
    }

    public function registration_update($fields = [], $id = null)
    {

      if (!$this->_db->registration_update('registration', $id, $fields)) {
        throw new Exception("There was an problem updating Patient Details");

      }
    }

    public function vision_report_update($fields = [], $id = null)
    {

      if (!$this->_db->vision_report_update('work_up', $id, $fields)) {
        throw new Exception("There was an problem updating Vision Report");

      }
    }

    public function visit_log_update($fields = [], $id = null)
    {
      // if (!$id && $this->isLoggedIn()) {
      //   $id = $this->data()->id;
      // }

      if (!$this->_db->visit_log_update('visit_log', $id, $fields)) {
        throw new Exception("There was an problem updating");

      }
    }

    public function new_order_update($fields = [], $rand_id = null, $item_id = null)
    {
      // if (!$id && $this->isLoggedIn()) {
      //   $id = $this->data()->id;
      // }

      if (!$this->_db->new_order_update('pharmacy_new_order', $rand_id, $item_id, $fields)) {
        throw new Exception("There was an problem updating");

      }
    }

    public function opticals_new_order_update($fields = [], $rand_id = null, $item_id = null)
    {
      // if (!$id && $this->isLoggedIn()) {
      //   $id = $this->data()->id;
      // }

      if (!$this->_db->opticals_new_order_update('opticals_new_order', $rand_id, $item_id, $fields)) {
        throw new Exception("There was an problem updating");

      }
    }

    public function pharmacy_new_order_update2($fields = [], $id = null)
    {

      if (!$this->_db->pharmacy_new_order_update2('pharmacy_new_order', $id, $fields)) {
        throw new Exception("There was an problem updating");

      }
    }

    public function opticlas_new_order_update2($fields = [], $id = null)
    {

      if (!$this->_db->pharmacy_new_order_update2('opticals_new_order', $id, $fields)) {
        throw new Exception("There was an problem updating");

      }
    }

    public function pharmacy_new_order_update3($fields = [], $id = null)
    {

      if (!$this->_db->pharmacy_new_order_update3('pharmacy_new_order', $id, $fields)) {
        throw new Exception("There was an problem updating");

      }
    }

    public function opticals_new_order_update3($fields = [], $id = null)
    {

      if (!$this->_db->pharmacy_new_order_update3('opticals_new_order', $id, $fields)) {
        throw new Exception("There was an problem updating");

      }
    }

    public function pharmacy_update_nn($fields = [], $visit_id = null, $item_id = null)
    {
      // if (!$id && $this->isLoggedIn()) {
      //   $id = $this->data()->id;
      // }

      if (!$this->_db->pharmacy_update_nn('pharmacy_sales', $visit_id, $item_id, $fields)) {
        throw new Exception("There was an problem updating". $visit_id . $item_id);

      }
    }

    public function opticals_update_nn($fields = [], $visit_id = null, $item_id = null)
    {
      // if (!$id && $this->isLoggedIn()) {
      //   $id = $this->data()->id;
      // }

      if (!$this->_db->opticals_update_nn('opticals_sales', $visit_id, $item_id, $fields)) {
        throw new Exception("There was an problem updating". $visit_id . $item_id);

      }
    }

    public function surgery_update_nn($fields = [], $visit_id = null, $item_id = null)
    {
      // if (!$id && $this->isLoggedIn()) {
      //   $id = $this->data()->id;
      // }

      if (!$this->_db->opticals_update_nn('surgery', $visit_id, $item_id, $fields)) {
        throw new Exception("There was an problem updating". $visit_id . $item_id);

      }
    }

    public function diagnostics_update_nn($fields = [], $visit_id = null, $item_id = null)
    {
      // if (!$id && $this->isLoggedIn()) {
      //   $id = $this->data()->id;
      // }

      if (!$this->_db->opticals_update_nn('diagnostics', $visit_id, $item_id, $fields)) {
        throw new Exception("There was an problem updating". $visit_id . $item_id);

      }
    }

    public function doctor_pharmacy_update($fields = [], $id = null)
    {

      if (!$this->_db->visit_log_update('doctor_pharmacy', $id, $fields)) {
        throw new Exception("There was an problem updating");

      }
    }

    public function paharmacy_main_update($fields = [], $id = null)
    {

      if (!$this->_db->visit_log_update('pharmacy_log', $id, $fields)) {
        throw new Exception("There was an problem updating");

      }
    }

    public function pharmacy_update($fields = [], $id = null)
    {

      if (!$this->_db->visit_log_update('pharmacy', $id, $fields)) {
        throw new Exception("There was an problem updating");

      }
    }

    public function eye_doctor_report_update($fields = [], $mrid_id = null, $visit_id)
    {

      if (!$this->_db->eye_doctor_report_update('eye_doctor_report', $mrid_id, $visit_id, $fields)) {
        throw new Exception("There was an problem updating");

      }
    }

    public function add_client_tablets($fields = [])
    {
      if (!$this->_db->insert('pharmacy_sales', $fields)) {
        throw new Exception("There was a problem with Adjustments.");
      }
    }
    public function add_client_surgery_report($fields = [])
    {
      if (!$this->_db->insert('surgery_upload', $fields)) {
        throw new Exception("There was a problem with Upload.");
      }
    }

    public function add_client_diagnostics_report($fields = [])
    {
      if (!$this->_db->insert('diagnostics_upload', $fields)) {
        throw new Exception("There was a problem with Upload.");
      }
    }

    public function medical_insurance_edit($fields = [], $id = null)
    {

      if (!$this->_db->medical_insurance_edit('medical_insurance', $id, $fields)) {
        throw new Exception("There was an problem updating");

      }
    }

    public function supplier_edit($fields = [], $id = null)
    {

      if (!$this->_db->supplier_edit('new_supplier', $id, $fields)) {
        throw new Exception("There was an problem updating");

      }
    }

    public function opticals_supplier_edit($fields = [], $id = null)
    {

      if (!$this->_db->supplier_edit('optical_new_supplier', $id, $fields)) {
        throw new Exception("There was an problem updating");

      }
    }

    public function surgery_supplier_edit($fields = [], $id = null)
    {

      if (!$this->_db->supplier_edit('surgery_new_supplier', $id, $fields)) {
        throw new Exception("There was an problem updating");

      }
    }

    public function diagnostics_supplier_edit($fields = [], $id = null)
    {

      if (!$this->_db->supplier_edit('diagnostics_new_supplier', $id, $fields)) {
        throw new Exception("There was an problem updating");

      }
    }

    public function item_edit($fields = [], $id = null)
    {

      if (!$this->_db->supplier_edit('new_item', $id, $fields)) {
        throw new Exception("There was an problem updating");

      }
    }

    public function opticals_item_edit($fields = [], $id = null)
    {

      if (!$this->_db->supplier_edit('opticals_new_item', $id, $fields)) {
        throw new Exception("There was an problem updating");

      }
    }

    public function surgery_item_edit($fields = [], $id = null)
    {

      if (!$this->_db->supplier_edit('surgery_new_item', $id, $fields)) {
        throw new Exception("There was an problem updating");

      }
    }

    public function diagnostics_item_edit($fields = [], $id = null)
    {

      if (!$this->_db->supplier_edit('diagnostics_new_item', $id, $fields)) {
        throw new Exception("There was an problem updating");

      }
    }

    public function delete_user($id = null)
    {

      $this->_db->delete('users', ['id', '=', $id]);

    }

    public function deletes($table, $id = null)
    {

      $this->_db->delete($table , ['id', '=', $id]);

    }

    public function delete_cart($table, $id = null)
    {

      $this->_db->delete($table , ['uniq', '=', $id]);

    }

    public function delete_visit_log($id = null)
    {

      $this->_db->delete('visit_log', ['id', '=', $id]);

    }

    public function delete_pharmacy_doc($id = null)
    {

      $this->_db->delete('pharmacy_sales', ['id', '=', $id]);

    }

    public function delete_optical_tbl($id = null)
    {

      $this->_db->delete('opticals_sales', ['id', '=', $id]);

    }

    public function delete_optical_payment($id = null)
    {

      $this->_db->delete('opticals_payments', ['payment_id', '=', $id]);

    }

    public function delete_surgery_tbl($id = null)
    {

      $this->_db->delete('surgery', ['id', '=', $id]);

    }

    public function delete_diagnostics_tbl($id = null)
    {

      $this->_db->delete('diagnostics', ['id', '=', $id]);

    }

    public function delete_surgery_report_tbl($id = null)
    {

      $this->_db->delete('surgery_upload', ['id', '=', $id]);

    }

    public function delete_diagnostics_report_tbl($id = null)
    {

      $this->_db->delete('diagnostics_upload', ['id', '=', $id]);

    }

    public function delete_doctor_pharmacy($id = null)
    {

      $this->_db->delete('doctor_pharmacy', ['id', '=', $id]);

    }

    public function delete_pharmacy_log($id = null)
    {

      $this->_db->delete('pharmacy_log', ['id', '=', $id]);

    }

    public function new_register($fields = [])
    {
      if (!$this->_db->insert('registration', $fields)) {
        throw new Exception("There was a problem creating an account.");
      }
    }

    public function medical_insurance($fields = [])
    {
      if (!$this->_db->insert('medical_insurance', $fields)) {
        throw new Exception("There was a problem creating the insurance.");
      }
    }

    public function doctor_pharmacy($fields = [])
    {
      if (!$this->_db->insert('doctor_pharmacy', $fields)) {
        throw new Exception("There was a problem creating an account.");
      }
    }

    public function doctor_opticals($fields = [])
    {
      if (!$this->_db->insert('doctor_opticals', $fields)) {
        throw new Exception("There was a problem creating an account.");
      }
    }

    public function opthalmic_investigation($fields = [])
    {
      if (!$this->_db->insert('opthalmic_investigation', $fields)) {
        throw new Exception("There was a problem creating an account.");
      }
    }

    public function routune_investigation($fields = [])
    {
      if (!$this->_db->insert('routune_investigation', $fields)) {
        throw new Exception("There was a problem creating an account.");
      }
    }

    public function optical_log($fields = [])
    {
      if (!$this->_db->insert('optical_log', $fields)) {
        throw new Exception("There was a problem creating an account.");
      }
    }

    public function surgery($fields = [])
    {
      if (!$this->_db->insert('surgery', $fields)) {
        throw new Exception("There was a problem creating an account.");
      }
    }

    public function paharmacy_main($fields = [])
    {
      if (!$this->_db->insert('pharmacy_log', $fields)) {
        throw new Exception("There was a problem creating an account.");
      }
    }

    public function new_work_up($fields = [])
    {
      if (!$this->_db->insert('work_up', $fields)) {
        throw new Exception("There was a problem creating an account.");
      }
    }

    public function new_eye_doctor_report($fields = [])
    {
      if (!$this->_db->insert('eye_doctor_report', $fields)) {
        throw new Exception("There was a problem creating an account.");
      }
    }

    public function new_slilampexamination2($fields = [])
    {
      if (!$this->_db->insert('slilampexamination2', $fields)) {
        throw new Exception("There was a problem creating an account.");
      }
    }

    public function add_new_supplier($fields = [])
    {
      if (!$this->_db->insert('new_supplier', $fields)) {
        throw new Exception("There was a problem creating an Supplier.");
      }
    }

    public function optical_add_new_supplier($fields = [])
    {
      if (!$this->_db->insert('optical_new_supplier', $fields)) {
        throw new Exception("There was a problem creating an Supplier.");
      }
    }
    public function surgery_add_new_supplier($fields = [])
    {
      if (!$this->_db->insert('surgery_new_supplier', $fields)) {
        throw new Exception("There was a problem creating an Supplier.");
      }
    }

    public function diagnostics_add_new_supplier($fields = [])
    {
      if (!$this->_db->insert('diagnostics_new_supplier', $fields)) {
        throw new Exception("There was a problem creating an Supplier.");
      }
    }
    public function add_new_item($fields = [])
    {
      if (!$this->_db->insert('new_item', $fields)) {
        throw new Exception("There was a problem creating an Item.");
      }
    }

    public function opticals_add_new_item($fields = [])
    {
      if (!$this->_db->insert('opticals_new_item', $fields)) {
        throw new Exception("There was a problem creating an Item.");
      }
    }

    public function surgery_add_new_item($fields = [])
    {
      if (!$this->_db->insert('surgery_new_item', $fields)) {
        throw new Exception("There was a problem creating an Item.");
      }
    }

    public function diagnostics_add_new_item($fields = [])
    {
      if (!$this->_db->insert('diagnostics_new_item', $fields)) {
        throw new Exception("There was a problem creating an Item.");
      }
    }

    public function add_new_order($fields = [])
    {
      if (!$this->_db->insert('pharmacy_new_order', $fields)) {
        throw new Exception("There was a problem Placing the order.");
      }
    }

    public function optical_add_new_order($fields = [])
    {
      if (!$this->_db->insert('opticals_new_order', $fields)) {
        throw new Exception("There was a problem Placing the order.");
      }
    }

    public function add_adjustments($fields = [])
    {
      if (!$this->_db->insert('pharmacy_inventory_adjustment', $fields)) {
        throw new Exception("There was a problem with Adjustments.");
      }
    }

    public function opticals_add_adjustments($fields = [])
    {
      if (!$this->_db->insert('opticals_inventory_adjustment', $fields)) {
        throw new Exception("There was a problem with Adjustments.");
      }
    }

    public function add_client_opticals($fields = [])
    {
      if (!$this->_db->insert('opticals_sales', $fields)) {
        throw new Exception("There was a problem with Adjustments.");
      }
    }

    public function add_client_surgery($fields = [])
    {
      if (!$this->_db->insert('surgery', $fields)) {
        throw new Exception("There was a problem with Adjustments.");
      }
    }

    public function add_client_diagnostics($fields = [])
    {
      if (!$this->_db->insert('diagnostics', $fields)) {
        throw new Exception("There was a problem with Adjustments.");
      }
    }

    public function doctor_pharmacy_sale($fields = [])
    {
      if (!$this->_db->insert('doctor_pharmacy_sale', $fields)) {
        throw new Exception("There was a problem with Pharmacy.");
      }
    }

    // public function new_order_update($fields = [], $id = null)
    // {
    //
    //   if (!$this->_db->new_order_update('pharmacy_new_order', $id, $fields)) {
    //     throw new Exception("There was an problem updating");
    //
    //   }
    // }

    public function find($user = null) {

      if ($user) {
        $field = (is_numeric($user)) ? 'id' : 'username';
        $data = $this->_db->get('users', [$field, '=', $user]);

        if ($data->count()) {
          $this->_data = $data->first();
          return TRUE;
        }
      }
      return FALSE;
    }

    public function login($username = null, $password = null, $remember = FALSE)
    {

      if (!$username && !$password && $this->exists()) {
        Session::put($this->_sessionName, $this->data()->id);
      } else {
      $user = $this->find($username);
        if ($user) {
          if ($this->data()->password === Hash::make($password, $this->data()->salt)) {
            Session::put($this->_sessionName, $this->data()->id);

            if ($remember) {
              $hash = Hash::unique();
              $hashCheck = $this->_db->get('users_session', ['user_id', '=', $this->data()->id]) ;

              if (!$hashCheck->count()) {
                $this->_db->insert('users_session', [
                  'user_id' => $this->data()->id,
                  'hash' => $hash
                  ]);
              } else {
                $hash = $hashCheck->first()->hash;
              }
              Cookie::put($this->_cookieName, $hash, Config::get('remember/cookie_expiry'));
            }

            return TRUE;
          }
        }
      }
      return FALSE;
    }

    public function hasPermission($key)
    {
      $group = $this->_db->get('groups', ['id', '=', $this->data()->group]);

      if ($group->count()) {
        $permissions = json_decode($group->first()->permissions, TRUE);

        if ($permissions[$key] == TRUE) {
          return TRUE;
        }
      }
      return FALSE;
    }

    public function exists() {
      return (!empty($this->_data)) ? TRUE : FALSE;
    }

    public function data() {
      return $this->_data;
    }

    public function isLoggedIn()
    {
      return $this->_isLoggedIn;
    }

    public function logout()
    {
      $this->_db->delete('users_session', ['user_id', '=', $this->data()->id]);

      Session::delete($this->_sessionName);
      Cookie::delete($this->_cookieName);
    }

  }
