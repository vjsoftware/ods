<div class="sloder-area">
  <div id="slider-active">
    <img src="img/slider/1.jpg" alt="" title="#active1"/>
    <img src="img/slider/2.jpg" alt="" title="#active2"/>
  </div>
  <div id="active1" class="nivo-html-caption">
    <div class="container">
      <div class="row">
        <div class="col-md-11 col-md-offset-1 col-sm-11 col-sm-offset-1 hidden-xs ">
          <div class="slide1-text text-left">
            <div class="middle-text">
              <div class="cap-sub-title animated">
                <h3>Welcome to OneDayShop </h3>
              </div>
              <div class="cap-title animated text-uppercase">
                <h1>Jio 4G Hotspot </h1>
              </div>
              <div class="cap-dec animated">
                <p>Unmatched 4G network with the lowest data rates globally. </p>
              </div>
              <div class="cap-readmore animated">
                <a href="<?php echo $uri; ?>/product/-/17">Buy Now</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="slider-progress"></div>
  </div>
  <div id="active2" class="nivo-html-caption">
    <div class="container">
      <div class="row">
        <div class="col-md-11 col-md-offset-1 col-sm-11 col-sm-offset-1 hidden-xs">
          <div class="slide1-text text-left">
            <div class="middle-text">
              <div class="cap-sub-title animated">
                <h3>Welcome to OneDayShop </h3>
              </div>
              <div class="cap-title animated text-uppercase">
                <h1>Upto to 70%  </h1>
              </div>
              <div class="cap-title animated text-uppercase">
                <h1>Discount </h1>
              </div>
              <div class="cap-dec animated">
                <p>On Selected Electronic Good and Gadgets   </p>
              </div>
              <!-- <div class="cap-readmore animated">
                <a href="<?php echo $uri; ?>/#">View Collection</a>
              </div> -->
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="slider-progress"></div>
  </div>
</div>
